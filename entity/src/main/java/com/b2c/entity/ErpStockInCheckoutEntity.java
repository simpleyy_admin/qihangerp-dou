package com.b2c.entity;

import java.util.Date;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-29 10:57
 */
public class ErpStockInCheckoutEntity {

    private long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private String number;// varchar(30) DEFAULT '' COMMENT '单据编号',
    private String source;// varchar(15) DEFAULT NULL COMMENT '检验来源，',
    private String transType;// char(15) DEFAULT '0' COMMENT '检验类型,对应invoice表transType对应枚举InvoiceTransTypeEnum',
    private String transTypeName;// varchar(20) DEFAULT NULL COMMENT '交易类型名称',
    private String remark;// varchar(255) DEFAULT NULL COMMENT '备注',
    private int isDelete;// tinyint(1) DEFAULT 0 COMMENT '1删除  0正常',
    private Integer checkoutUserId;// smallint(6) DEFAULT 0 COMMENT '检验人id',
    private String checkoutUserName;// varchar(50) DEFAULT '' COMMENT '检验人',
    private Date createTime;// bigint(20) DEFAULT NULL COMMENT '创建时间',
    private Date modifyTime;// bigint(20) DEFAULT NULL COMMENT '更新时间',
    private String invoiceNo;// varchar(30) DEFAULT '' COMMENT '来源单号，对应erp_invoice表billNo',
    private Long invoiceId;// bigint(20) DEFAULT 0 COMMENT '来源单id，对应erp_invoice表id',
    //    private Long totalQuantity;// bigint(20) NOT NULL COMMENT '总数量',
//    private Long inQuantity;// bigint(20) NOT NULL COMMENT '入库数量',
    private Integer stockInUserId;
    private String stockInUserName;// varchar(25) DEFAULT NULL COMMENT '入库人',
    private Date stockInTime;// date DEFAULT NULL COMMENT '入库时间',
    private Integer stockInStatus;//状态（0未入库，1已入库）
    private Integer hasQcReport;
    private String qcInspector;


    private Long quantity;//总数量，
    private Long inQuantity;//总入库数量
    private Long createOn;//
    private Long modifyOn;//

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public Long getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Long modifyOn) {
        this.modifyOn = modifyOn;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Integer getHasQcReport() {
        return hasQcReport;
    }

    public void setHasQcReport(Integer hasQcReport) {
        this.hasQcReport = hasQcReport;
    }

    public String getQcInspector() {
        return qcInspector;
    }

    public void setQcInspector(String qcInspector) {
        this.qcInspector = qcInspector;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransTypeName() {
        return transTypeName;
    }

    public void setTransTypeName(String transTypeName) {
        this.transTypeName = transTypeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getCheckoutUserId() {
        return checkoutUserId;
    }

    public void setCheckoutUserId(Integer checkoutUserId) {
        this.checkoutUserId = checkoutUserId;
    }

    public String getCheckoutUserName() {
        return checkoutUserName;
    }

    public void setCheckoutUserName(String checkoutUserName) {
        this.checkoutUserName = checkoutUserName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getStockInStatus() {
        return stockInStatus;
    }

    public void setStockInStatus(Integer stockInStatus) {
        this.stockInStatus = stockInStatus;
    }

    public Integer getStockInUserId() {
        return stockInUserId;
    }

    public void setStockInUserId(Integer stockInUserId) {
        this.stockInUserId = stockInUserId;
    }

    public String getStockInUserName() {
        return stockInUserName;
    }

    public void setStockInUserName(String stockInUserName) {
        this.stockInUserName = stockInUserName;
    }

    public Date getStockInTime() {
        return stockInTime;
    }

    public void setStockInTime(Date stockInTime) {
        this.stockInTime = stockInTime;
    }
}
