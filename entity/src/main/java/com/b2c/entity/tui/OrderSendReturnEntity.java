package com.b2c.entity.tui;

import java.math.BigDecimal;

public class OrderSendReturnEntity {
    private Long id;
    private String orderSn;
    private Integer shopId;
    private Integer shopType;
    private Integer returnType;
    private String shopName;
    private Integer goodsId;
    private String goodsName;
    private String goodsNumber;
    private String goodsImg;
    private Integer goodsSpecId;
    private String goodsSpec;
    private String goodsSpecNumber;
    private String logisticsCode;    
    private Integer quantity;
    private Float goodsPrice;
    private BigDecimal refundAmount;
    private Integer receiveType;
    private String receiveTime;
    private String createTime;
    private String remark;
    private Integer status;
    private Integer isSettle;
    // private String stockLocationNum;//所在仓库编码

    
    public Long getId() {
        return id;
    }
    public Integer getShopType() {
        return shopType;
    }
    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }
    public Integer getReturnType() {
        return returnType;
    }
    public void setReturnType(Integer returnType) {
        this.returnType = returnType;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getOrderSn() {
        return orderSn;
    }
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public Integer getShopId() {
        return shopId;
    }
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
    public String getShopName() {
        return shopName;
    }
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
    public Integer getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public Integer getGoodsSpecId() {
        return goodsSpecId;
    }
    public void setGoodsSpecId(Integer goodsSpecId) {
        this.goodsSpecId = goodsSpecId;
    }
    public String getGoodsSpec() {
        return goodsSpec;
    }
    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }
    public String getGoodsSpecNumber() {
        return goodsSpecNumber;
    }
    public void setGoodsSpecNumber(String goodsSpecNumber) {
        this.goodsSpecNumber = goodsSpecNumber;
    }
    public String getLogisticsCode() {
        return logisticsCode;
    }
    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }
    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }
    public Integer getReceiveType() {
        return receiveType;
    }
    public void setReceiveType(Integer receiveType) {
        this.receiveType = receiveType;
    }
    public String getReceiveTime() {
        return receiveTime;
    }
    public void setReceiveTime(String receiveTime) {
        this.receiveTime = receiveTime;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getGoodsImg() {
        return goodsImg;
    }
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public String getGoodsNumber() {
        return goodsNumber;
    }
    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
    public Integer getIsSettle() {
        return isSettle;
    }
    public void setIsSettle(Integer isSettle) {
        this.isSettle = isSettle;
    }
    // public String getStockLocationNum() {
    //     return stockLocationNum;
    // }
    // public void setStockLocationNum(String stockLocationNum) {
    //     this.stockLocationNum = stockLocationNum;
    // }
    public Float getGoodsPrice() {
        return goodsPrice;
    }
    public void setGoodsPrice(Float goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    
}
