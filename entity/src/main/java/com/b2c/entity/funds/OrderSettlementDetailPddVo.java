package com.b2c.entity.funds;



public class OrderSettlementDetailPddVo {
    private String orderSn;
    private String orderTime;
    private String shippingTime;
    private String trackingNumber;
    private Integer refundStatus;
    private Integer orderStatus;
    private Float discountAmount;
    private Float payAmount;
    private Float income;
    private Float expend;
    private Integer auditStatus;
    private Integer settlementStatus;
    private Integer sendStatus;


    public Float getIncome() {
        return income;
    }
    public void setIncome(Float income) {
        this.income = income;
    }
    public Float getExpend() {
        return expend;
    }
    public void setExpend(Float expend) {
        this.expend = expend;
    }
    public String getOrderSn() {
        return orderSn;
    }
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public String getOrderTime() {
        return orderTime;
    }
    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }
    public String getShippingTime() {
        return shippingTime;
    }
    public void setShippingTime(String shippingTime) {
        this.shippingTime = shippingTime;
    }
    public String getTrackingNumber() {
        return trackingNumber;
    }
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }
    public Integer getRefundStatus() {
        return refundStatus;
    }
    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }
    public Integer getOrderStatus() {
        return orderStatus;
    }
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
    public Float getDiscountAmount() {
        return discountAmount;
    }
    public void setDiscountAmount(Float discountAmount) {
        this.discountAmount = discountAmount;
    }
    public Float getPayAmount() {
        return payAmount;
    }
    public void setPayAmount(Float payAmount) {
        this.payAmount = payAmount;
    }
    public Integer getAuditStatus() {
        return auditStatus;
    }
    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }
    public Integer getSettlementStatus() {
        return settlementStatus;
    }
    public void setSettlementStatus(Integer settlementStatus) {
        this.settlementStatus = settlementStatus;
    }
    public Integer getSendStatus() {
        return sendStatus;
    }
    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }
 
}
