package com.b2c.entity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-15 17:52
 */
public class ErpGoodsBadStockEntity {
    private Long id;//` bigint(11) NOT NULL AUTO_INCREMENT,
    private Integer goodsId;//` int(11) NOT NULL COMMENT '商品id',
    private Integer specId;//` int(11) NOT NULL COMMENT '商品规格id',
    private String specNumber;//` varchar(25) NOT NULL DEFAULT '' COMMENT '规格编码（唯一）',
    private Integer quantity;//` bigint(20) NOT NULL DEFAULT 0 COMMENT '当前库存',
    private Integer isDelete;//` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0正常  1删除',
    private Integer type;//类型（1报损2退货报损）
    private String sourceId;
    private Integer locationId;
    private String result;
    private String reason;
    private Integer status;
    private String createTime;
    private String resultTime;

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getResultTime() {
        return resultTime;
    }

    public void setResultTime(String resultTime) {
        this.resultTime = resultTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
