package com.b2c.entity;

import java.util.List;

/**
 * @Description:分类属性 pbd add 2018/12/20 10:39
 */
public class GoodsCategoryAttributeEntity {
    private Integer id;
    /**
     * 属性所属id
     */
    private Integer goodsCategoryId;
    /**
     * 标题
     */
    private String title;
    private String code;
    /**
     * 属性值
     */
    private String valueText;
    /**
     * 是否筛选属性
     */
    private Integer isFilter;
    /**
     * 类型 0：属性 1:规格
     */
    private int type;

    /**
     * 值类型：0文本1下拉
     */
    private Integer valueType;
    /**
     * 显示属性值
     */
    private String valueTextShow;

    private List<GoodsCategoryAttributeValueEntity> values;

    public List<GoodsCategoryAttributeValueEntity> getValues() {
        return values;
    }

    public void setValues(List<GoodsCategoryAttributeValueEntity> values) {
        this.values = values;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getValueType() {
        return valueType;
    }

    public void setValueType(Integer valueType) {
        this.valueType = valueType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsCategoryId() {
        return goodsCategoryId;
    }

    public void setGoodsCategoryId(Integer goodsCategoryId) {
        this.goodsCategoryId = goodsCategoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValueText() {
        return valueText;
    }

    public void setValueText(String valueText) {
        this.valueText = valueText;
    }

    public Integer getIsFilter() {
        return isFilter;
    }

    public void setIsFilter(Integer isFilter) {
        this.isFilter = isFilter;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValueTextShow() {
        return valueTextShow;
    }

    public void setValueTextShow(String valueTextShow) {
        this.valueTextShow = valueTextShow;
    }
}
