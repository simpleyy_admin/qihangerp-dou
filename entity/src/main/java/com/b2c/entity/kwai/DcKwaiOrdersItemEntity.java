package com.b2c.entity.kwai;

import java.math.BigDecimal;

public class DcKwaiOrdersItemEntity {
    /**
     * id，自增
     */
    private Long id;

    /**
     * 快手子订单id
     */
    private Long itemid;

    /**
     * 订单id
     */
    private Long orderid;

    /**
     * erp系统商品id
     */
    private Integer erpgoodsId;

    /**
     * erp系统商品规格id
     */
    private Integer erpgoodsSpecid;

    /**
     * 商品名称
     */
    private String itemtitle;

    /**
     * 商品图片
     */
    private String itempicurl;

    /**
     * 商品编码
     */
    private String goodsnum;

    /**
     * 商品规格
     */
    private String goodsspec;

    /**
     * 商品规格编码
     */
    private String skunick;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private Long num;

    /**
     * 退货id
     */
    private Long refundId;

    /**
     * 退货状态
     */
    private Integer refundStatus;

    /**
     * remark
     */
    private String remark;
    //是否赠品
    private Integer isGift;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemid() {
        return itemid;
    }

    public void setItemid(Long itemid) {
        this.itemid = itemid;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public Integer getErpgoodsId() {
        return erpgoodsId;
    }

    public void setErpgoodsId(Integer erpgoodsId) {
        this.erpgoodsId = erpgoodsId;
    }

    public Integer getErpgoodsSpecid() {
        return erpgoodsSpecid;
    }

    public void setErpgoodsSpecid(Integer erpgoodsSpecid) {
        this.erpgoodsSpecid = erpgoodsSpecid;
    }

    public String getItemtitle() {
        return itemtitle;
    }

    public void setItemtitle(String itemtitle) {
        this.itemtitle = itemtitle;
    }

    public String getItempicurl() {
        return itempicurl;
    }

    public void setItempicurl(String itempicurl) {
        this.itempicurl = itempicurl;
    }

    public String getGoodsnum() {
        return goodsnum;
    }

    public void setGoodsnum(String goodsnum) {
        this.goodsnum = goodsnum;
    }

    public String getGoodsspec() {
        return goodsspec;
    }

    public void setGoodsspec(String goodsspec) {
        this.goodsspec = goodsspec;
    }

    public String getSkunick() {
        return skunick;
    }

    public void setSkunick(String skunick) {
        this.skunick = skunick;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public Long getRefundId() {
        return refundId;
    }

    public void setRefundId(Long refundId) {
        this.refundId = refundId;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIsGift() {
        return isGift;
    }

    public void setIsGift(Integer isGift) {
        this.isGift = isGift;
    }
}
