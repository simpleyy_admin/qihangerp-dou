package com.b2c.entity.mall;

import java.math.BigDecimal;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-17 14:16
 */
public class OrderCancelItemEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
    private Long orderCancelId;
    private Long orderId;//` bigint(20) NOT NULL DEFAULT 0 COMMENT '订单id',
    private Long orderItemId;
    private Integer goodsId;//` int(11) NOT NULL,
    private Integer specId;//` int(11) NOT NULL COMMENT 'sku id',
    private String specNumber;//` varchar(25) NOT NULL COMMENT '商品sku',
    private String title;//` varchar(45) NOT NULL DEFAULT '' COMMENT '商品名称',
    private String image;//` varchar(145) NOT NULL DEFAULT '' COMMENT '商品主图',
    private String color;//` varchar(45) DEFAULT NULL COMMENT '颜色',
    private String size;//` varchar(45) DEFAULT NULL COMMENT '尺码',
    private Integer quantity;//` int(11) NOT NULL COMMENT '商品数量',
    private BigDecimal price;//` decimal(10,2) NOT NULL COMMENT '商品价格',
    private BigDecimal discountPrice;//` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT '折扣价格',
    private Integer status;//` int(11) DEFAULT 0 COMMENT '售后状态0未申请售后1售后申请中(退款待审核)2同意退货(退款待收货)3买家已发货，待收货(待收货)4已收货（待退款）5退款退货成功(退款完成)6退款拒绝7已确认收货，正在退款中 8退款取消'

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderCancelId() {
        return orderCancelId;
    }

    public void setOrderCancelId(Long orderCancelId) {
        this.orderCancelId = orderCancelId;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
