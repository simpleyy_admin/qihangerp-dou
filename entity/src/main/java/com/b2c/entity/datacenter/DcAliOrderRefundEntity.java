package com.b2c.entity.datacenter;

import java.io.Serializable;

public class DcAliOrderRefundEntity implements Serializable {
    //退款单编号
    private  Long refId;
    //退款单逻辑主键
    private  String refundId;
    //支付宝交易号
    private  String alipayPaymentId;

    //买家原始输入的退款金额(可以为空)
//    private  Long applyExpect;

    //买家申请退款金额，单位：分
    private  Long applyPayment;

    //申请原因
    private  String applyReason;

    //申请原因Id
//    private  Integer applyReasonId;

    //二级退款原因
    private  String applySubReason;

    //二级退款原因Id
//    private  Integer applySubReasonId;

    //买家支付宝ID
//    private  String buyerAlipayId;

    private String buyerLoginId;

    //买家退货物流公司名
    private  String buyerLogisticsName;

    //买家会员ID
    private  String buyerMemberId;

    //买家阿里帐号ID(包括淘宝帐号Id)
    private  Long buyerUserId;

    //最大能够退款金额，单位：分
    private  Long canRefundPayment;

    //极速到账打款渠道
//    private  String disburseChannel;

    //纠纷类型：售中退款 售后退款，默认为售中退款
    private  Integer disputeType;

    //扩展信息
    private  String extInfo;

    //运单号
    private  String freightBill;

    //实际冻结账户金额,单位：分
    private  Long frozenFund;

    //申请退款时间
    private  Long gmtApply;

    //完成时间
    private  Long gmtCompleted;

    //创建时间
    private  Long gmtCreate;

    //该退款单超时冻结开始时间
    private  Long gmtFreezed;

    //修改时间
    private Long gmtModified;

    //该退款单超时完成的时间期限
    private  Long gmtTimeOut;

    //1：买家未收到货 2：买家已收到货 3：买家已退货
    private  Integer goodsStatus;

    //极速到账退款类型
//    private  String instantRefundType;

    //交易4.0退款余额不足
    private  Integer insufficientAccount;


    private  String orderEntryCountMap;

    //退款单包含的订单明细，时间逆序排列
    private  String orderEntryIdList;

    //退款单对应的订单编号
    private  Long orderId;

    //产品名称
    private  String productName;

    //运费的实际退款金额，单位：分
    private  Long refundCarriage;



    //实际退款金额，单位：分
    private  Long refundPayment;

    //卖家拒绝原因
    private  String rejectReason;

    //退款单被拒绝的次数
    private  Integer rejectTimes;

    //卖家支付宝ID
//    private  String sellerAlipayId;

    //卖家会员ID
//    private  String sellerMemberId;

    //收货人手机
//    private  String sellerMobile;

    //收货人姓名
//    private  String sellerRealName;

    //买家退货时卖家收货地址
//    private  String sellerReceiveAddress;

    //卖家阿里帐号ID(包括淘宝帐号Id)
    private  Long sellerUserId;

    //退款状态1等待卖家同意2退款成功3退款关闭4待买家修改5等待买家退货6等待卖家确认收货
    private  Integer status;

    private Integer isRefundGoods;//是否要求退货
    private Integer isOnlyRefund;//是否仅退款
    private Integer isGoodsReceived;//买家是否已收到货
    private Integer isBuyerSendGoods;//买家是否已经发货（如果有退货的流程）

    private Integer auditStatus;//订单审核状态（0待审核1已审核）

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getIsRefundGoods() {
        return isRefundGoods;
    }

    public void setIsRefundGoods(Integer isRefundGoods) {
        this.isRefundGoods = isRefundGoods;
    }

    public Integer getIsOnlyRefund() {
        return isOnlyRefund;
    }

    public void setIsOnlyRefund(Integer isOnlyRefund) {
        this.isOnlyRefund = isOnlyRefund;
    }

    public Integer getIsGoodsReceived() {
        return isGoodsReceived;
    }

    public void setIsGoodsReceived(Integer isGoodsReceived) {
        this.isGoodsReceived = isGoodsReceived;
    }

    public Integer getIsBuyerSendGoods() {
        return isBuyerSendGoods;
    }

    public void setIsBuyerSendGoods(Integer isBuyerSendGoods) {
        this.isBuyerSendGoods = isBuyerSendGoods;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getAlipayPaymentId() {
        return alipayPaymentId;
    }

    public void setAlipayPaymentId(String alipayPaymentId) {
        this.alipayPaymentId = alipayPaymentId;
    }

    public Integer getRejectTimes() {
        return rejectTimes;
    }

    public void setRejectTimes(Integer rejectTimes) {
        this.rejectTimes = rejectTimes;
    }

    public Long getApplyPayment() {
        return applyPayment;
    }

    public void setApplyPayment(Long applyPayment) {
        this.applyPayment = applyPayment;
    }

    public String getApplyReason() {
        return applyReason;
    }

    public void setApplyReason(String applyReason) {
        this.applyReason = applyReason;
    }

    public String getBuyerLoginId() {
        return buyerLoginId;
    }

    public void setBuyerLoginId(String buyerLoginId) {
        this.buyerLoginId = buyerLoginId;
    }

    public String getApplySubReason() {
        return applySubReason;
    }

    public void setApplySubReason(String applySubReason) {
        this.applySubReason = applySubReason;
    }


    public String getBuyerLogisticsName() {
        return buyerLogisticsName;
    }

    public void setBuyerLogisticsName(String buyerLogisticsName) {
        this.buyerLogisticsName = buyerLogisticsName;
    }

    public String getBuyerMemberId() {
        return buyerMemberId;
    }

    public void setBuyerMemberId(String buyerMemberId) {
        this.buyerMemberId = buyerMemberId;
    }

    public Long getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(Long buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public Long getCanRefundPayment() {
        return canRefundPayment;
    }

    public void setCanRefundPayment(Long canRefundPayment) {
        this.canRefundPayment = canRefundPayment;
    }

    public String getExtInfo() {
        return extInfo;
    }

    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }

    public String getFreightBill() {
        return freightBill;
    }

    public void setFreightBill(String freightBill) {
        this.freightBill = freightBill;
    }

    public Long getFrozenFund() {
        return frozenFund;
    }

    public void setFrozenFund(Long frozenFund) {
        this.frozenFund = frozenFund;
    }

    public Integer getDisputeType() {
        return disputeType;
    }

    public void setDisputeType(Integer disputeType) {
        this.disputeType = disputeType;
    }

    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }

    public Integer getInsufficientAccount() {
        return insufficientAccount;
    }

    public void setInsufficientAccount(Integer insufficientAccount) {
        this.insufficientAccount = insufficientAccount;
    }

    public String getOrderEntryCountMap() {
        return orderEntryCountMap;
    }

    public void setOrderEntryCountMap(String orderEntryCountMap) {
        this.orderEntryCountMap = orderEntryCountMap;
    }

    public String getOrderEntryIdList() {
        return orderEntryIdList;
    }

    public void setOrderEntryIdList(String orderEntryIdList) {
        this.orderEntryIdList = orderEntryIdList;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getRefundCarriage() {
        return refundCarriage;
    }

    public void setRefundCarriage(Long refundCarriage) {
        this.refundCarriage = refundCarriage;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public Long getRefundPayment() {
        return refundPayment;
    }

    public void setRefundPayment(Long refundPayment) {
        this.refundPayment = refundPayment;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }


    public Long getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Long sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getGmtApply() {
        return gmtApply;
    }

    public void setGmtApply(Long gmtApply) {
        this.gmtApply = gmtApply;
    }

    public Long getGmtCompleted() {
        return gmtCompleted;
    }

    public void setGmtCompleted(Long gmtCompleted) {
        this.gmtCompleted = gmtCompleted;
    }

    public Long getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Long gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Long getGmtFreezed() {
        return gmtFreezed;
    }

    public void setGmtFreezed(Long gmtFreezed) {
        this.gmtFreezed = gmtFreezed;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Long getGmtTimeOut() {
        return gmtTimeOut;
    }

    public void setGmtTimeOut(Long gmtTimeOut) {
        this.gmtTimeOut = gmtTimeOut;
    }
}
