package com.b2c.entity.datacenter;

import java.math.BigDecimal;

/**
 * 描述：天猫订单excel（适用于淘宝开放平台服务市场订单导出应用）
 *
 * @author qlp
 * @date 2019-10-23 09:13
 */
public class DcTmallOrderItemEntity {
    private Long id;//子订单号，主键，自增长
    private String orderId;//主订单Id
    private String subItemId;//天猫子订单id
    private BigDecimal itemAmount;//明细金额
    private BigDecimal discountFee;//优惠金额
    private BigDecimal adjustFee;//手工调整金额
    private String goodsTitle;//宝贝名称
    private String goodsNumber;//商家编码
    private String productImgUrl;//主图productImgUrl
    private String productUrl;//商品链接
    private String productId;//商品数字ID,天猫店铺的商品id
    private String skuId;//SKUID,天猫店铺的skuid
    private String specNumber;//属性商家编码
    private String skuInfo;//商品规格属性信息
    private BigDecimal price;//实际单价
    private Double quantity;//数量
    private Integer erpGoodsId;
    private Integer erpGoodsSpecId;
    private Integer shopId;
    private Double goodsPurPrice;//商品采购价


    private String statusStr;//子订单状态字符串
    private Integer status;//子订单状态
    private String refundStatusStr;//子订单退货状态字符串
    private Integer refundStatus;//子订单脱货状态
    private BigDecimal refundAmount;//退款金额
    private String refundId;//退款单id
    private Integer logisticsStatus;//发货状态   1 未发货 2 已发货 3 已收货 4 已经退货 5 部分发货 8 还未创建物流订单

    private Integer newSpecId;
    private String newSpecNumber;

    private String erpSpec;//仓库商品规格
    private String newSpec;//新规格
    /**********商品仓库库存信息**********/
    private Long currentQty;
    private Long lockedQty;
    private Long pickQty;
    private Integer isGift;//是否礼品0否1是
    private Integer isSwap;//是否换货(0:否1:是)
    /**
     * 售后状态
     */
    private Integer afterSaleState;
    private Integer erpContactId;

    
    public Integer getErpGoodsId() {
        return erpGoodsId;
    }

    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }

    public Integer getErpGoodsSpecId() {
        return erpGoodsSpecId;
    }

    public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
        this.erpGoodsSpecId = erpGoodsSpecId;
    }

    public BigDecimal getDiscountFee() {
        return discountFee;
    }

    public void setDiscountFee(BigDecimal discountFee) {
        this.discountFee = discountFee;
    }

    public BigDecimal getAdjustFee() {
        return adjustFee;
    }

    public void setAdjustFee(BigDecimal adjustFee) {
        this.adjustFee = adjustFee;
    }

    public String getErpSpec() {
        return erpSpec;
    }

    public void setErpSpec(String erpSpec) {
        this.erpSpec = erpSpec;
    }

    public String getNewSpec() {
        return newSpec;
    }

    public void setNewSpec(String newSpec) {
        this.newSpec = newSpec;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Long getPickQty() {
        return pickQty;
    }

    public void setPickQty(Long pickQty) {
        this.pickQty = pickQty;
    }

    public Integer getAfterSaleState() {
        return afterSaleState;
    }

    public void setAfterSaleState(Integer afterSaleState) {
        this.afterSaleState = afterSaleState;
    }

    public Integer getNewSpecId() {
        return newSpecId;
    }

    public void setNewSpecId(Integer newSpecId) {
        this.newSpecId = newSpecId;
    }

    public String getNewSpecNumber() {
        return newSpecNumber;
    }

    public void setNewSpecNumber(String newSpecNumber) {
        this.newSpecNumber = newSpecNumber;
    }

    public String getSkuInfo() {
        return skuInfo;
    }

    public void setSkuInfo(String skuInfo) {
        this.skuInfo = skuInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSubItemId() {
        return subItemId;
    }

    public void setSubItemId(String subItemId) {
        this.subItemId = subItemId;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public Integer getLogisticsStatus() {
        return logisticsStatus;
    }

    public void setLogisticsStatus(Integer logisticsStatus) {
        this.logisticsStatus = logisticsStatus;
    }

    public String getGoodsTitle() {
        return goodsTitle;
    }

    public void setGoodsTitle(String goodsTitle) {
        this.goodsTitle = goodsTitle;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRefundStatusStr() {
        return refundStatusStr;
    }

    public void setRefundStatusStr(String refundStatusStr) {
        this.refundStatusStr = refundStatusStr;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public Integer getIsGift() {
        return isGift;
    }

    public void setIsGift(Integer isGift) {
        this.isGift = isGift;
    }

    public Integer getIsSwap() {
        return isSwap;
    }

    public void setIsSwap(Integer isSwap) {
        this.isSwap = isSwap;
    }

    public Integer getErpContactId() {
        return erpContactId;
    }

    public void setErpContactId(Integer erpContactId) {
        this.erpContactId = erpContactId;
    }

    public Double getGoodsPurPrice() {
        return goodsPurPrice;
    }

    public void setGoodsPurPrice(Double goodsPurPrice) {
        this.goodsPurPrice = goodsPurPrice;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
}
