package com.b2c.entity.xhs;

import java.math.BigDecimal;

public class XhsOrderSettleEntity {
    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
    private String orderNo;//` VARCHAR(50) NOT NULL COMMENT '订单号' COLLATE 'utf8_general_ci',
    private String afterSaleNo;//` VARCHAR(50) NOT NULL COMMENT '售后单号' COLLATE 'utf8_general_ci',
    private String orderCreateTime;//` DATETIME NOT NULL COMMENT '订单创建时间',
    private String orderSettleTime;//` DATETIME NOT NULL COMMENT '结算时间',
    private String transactionType;//` VARCHAR(50) NOT NULL COMMENT '交易类型' COLLATE 'utf8_general_ci',
    private String settleAccount;//` VARCHAR(50) NOT NULL COMMENT '结算账户' COLLATE 'utf8_general_ci',
    private BigDecimal amount;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '动账金额',
    private BigDecimal settleAmount;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '结算金额',
    private BigDecimal goodsAmount;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '商品实付/实退',
    private BigDecimal freightAmount;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '运费实付/实退',
    private BigDecimal platformDiscount;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '平台优惠',
    private BigDecimal goodsTax;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '商品税金',
    private BigDecimal freightTax;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '运费税金',
    private BigDecimal commission;//` DECIMAL(6,2) NOT NULL DEFAULT '0.00' COMMENT '佣金',
    private BigDecimal paymentChannelFee;//` DECIMAL(6,2) NOT NULL COMMENT '支付渠道费',
    private BigDecimal distributionCommission;//` DECIMAL(6,2) NOT NULL COMMENT '分销佣金',
    private BigDecimal huabeiFee;//` DECIMAL(6,2) NOT NULL COMMENT '花呗手续费',
    private String remark;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '备注' COLLATE 'utf8_general_ci',
    private String createTime;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getAfterSaleNo() {
        return afterSaleNo;
    }

    public void setAfterSaleNo(String afterSaleNo) {
        this.afterSaleNo = afterSaleNo;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderSettleTime() {
        return orderSettleTime;
    }

    public void setOrderSettleTime(String orderSettleTime) {
        this.orderSettleTime = orderSettleTime;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSettleAccount() {
        return settleAccount;
    }

    public void setSettleAccount(String settleAccount) {
        this.settleAccount = settleAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(BigDecimal settleAmount) {
        this.settleAmount = settleAmount;
    }

    public BigDecimal getGoodsAmount() {
        return goodsAmount;
    }

    public void setGoodsAmount(BigDecimal goodsAmount) {
        this.goodsAmount = goodsAmount;
    }

    public BigDecimal getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(BigDecimal freightAmount) {
        this.freightAmount = freightAmount;
    }

    public BigDecimal getPlatformDiscount() {
        return platformDiscount;
    }

    public void setPlatformDiscount(BigDecimal platformDiscount) {
        this.platformDiscount = platformDiscount;
    }

    public BigDecimal getGoodsTax() {
        return goodsTax;
    }

    public void setGoodsTax(BigDecimal goodsTax) {
        this.goodsTax = goodsTax;
    }

    public BigDecimal getFreightTax() {
        return freightTax;
    }

    public void setFreightTax(BigDecimal freightTax) {
        this.freightTax = freightTax;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public BigDecimal getPaymentChannelFee() {
        return paymentChannelFee;
    }

    public void setPaymentChannelFee(BigDecimal paymentChannelFee) {
        this.paymentChannelFee = paymentChannelFee;
    }

    public BigDecimal getDistributionCommission() {
        return distributionCommission;
    }

    public void setDistributionCommission(BigDecimal distributionCommission) {
        this.distributionCommission = distributionCommission;
    }

    public BigDecimal getHuabeiFee() {
        return huabeiFee;
    }

    public void setHuabeiFee(BigDecimal huabeiFee) {
        this.huabeiFee = huabeiFee;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
