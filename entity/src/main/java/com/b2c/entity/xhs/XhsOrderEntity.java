package com.b2c.entity.xhs;

import java.util.List;

public class XhsOrderEntity {
    private Long id;
    private String orderId;
    private Integer shopType;

    private Integer shopId;
    private Integer orderType;//` INT(11) NOT NULL DEFAULT '0' COMMENT '订单类型：（小红书：订单类型，1普通 2定金预售 3全款预售 4延迟发货 5换货补发）',
    private Integer orderStatus;//` INT(11) NOT NULL DEFAULT '0' COMMENT '小红书订单状态，1已下单待付款 2已支付处理中 3清关中 4待发货 5部分发货 6待收货 7已完成 8已关闭 9已取消 10换货申请中',
    private Integer afterSalesStatus;//` INT(11) NOT NULL DEFAULT '0' COMMENT '小红书售后状态，1无售后 2售后处理中 3售后完成(含取消)',
    private Integer cancelStatus;//` INT(11) NOT NULL DEFAULT '0' COMMENT '申请取消状态，0未申请取消 1取消处理中',
    private Integer erpSendStatus;//ERP发货状态0待处理1出库中2已出库3已发货
    private Long createdTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单创建时间 单位ms',
    private Long paidTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单支付时间 单位ms',
    private Long updateTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单更新时间 单位ms',
    private Long deliveryTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单发货时间 单位ms',
    private Long cancelTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单取消时间 单位ms',
    private Long finishTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单完成时间 单位ms',
    private Long promiseLastDeliveryTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '承诺最晚发货时间 单位ms',
    private String customerRemark;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '用户备注' COLLATE 'utf8_general_ci',
    private String sellerRemark;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '商家标记备注' COLLATE 'utf8_general_ci',
    private Integer sellerRemarkFlag;//` INT(11) NOT NULL DEFAULT '0' COMMENT '商家标记优先级，ark订单列表展示旗子颜色 1灰旗 2红旗 3黄旗 4绿旗 5蓝旗 6紫旗',
    private Long presaleDeliveryStartTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '预售最早发货时间 单位ms',
    private Long presaleDeliveryEndTime;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '预售最晚发货时间 单位ms',
    private String originalPackageId;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '原始关联订单号(退换订单的原订单)' COLLATE 'utf8_general_ci',
    private Integer totalPayAmount;//` INT(11) NOT NULL DEFAULT '0' COMMENT '订单实付金额(包含运费) 单位分',
    private Integer totalShippingFree;//` INT(11) NOT NULL DEFAULT '0' COMMENT '订单运费 单位分',
    private String expressTrackingNo;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '快递单号' COLLATE 'utf8_general_ci',
    private String expressCompanyCode;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '快递公司编码' COLLATE 'utf8_general_ci',
    private String openAddressId;//` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '收件人姓名+手机+地址等计算得出，用来查询收件人详情' COLLATE 'utf8_general_ci',
    private String createOn;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间（系统）',
    private String modifyOn;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间（系统）',
    private String province;
    private String city;
    private String district;
    private String receiver;
    private String phone;
    private String address;


    private List<XhsOrderItemEntity> items;

    public Integer getErpSendStatus() {
        return erpSendStatus;
    }

    public void setErpSendStatus(Integer erpSendStatus) {
        this.erpSendStatus = erpSendStatus;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public List<XhsOrderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<XhsOrderItemEntity> items) {
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getAfterSalesStatus() {
        return afterSalesStatus;
    }

    public void setAfterSalesStatus(Integer afterSalesStatus) {
        this.afterSalesStatus = afterSalesStatus;
    }

    public Integer getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(Integer cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public Long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    public Long getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(Long paidTime) {
        this.paidTime = paidTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Long getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Long cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Long finishTime) {
        this.finishTime = finishTime;
    }

    public Long getPromiseLastDeliveryTime() {
        return promiseLastDeliveryTime;
    }

    public void setPromiseLastDeliveryTime(Long promiseLastDeliveryTime) {
        this.promiseLastDeliveryTime = promiseLastDeliveryTime;
    }

    public String getCustomerRemark() {
        return customerRemark;
    }

    public void setCustomerRemark(String customerRemark) {
        this.customerRemark = customerRemark;
    }

    public String getSellerRemark() {
        return sellerRemark;
    }

    public void setSellerRemark(String sellerRemark) {
        this.sellerRemark = sellerRemark;
    }

    public Integer getSellerRemarkFlag() {
        return sellerRemarkFlag;
    }

    public void setSellerRemarkFlag(Integer sellerRemarkFlag) {
        this.sellerRemarkFlag = sellerRemarkFlag;
    }

    public Long getPresaleDeliveryStartTime() {
        return presaleDeliveryStartTime;
    }

    public void setPresaleDeliveryStartTime(Long presaleDeliveryStartTime) {
        this.presaleDeliveryStartTime = presaleDeliveryStartTime;
    }

    public Long getPresaleDeliveryEndTime() {
        return presaleDeliveryEndTime;
    }

    public void setPresaleDeliveryEndTime(Long presaleDeliveryEndTime) {
        this.presaleDeliveryEndTime = presaleDeliveryEndTime;
    }

    public String getOriginalPackageId() {
        return originalPackageId;
    }

    public void setOriginalPackageId(String originalPackageId) {
        this.originalPackageId = originalPackageId;
    }

    public Integer getTotalPayAmount() {
        return totalPayAmount;
    }

    public void setTotalPayAmount(Integer totalPayAmount) {
        this.totalPayAmount = totalPayAmount;
    }

    public Integer getTotalShippingFree() {
        return totalShippingFree;
    }

    public void setTotalShippingFree(Integer totalShippingFree) {
        this.totalShippingFree = totalShippingFree;
    }

    public String getExpressTrackingNo() {
        return expressTrackingNo;
    }

    public void setExpressTrackingNo(String expressTrackingNo) {
        this.expressTrackingNo = expressTrackingNo;
    }

    public String getExpressCompanyCode() {
        return expressCompanyCode;
    }

    public void setExpressCompanyCode(String expressCompanyCode) {
        this.expressCompanyCode = expressCompanyCode;
    }

    public String getOpenAddressId() {
        return openAddressId;
    }

    public void setOpenAddressId(String openAddressId) {
        this.openAddressId = openAddressId;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }
}
