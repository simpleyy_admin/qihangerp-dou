package com.b2c.entity.query;
/**
 * 商品库存查询
 */
public class TmallOrderCostQuery  {
    //开始页码
    private int pageIndex;
    //页数
    private int pageSize;
    //订单id
    private Long orderId;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
