package com.b2c.entity.pdd;

import java.math.BigDecimal;

public class SalesAndRefundReportPddVo {
    private String goodsImage;
    private String goodsName;
    private String goodsNick;
    private String goodsNum;
    private Long goodsId;
    private Long erpGoodsId;
    private Float refundTotal;
    private Float salesTotal;

    
    public String getGoodsImage() {
        return goodsImage;
    }
    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }
    public Long getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
    public Long getErpGoodsId() {
        return erpGoodsId;
    }
    public void setErpGoodsId(Long erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }
    public Float getRefundTotal() {
        return refundTotal;
    }
    public void setRefundTotal(Float refundTotal) {
        this.refundTotal = refundTotal;
    }
    public Float getSalesTotal() {
        return salesTotal;
    }
    public void setSalesTotal(Float salesTotal) {
        this.salesTotal = salesTotal;
    }
    public String getGoodsNick() {
        return goodsNick;
    }
    public void setGoodsNick(String goodsNick) {
        this.goodsNick = goodsNick;
    }

    
   

    
}
