package com.b2c.entity.pdd;

public class ShopGoodsSkuEntity {
    private Long id;
    private java.lang.String spec;
  
  private java.lang.Long skuId;
  
  private java.lang.Long skuQuantity;
  
  private java.lang.String outerId;
  
  private java.lang.String outerGoodsId;
  
  private java.lang.Integer isSkuOnsale;
  
  private java.lang.Long reserveQuantity;
  private Integer erpGoodsId;
  private Integer erpGoodsSpecId;
  private String erpGoodsSpecCode;

  
public String getErpGoodsSpecCode() {
    return erpGoodsSpecCode;
}

public void setErpGoodsSpecCode(String erpGoodsSpecCode) {
    this.erpGoodsSpecCode = erpGoodsSpecCode;
}

public Integer getErpGoodsSpecId() {
    return erpGoodsSpecId;
}

public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
    this.erpGoodsSpecId = erpGoodsSpecId;
}

public java.lang.String getSpec() {
    return spec;
}

public void setSpec(java.lang.String spec) {
    this.spec = spec;
}

public java.lang.Long getSkuId() {
    return skuId;
}

public void setSkuId(java.lang.Long skuId) {
    this.skuId = skuId;
}

public java.lang.Long getSkuQuantity() {
    return skuQuantity;
}

public void setSkuQuantity(java.lang.Long skuQuantity) {
    this.skuQuantity = skuQuantity;
}

public java.lang.String getOuterId() {
    return outerId;
}

public void setOuterId(java.lang.String outerId) {
    this.outerId = outerId;
}

public java.lang.String getOuterGoodsId() {
    return outerGoodsId;
}

public void setOuterGoodsId(java.lang.String outerGoodsId) {
    this.outerGoodsId = outerGoodsId;
}

public java.lang.Integer getIsSkuOnsale() {
    return isSkuOnsale;
}

public void setIsSkuOnsale(java.lang.Integer isSkuOnsale) {
    this.isSkuOnsale = isSkuOnsale;
}

public java.lang.Long getReserveQuantity() {
    return reserveQuantity;
}

public void setReserveQuantity(java.lang.Long reserveQuantity) {
    this.reserveQuantity = reserveQuantity;
}

public Long getId() {
    return id;
}

public void setId(Long id) {
    this.id = id;
}

public Integer getErpGoodsId() {
    return erpGoodsId;
}

public void setErpGoodsId(Integer erpGoodsId) {
    this.erpGoodsId = erpGoodsId;
}

  
}
