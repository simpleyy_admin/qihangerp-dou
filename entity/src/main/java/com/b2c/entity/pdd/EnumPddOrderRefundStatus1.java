package com.b2c.entity.pdd;

public enum EnumPddOrderRefundStatus1 {
    //订单售后状态，1：无售后或售后关闭，2：售后处理中，3：退款中，4：退款成功，0：异常
    WSH("无售后或售后关闭",1),
    SHZ("售后处理中",2),
    TKZ("退款中",3),
    TKCC("退款成功",4),
    YC("异常",0);

    // 成员变量
    private String name;
    private int pddIndex;

    // 构造方法
    private EnumPddOrderRefundStatus1(String name, int pddIndex) {
        this.name = name;
        this.pddIndex = pddIndex;
    }

    // 普通方法
    public static String getName(int pddIndex) {
        for (EnumPddOrderRefundStatus1 c : EnumPddOrderRefundStatus1.values()) {
            if (c.getPddIndex()==pddIndex) {
                return c.name;
            }
        }
        return null;
    }


    public static int getPddIndex(String name) {
        for (EnumPddOrderRefundStatus1 c : EnumPddOrderRefundStatus1.values()) {
            if (c.toString().equals(name)) {
                return c.pddIndex;
            }
        }
        return 0;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPddIndex() {
        return pddIndex;
    }

    public void setPddIndex(int pddIndex) {
        this.pddIndex = pddIndex;
    }



}
