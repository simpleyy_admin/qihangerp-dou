package com.b2c.entity.pdd;


/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-16 20:49
 */
public enum EnumPddOrderRefundStatus {
    //pdd 退货状态 1：全部 2：买家申请退款，待商家处理 3：退货退款，待商家处理 4：商家同意退款，退款中 5：平台同意退款，退款中 6：驳回退款， 待买家处理 7：已同意退货退款,待用户发货 8：平台处理中 
    //9：平台拒 绝退款，退款关闭 10：退款成功 11：买家撤销 12：买家逾期未处 理，退款失败 13：买家逾期，超过有效期 14 : 换货补寄待商家处理 15:换货补寄待用户处理 16:换货补寄成功 17:换货补寄失败 18:换货补寄待用户确认完成 
    //31：商家同意拒收退款，待用户拒收;32: 待商家补寄发货'
    //erp 退款状态 -1取消申请；0拒绝退货；1申请中(待审核)；2等待买家发货；3买家已发货(待收货)；4已收货（完成）5成功
    SQZ("买家申请退款",2, 1),
    TKZ("退货退款，待商家处理",3,1),
    TKZPT("平台同意退款，退款中",4,1),
    TKZSJ("商家同意退款，退款中",5,1),
    YBH("驳回退款，待买家处理",6, 0),
    DFH("已同意退货退款,待用户发货",7,2),
    YJJ("平台拒绝退款，退款关闭",9, 0),
    MJCX("买家撤销",11, 11),
    MJYQ("买家逾期未处理，退款失败",12, 12),
    HHBJCG("换货补寄成功",16, 16),
    HHBJSB("换货补寄失败",17, 17),
    HHYFHDYHQR("换货补寄待用户确认完成",18, 18),
    DYHJS("已同意拒收退款，待用户拒收",31, 31),
    THCG("退款成功",10,5);

    // 成员变量
    private String name;
    private int pddIndex;
    private int erpIndex;

    // 构造方法
    private EnumPddOrderRefundStatus(String name, int pddIndex,int erpIndex) {
        this.name = name;
        this.pddIndex = pddIndex;
        this.erpIndex=erpIndex;
    }

    // 普通方法
    public static String getName(int pddIndex) {
        for (EnumPddOrderRefundStatus c : EnumPddOrderRefundStatus.values()) {
            if (c.getPddIndex()==pddIndex) {
                return c.name;
            }
        }
        return null;
    }

    public static int getErpIndex(int pddIndex) {
        for (EnumPddOrderRefundStatus c : EnumPddOrderRefundStatus.values()) {
            if (c.getPddIndex()==pddIndex) {
                return c.erpIndex;
            }
        }
        return 0;
    }
    public static int getPddIndex(String name) {
        for (EnumPddOrderRefundStatus c : EnumPddOrderRefundStatus.values()) {
            if (c.toString().equals(name)) {
                return c.pddIndex;
            }
        }
        return 0;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPddIndex() {
        return pddIndex;
    }

    public void setPddIndex(int pddIndex) {
        this.pddIndex = pddIndex;
    }

    public int getErpIndex() {
        return erpIndex;
    }

    public void setErpIndex(int erpIndex) {
        this.erpIndex = erpIndex;
    }
}
