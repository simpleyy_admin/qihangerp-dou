package com.b2c.entity;

import java.math.BigDecimal;

/**
 * 描述：
 * erp系统订单明细
 *
 * @author qlp
 * @date 2019-09-17 11:55
 */
public class ErpOrderItemEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private Long orderId;//` bigint(20) NOT NULL COMMENT '订单id',
    private Long aliSubItemID;//阿里子订单id，用于阿里发货
    private String productMallName;//` varchar(50) DEFAULT NULL COMMENT '商城商品名',
    private Integer productId;//` int(11) NOT NULL COMMENT 'erp系统商品id',
    private String productNumber;//` varchar(20) NOT NULL COMMENT 'erp商品编码',
    private String skuNumber;//` varchar(30) NOT NULL DEFAULT '' COMMENT 'erp系统sku编码',
    private String skuName;//sku名
    private Long skuId;//erp系统goodsSpecId
    private String productImgUrl;//` varchar(100) DEFAULT NULL,
    private BigDecimal itemAmount;//子订单价格
    private Long quantity;//` bigint(20) NOT NULL,
    private Integer status;//` int(11) NOT NULL COMMENT '子订单状态,0未处理1拣货中2已拣货3已发货',
    private Long stockOutFormId;//出库单号,关联erp_stock_out_form
    private Long stockOutFormItemId;//出库单明细id，关联表erp_stock_out_form_item
    private String stockOutNo;//出库单号
    private Integer outLocationId;//出库仓库id，用于订单出库
    private String locationName;    //仓库名
    private String specNumber;      //Sku
    private String colorValue;
    private String colorImage;
    private String sizeValue;
    private Double price;
    private Double purPrice;
    private Long currentQty;//商品当前库存
    private Long lockedQty;//商品当前锁定的库存
    private Long pickingQty;//商品当前拣货中的库存
    private Long printQty;//打印占用库存


    
    public Long getStockOutFormItemId() {
        return stockOutFormItemId;
    }

    public void setStockOutFormItemId(Long stockOutFormItemId) {
        this.stockOutFormItemId = stockOutFormItemId;
    }

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getStockOutNo() {
        return stockOutNo;
    }

    public void setStockOutNo(String stockOutNo) {
        this.stockOutNo = stockOutNo;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Long getPickingQty() {
        return pickingQty;
    }

    public void setPickingQty(Long pickingQty) {
        this.pickingQty = pickingQty;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getAliSubItemID() {
        return aliSubItemID;
    }

    public void setAliSubItemID(Long aliSubItemID) {
        this.aliSubItemID = aliSubItemID;
    }

    public Integer getOutLocationId() {
        return outLocationId;
    }

    public void setOutLocationId(Integer outLocationId) {
        this.outLocationId = outLocationId;
    }

    public Long getStockOutFormId() {
        return stockOutFormId;
    }

    public void setStockOutFormId(Long stockOutFormId) {
        this.stockOutFormId = stockOutFormId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getProductMallName() {
        return productMallName;
    }

    public void setProductMallName(String productMallName) {
        this.productMallName = productMallName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPurPrice() {
        return purPrice;
    }

    public void setPurPrice(Double purPrice) {
        this.purPrice = purPrice;
    }

    public Long getPrintQty() {
        return printQty;
    }

    public void setPrintQty(Long printQty) {
        this.printQty = printQty;
    }
}
