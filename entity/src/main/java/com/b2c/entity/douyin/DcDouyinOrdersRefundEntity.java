package com.b2c.entity.douyin;

import java.util.List;

/**
 * 退货
 */
public class DcDouyinOrdersRefundEntity {
    private long id;
    private double postAmount;
    private long receiptTime;
    private String code;
    private long logisticsTime;
    private String userName;
    private int comboAmount;
    private String pid;
    private int logisticsId;
    private int orderStatus;
    private long updateTime;
    private int finalStatus;
    private String postTel;
    private int payType;
    private int orderType;
    private String postReceiver;
    private long createTime;
    private String specDesc;
    private String productPic;
    private String postCode;
    private String postAddr;
    private String logisticsCompany;
    private String logisticsCode;
    private String buyerWords;
    private String productName;
    private long payTime;
    private int comboNum;
    private long shopId;
    private String cancelReason;
    private String sellerWords;
    private double totalAmount;
    private String orderId;
    private String questionDesc;
    private long applyTime;
    //售后类型 ,0 售后退货退款1 售后退款2 售前退款3 换货
    private Integer aftersaleType;
    //退货售后状态，枚举为6(待商家同意),7(待买家退货),11(待商家二次同意),12(售后成功),13(换货待买家收货),14(换货成功),27(商家一次拒绝),28(售后失败),29(商家二次拒绝)
    private Integer refundStatus;
    //物流公司
    private String companyName;
    //是否确认0:否1：是
    private Integer auditStatus;
    //商品编码
    private String goodsNumber;
    //购买数量
    private Integer buyQuantity;
    //仓库退款状态
    private Integer stockStatus;
    //售后Id
    private Long aftersaleId;

    private List<DcDouyinOrdersRefundItemEntity> items;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }


    public long getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(long applyTime) {
        this.applyTime = applyTime;
    }

    public double getPostAmount() {
        return postAmount;
    }

    public void setPostAmount(double postAmount) {
        this.postAmount = postAmount;
    }

    public long getReceiptTime() {
        return receiptTime;
    }

    public void setReceiptTime(long receiptTime) {
        this.receiptTime = receiptTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getLogisticsTime() {
        return logisticsTime;
    }

    public void setLogisticsTime(long logisticsTime) {
        this.logisticsTime = logisticsTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getComboAmount() {
        return comboAmount;
    }

    public void setComboAmount(int comboAmount) {
        this.comboAmount = comboAmount;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(int logisticsId) {
        this.logisticsId = logisticsId;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public int getFinalStatus() {
        return finalStatus;
    }

    public void setFinalStatus(int finalStatus) {
        this.finalStatus = finalStatus;
    }

    public String getPostTel() {
        return postTel;
    }

    public void setPostTel(String postTel) {
        this.postTel = postTel;
    }

    public int getPayType() {
        return payType;
    }

    public void setPayType(int payType) {
        this.payType = payType;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public String getPostReceiver() {
        return postReceiver;
    }

    public void setPostReceiver(String postReceiver) {
        this.postReceiver = postReceiver;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getSpecDesc() {
        return specDesc;
    }

    public void setSpecDesc(String specDesc) {
        this.specDesc = specDesc;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostAddr() {
        return postAddr;
    }

    public void setPostAddr(String postAddr) {
        this.postAddr = postAddr;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public String getBuyerWords() {
        return buyerWords;
    }

    public void setBuyerWords(String buyerWords) {
        this.buyerWords = buyerWords;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getPayTime() {
        return payTime;
    }

    public void setPayTime(long payTime) {
        this.payTime = payTime;
    }

    public int getComboNum() {
        return comboNum;
    }

    public void setComboNum(int comboNum) {
        this.comboNum = comboNum;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getSellerWords() {
        return sellerWords;
    }

    public void setSellerWords(String sellerWords) {
        this.sellerWords = sellerWords;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }


    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getBuyQuantity() {
        return buyQuantity;
    }

    public void setBuyQuantity(Integer buyQuantity) {
        this.buyQuantity = buyQuantity;
    }

    public Integer getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(Integer stockStatus) {
        this.stockStatus = stockStatus;
    }

    public Integer getAftersaleType() {
        return aftersaleType;
    }

    public void setAftersaleType(Integer aftersaleType) {
        this.aftersaleType = aftersaleType;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getAftersaleId() {
        return aftersaleId;
    }

    public void setAftersaleId(Long aftersaleId) {
        this.aftersaleId = aftersaleId;
    }

    public List<DcDouyinOrdersRefundItemEntity> getItems() {
        return items;
    }

    public void setItems(List<DcDouyinOrdersRefundItemEntity> items) {
        this.items = items;
    }
}
