package com.b2c.entity.douyin;

import java.math.BigDecimal;
import java.util.List;

public class DcDouyinOrdersEntity {
    /**
     * 订单id，自增
     */
    private Long id;

    /**
     * 抖音订单id
     */
    private String orderId;

    /**
     * 订单所属商户id
     */
    private Long shopId;

    /**
     * 买家用户名
     */
    private String userName;

    /**
     * 邮寄地址 (展开为省市区json， 格式参考 订单-获取订单列表 返回示例)
     */
    private String postAddr;

    /**
     * 邮政编码
     */
    private String postCode;

    /**
     * 收件人姓名
     */
    private String postReceiver;

    /**
     * 收件人电话
     */
    private String postTel;

    /**
     * 买家备注
     */
    private String buyerWords;

    /**
     * 卖家备注
     */
    private String sellerWords;

    /**
     * 物流公司id
     */
    private String logisticsId;

    /**
     * 物流单号
     */
    private String logisticsCode;

    /**
     * 发货时间
     */
    private String logisticsTime;

    /**
     * 收货时间
     */
    private Long receiptTime;

    /**
     * 订单状态
     */
    private Integer orderStatus;

    /**
     * 订单创建时间
     */
    private String createTime;

    /**
     * 最晚发货时间
     */
    private String expShipTime;


    /**
     * 订单取消原因
     */
    private String cancelReason;

    /**
     * 支付类型 (0：货到付款，1：微信，2：支付宝)
     */
    private Integer payType;

    /**
     * 支付时间 (pay_type为0货到付款时， 此字段为空)
     */
    private String payTime;

    /**
     * 邮费金额 (单位: 分)
     */
    private Double postAmount;

    /**
     * 平台优惠券金额 (单位: 分)
     */
    private Double couponAmount;

    /**
     * 商家优惠券金额 (单位: 分)
     */
    private Double shopCouponAmount;
    private Long postInsuranceAmount;//运费险金额

    /**
     * 优惠券id
     */
    private Integer couponMetaId;

    /**
     * 优惠券详情 (type为优惠券类型， credit为优惠金额，单位分)
     */
    private String couponInfo;

    /**
     * 父订单总金额 (单位: 分)
     */
    private Double orderTotalAmount;

    private Double orderAmount;

    /**
     * int(11) not null comment 是否评价 (1:已评价)
     */
    private Integer isComment;

    /**
     * 催单次数
     */
    private Integer urgeCnt;

    /**
     * 订单佣金 (详情见附录)
     */
    private Integer cType;

    /**
     * 订单渠道 (站外0 火山1 抖音2 头条3 西瓜4 微信5 闪购6 头条lite版本7 懂车帝8 皮皮虾9)
     */
    private Integer bType;

    /**
     * 佣金率
     */
    private BigDecimal cosRatio;

    /**
     * 创建时间
     */
    private String createOn;
    /**
     * 发货时间
     */
    private String sendTime;
    /**
     *订单审核状态
     */
    private Integer auditStatus;
    /**
     * 发货状态（0待出库1拣货中2已拣货3已出库4已发货）
     */
    private Integer sendStatus;
    /**
     * 物流公司
     */
    private String logisticsCompany;

    private String province;
    private String town;
    private String city;
    private String street;
    private String encryptDetail;

    private Long shipTime;
    /**
     * 【交易类型】 0、普通 1、拼团 2、定金预售 3、订金找贷 4、拍卖 5、0元单 6、回收 7、寄卖
     */
    private Integer tradeType;

    private String encrypt_post_tel;
    private String encrypt_post_receiver;
    private Integer printStatus;
    private String phoneKey;
    private String addressKey;

    private String shopName;
    

    /**
     * 子订单列表
     */
    private List<DcDouyinOrdersItemsEntity> skuOrderList;

    public Long getPostInsuranceAmount() {
        return postInsuranceAmount;
    }

    public void setPostInsuranceAmount(Long postInsuranceAmount) {
        this.postInsuranceAmount = postInsuranceAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostAddr() {
        return postAddr;
    }

    public void setPostAddr(String postAddr) {
        this.postAddr = postAddr;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostReceiver() {
        return postReceiver;
    }

    public void setPostReceiver(String postReceiver) {
        this.postReceiver = postReceiver;
    }

    public String getPostTel() {
        return postTel;
    }

    public void setPostTel(String postTel) {
        this.postTel = postTel;
    }

    public String getBuyerWords() {
        return buyerWords;
    }

    public void setBuyerWords(String buyerWords) {
        this.buyerWords = buyerWords;
    }

    public String getSellerWords() {
        return sellerWords;
    }

    public void setSellerWords(String sellerWords) {
        this.sellerWords = sellerWords;
    }

    public String getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(String logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public String getLogisticsTime() {
        return logisticsTime;
    }

    public void setLogisticsTime(String logisticsTime) {
        this.logisticsTime = logisticsTime;
    }

    public Long getReceiptTime() {
        return receiptTime;
    }

    public void setReceiptTime(Long receiptTime) {
        this.receiptTime = receiptTime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }



    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }



    public Double getPostAmount() {
        return postAmount;
    }

    public void setPostAmount(Double postAmount) {
        this.postAmount = postAmount;
    }

    public Double getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(Double couponAmount) {
        this.couponAmount = couponAmount;
    }

    public Double getShopCouponAmount() {
        return shopCouponAmount;
    }

    public void setShopCouponAmount(Double shopCouponAmount) {
        this.shopCouponAmount = shopCouponAmount;
    }

    public Integer getCouponMetaId() {
        return couponMetaId;
    }

    public void setCouponMetaId(Integer couponMetaId) {
        this.couponMetaId = couponMetaId;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Double getOrderTotalAmount() {
        return orderTotalAmount;
    }

    public void setOrderTotalAmount(Double orderTotalAmount) {
        this.orderTotalAmount = orderTotalAmount;
    }

    public Integer getIsComment() {
        return isComment;
    }

    public void setIsComment(Integer isComment) {
        this.isComment = isComment;
    }

    public Integer getUrgeCnt() {
        return urgeCnt;
    }

    public void setUrgeCnt(Integer urgeCnt) {
        this.urgeCnt = urgeCnt;
    }

    public Integer getcType() {
        return cType;
    }

    public void setcType(Integer cType) {
        this.cType = cType;
    }

    public Integer getbType() {
        return bType;
    }

    public void setbType(Integer bType) {
        this.bType = bType;
    }

    public BigDecimal getCosRatio() {
        return cosRatio;
    }

    public void setCosRatio(BigDecimal cosRatio) {
        this.cosRatio = cosRatio;
    }


    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public List<DcDouyinOrdersItemsEntity> getSkuOrderList() {
        return skuOrderList;
    }

    public void setSkuOrderList(List<DcDouyinOrdersItemsEntity> skuOrderList) {
        this.skuOrderList = skuOrderList;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getEncryptDetail() {
        return encryptDetail;
    }

    public void setEncryptDetail(String encryptDetail) {
        this.encryptDetail = encryptDetail;
    }

    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public String getEncrypt_post_tel() {
        return encrypt_post_tel;
    }

    public void setEncrypt_post_tel(String encrypt_post_tel) {
        this.encrypt_post_tel = encrypt_post_tel;
    }

    public String getEncrypt_post_receiver() {
        return encrypt_post_receiver;
    }

    public void setEncrypt_post_receiver(String encrypt_post_receiver) {
        this.encrypt_post_receiver = encrypt_post_receiver;
    }

    public Integer getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(Integer printStatus) {
        this.printStatus = printStatus;
    }

    public String getPhoneKey() {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getExpShipTime() {
        return expShipTime;
    }

    public void setExpShipTime(String expShipTime) {
        this.expShipTime = expShipTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

}
