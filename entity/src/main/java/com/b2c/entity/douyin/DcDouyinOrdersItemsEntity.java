package com.b2c.entity.douyin;

public class DcDouyinOrdersItemsEntity {
    /**
     * id，自增
     */
    private Long id;

    /**
     * 抖音订单id
     */
    private String orderId;
    private String subOrderId;

    /**
     * 订单所属商户id
     */
    private Long shopId;


    /**
     * 该子订单购买的商品id
     */
    private String productId;

    /**
     * 商品名称
     */
    private String productName;
    private String erpGoodsName;

    public String getErpGoodsName() {
        return erpGoodsName;
    }

    public void setErpGoodsName(String erpGoodsName) {
        this.erpGoodsName = erpGoodsName;
    }
    /**
     * 商品图片 (spu维度的商品主图)
     */
    private String productPic;
    /**
     * 该子订单购买的商品 sku_id
     */
    private Long comboId;

    /**
     * 该子订单购买的商品的编码 code
     */
    private String specCode;
    /**
     * 商品编码
     */
    private String goodsNumber;
    /**
     * 该子订单所属商品规格描述
     */
    private String specDesc;

    /**
     * 该子订单所购买的sku的数量
     */
    private Integer comboNum;

    /**
     * 邮费金额 (单位: 分)
     */
    private Double postAmount;

    /**
     * 平台优惠券金额 (单位: 分)
     */
    private Double couponAmount;

    /**
     * 商家优惠券金额 (单位: 分)
     */
    private Double shopCouponAmount;

    /**
     * 优惠券id
     */
    private Long couponMetaId;

    /**
     * 优惠券详情 (type为优惠券类型， credit为优惠金额，单位分)
     */
    private String couponInfo;



    /**
     * 活动细则 (活动可能会导致商品成交价combo_amount变成活动sku价格 ，活动campaign_info字段中的title为活动标题)
     */
    private String campaignInfo;
    /**
     * 该子订单总金额 (单位: 分)
     */
    private Double totalAmount;

    /**
     * int(11) not null comment 是否评价 (1:已评价)
     */
    private Integer isComment;
    /**
     * erp商品id
     */
    private Integer erpGoodsId;
    /**
     * erp商品规格id
     */
    private Integer erpGoodsSpecId;
    private String erpGoodsSpecImg;
    private Double goodsPurPrice;
    
    /**
     * 商品单价
     */
    private Double price;
    /**
     * 修改备注
     */
    private String remark;
    /**
     * erp 规格信息
      */
    private String  skuInfo;
    private Integer isGift;

    private Double orderAmount;

    private Integer itemNum;

    private Long currentQty;

    private Long pickingQty;

    private String province;
    private String town;
    private String city;

    private Long createTime;

    private Long expShipTime;

    private Integer orderStatus;
    private String logisticsCompany;
    private String  logisticsCode;
    private String logisticsId;
    private String result;
    private String printDate;
    private String locationNumber;

    private String spec;
    private String buyerWords;
    private String sellerWords;
    private String phoneKey;
    private String addressKey;
    private String street;
    private String encryptPostTel;
    private String encryptPostReceiver;
    private String encryptDetail;
    private Long authorId;
    private String authorName;
    public Integer getIsGift() {
        return isGift;
    }
    private Integer itemStatus;//子订单状态

    public Integer getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(Integer itemStatus) {
        this.itemStatus = itemStatus;
    }

    public void setIsGift(Integer isGift) {
        this.isGift = isGift;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

   

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public Long getComboId() {
        return comboId;
    }

    public void setComboId(Long comboId) {
        this.comboId = comboId;
    }

   

    public String getSpecDesc() {
        return specDesc;
    }

    public void setSpecDesc(String specDesc) {
        this.specDesc = specDesc;
    }

    public Integer getComboNum() {
        return comboNum;
    }

    public void setComboNum(Integer comboNum) {
        this.comboNum = comboNum;
    }

    public Double getPostAmount() {
        return postAmount;
    }

    public void setPostAmount(Double postAmount) {
        this.postAmount = postAmount;
    }

    public Double getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(Double couponAmount) {
        this.couponAmount = couponAmount;
    }

    public Double getShopCouponAmount() {
        return shopCouponAmount;
    }

    public void setShopCouponAmount(Double shopCouponAmount) {
        this.shopCouponAmount = shopCouponAmount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getIsComment() {
        return isComment;
    }

    public void setIsComment(Integer isComment) {
        this.isComment = isComment;
    }

    public Long getCouponMetaId() {
        return couponMetaId;
    }

    public void setCouponMetaId(Long couponMetaId) {
        this.couponMetaId = couponMetaId;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }


    public String getCampaignInfo() {
        return campaignInfo;
    }

    public void setCampaignInfo(String campaignInfo) {
        this.campaignInfo = campaignInfo;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getErpGoodsId() {
        return erpGoodsId;
    }

    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }

    public Integer getErpGoodsSpecId() {
        return erpGoodsSpecId;
    }

    public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
        this.erpGoodsSpecId = erpGoodsSpecId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSkuInfo() {
        return skuInfo;
    }

    public void setSkuInfo(String skuInfo) {
        this.skuInfo = skuInfo;
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getItemNum() {
        return itemNum;
    }

    public void setItemNum(Integer itemNum) {
        this.itemNum = itemNum;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getPickingQty() {
        return pickingQty;
    }

    public void setPickingQty(Long pickingQty) {
        this.pickingQty = pickingQty;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getExpShipTime() {
        return expShipTime;
    }

    public void setExpShipTime(Long expShipTime) {
        this.expShipTime = expShipTime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(String logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getPrintDate() {
        return printDate;
    }

    public void setPrintDate(String printDate) {
        this.printDate = printDate;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getBuyerWords() {
        return buyerWords;
    }

    public void setBuyerWords(String buyerWords) {
        this.buyerWords = buyerWords;
    }

    public String getSellerWords() {
        return sellerWords;
    }

    public void setSellerWords(String sellerWords) {
        this.sellerWords = sellerWords;
    }

   

    public String getPhoneKey() {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getEncryptPostTel() {
        return encryptPostTel;
    }

    public void setEncryptPostTel(String encryptPostTel) {
        this.encryptPostTel = encryptPostTel;
    }

    public String getEncryptPostReceiver() {
        return encryptPostReceiver;
    }

    public void setEncryptPostReceiver(String encryptPostReceiver) {
        this.encryptPostReceiver = encryptPostReceiver;
    }

    public String getEncryptDetail() {
        return encryptDetail;
    }

    public void setEncryptDetail(String encryptDetail) {
        this.encryptDetail = encryptDetail;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getSubOrderId() {
        return subOrderId;
    }

    public void setSubOrderId(String subOrderId) {
        this.subOrderId = subOrderId;
    }

    public String getSpecCode() {
        return specCode;
    }

    public void setSpecCode(String specCode) {
        this.specCode = specCode;
    }

    public String getErpGoodsSpecImg() {
        return erpGoodsSpecImg;
    }

    public void setErpGoodsSpecImg(String erpGoodsSpecImg) {
        this.erpGoodsSpecImg = erpGoodsSpecImg;
    }

    public Double getGoodsPurPrice() {
        return goodsPurPrice;
    }

    public void setGoodsPurPrice(Double goodsPurPrice) {
        this.goodsPurPrice = goodsPurPrice;
    }

    
}
