package com.b2c.entity.douyin;

import java.math.BigDecimal;

public class DouyinOrderStatis {
    private String zbId;
    private String zbName;
    private Long orderCount;
    private BigDecimal orderAmount;
    private Long goodNum;

    public String getZbName() {
        return zbName;
    }

    public void setZbName(String zbName) {
        this.zbName = zbName;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Long getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(Long goodNum) {
        this.goodNum = goodNum;
    }

    public String getZbId() {
        return zbId;
    }

    public void setZbId(String zbId) {
        this.zbId = zbId;
    }
}
