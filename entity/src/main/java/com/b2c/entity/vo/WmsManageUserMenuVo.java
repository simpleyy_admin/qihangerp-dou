package com.b2c.entity.vo;

import java.util.List;

/**
 * @Description: pbd add 2019/9/28 14:42
 */
public class WmsManageUserMenuVo {
    /**
     * 菜单id
     */
    private Integer id;
    /**
     * 父级菜单id
     */
    private Integer parentId;
    private String parentKey;//父级菜单key
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 菜单标识
     */
    private String permissionKey;
    /**
     * 菜单链接
     */
    private String url;
    /**
     * 是否菜单0是1否
     */
    private Integer isMenu;
    /**
     * 菜单状态：0正常 1禁用
     */
    private Integer status;

    private String icon;
    /**
     * 子菜单
     */
    private List<WmsManageUserMenuVo> chirdMenus;

    
    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(Integer isMenu) {
        this.isMenu = isMenu;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    public List<WmsManageUserMenuVo> getChirdMenus() {
        return chirdMenus;
    }

    public void setChirdMenus(List<WmsManageUserMenuVo> chirdMenus) {
        this.chirdMenus = chirdMenus;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
