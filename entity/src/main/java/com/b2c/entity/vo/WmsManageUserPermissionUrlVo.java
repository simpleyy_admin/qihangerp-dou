package com.b2c.entity.vo;

/**
 * @Description:用户权限菜单访问地址
 * pbd add 2019/10/2 11:29
 */
public class WmsManageUserPermissionUrlVo {
    private String requestUrl;

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }
}
