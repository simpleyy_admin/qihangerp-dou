package com.b2c.entity.vo;

public class OrderRefundApplyItemVo {
    private Long orderItemId;//订单item Id
    private Integer returnCount;//退货数量

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getReturnCount() {
        return returnCount;
    }

    public void setReturnCount(Integer returnCount) {
        this.returnCount = returnCount;
    }
}
