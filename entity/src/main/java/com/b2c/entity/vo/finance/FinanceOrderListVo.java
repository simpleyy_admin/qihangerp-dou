package com.b2c.entity.vo.finance;

import java.math.BigDecimal;
import java.util.Date;

public class FinanceOrderListVo {
    private Long orderId;
    private String orderNum;//订单id
    private Long totalQuantity;
    private BigDecimal totalAmount;//订单金额
    private BigDecimal shippingFee;//运费金额
    private Date createTime;//下单时间
    private Long orderTime;//下单时间
    private Integer payStatus;
    private Integer payMethod;
    private Long payTime;//付款时间
    private String payVoucher;
    private BigDecimal payAmount;
//    private Date payDateTime;//阿里、天猫付款时间

//    private Integer sendStatus;//发货状态
//    private Long sendTime;//发货时间，仓库真实发货时间
    private Integer deliveredStatus;
    private Long deliveredTime;//发货时间，仓库真实发货时间
    private Integer status;//订单状态

    private Integer shopId;//店铺id
    private String shopName;//店铺名
    private String logisticsCompany;//快递公司
    private String logisticsCode;//快递单号
//    private Integer shopType;//店铺类型，用于云购订单
    private Integer saleType;//采购订单类型（0样品1销售），用于云购订单
    private Integer financialStatus;//财务对账状态0未对账1已对账

    private String buyerName;//客户名
    private String developer;//业务员姓名
    private String developerMobile;//业务员手机号
    private String contactPerson;//收货人
    private String contactMobile;//收货人手机号
    private String address;//收货人地址

    private String sellerMemo;


    public Integer getFinancialStatus() {
        return financialStatus;
    }

    public void setFinancialStatus(Integer financialStatus) {
        this.financialStatus = financialStatus;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(Long deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }


    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }


    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(Integer payMethod) {
        this.payMethod = payMethod;
    }

    public String getPayVoucher() {
        return payVoucher;
    }

    public void setPayVoucher(String payVoucher) {
        this.payVoucher = payVoucher;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getDeliveredStatus() {
        return deliveredStatus;
    }

    public void setDeliveredStatus(Integer deliveredStatus) {
        this.deliveredStatus = deliveredStatus;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getDeveloperMobile() {
        return developerMobile;
    }

    public void setDeveloperMobile(String developerMobile) {
        this.developerMobile = developerMobile;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
