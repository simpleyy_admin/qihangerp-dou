package com.b2c.entity.vo;

/**
 * 描述：
 * 验货单入库items
 *
 * @author qlp
 * @date 2019-05-30 17:41
 */
public class ErpCheckoutStockInItemVo {
//    private Long checkoutId;//验货单id
    private Long checkoutItemId;//验货单item id
    private Integer goodsId;//商品id

    private Integer specId;//规格id
    private String specNumber;//规格编码
    private Integer locationId;//入库仓位id
    private String locationNumber;//入库仓位编码
    private Long quantity;//入库数量，入库时，以这个数量为准

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getCheckoutItemId() {
        return checkoutItemId;
    }

    public void setCheckoutItemId(Long checkoutItemId) {
        this.checkoutItemId = checkoutItemId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
