package com.b2c.entity.vo;

import com.b2c.entity.GoodsSpecAttrEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述：
 * 商品添加VO
 *
 * @author qlp
 * @date 2018-12-25 4:01 PM
 */
public class GoodsAddVo {
    private Integer categoryId;
    private String title;
    private String deputyTitle;
    private BigDecimal price;
    private BigDecimal costPrice;//采购价格
    //    private Integer inventory;
    private Integer saleType;//销售模式1现售2预售
    private Integer sendDays;//预计发货天数（预售生效）
    private String goodsNumber;//仓库商品编码
    private String mDetail;
    private String pcDetail;
    private Integer freightTemplate;
    private Integer time;//上架时间，1立即上架2放入商品库
    private String image;//商品主图
    private String[] images;//商品图片
    private String video;//视频链接
    //    private String videoImg;//视频图片
    private List<GoodsAttrVo> goodsAttr;//商品属性
    private List<GoodsSpecVo> goodsSpec;//商品规格
    private String keyword;//商品关键词
    private Integer saleNumShow;//用于展示销量
    private BigDecimal commisionRate;//佣金比例
    private Integer erpGoodsId;//erp商品ID

    private Long goodTotalQty;

    private List<GoodsSpecAttrEntity> sizes;//尺码list
    private List<GoodsSpecAttrEntity> colors;//颜色list
    private List<GoodsSpecAttrEntity> styles;//款式list

    public Integer getErpGoodsId() {
        return erpGoodsId;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public Integer getSendDays() {
        return sendDays;
    }

    public void setSendDays(Integer sendDays) {
        this.sendDays = sendDays;
    }

    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }

    public List<GoodsSpecAttrEntity> getSizes() {
        return sizes;
    }

    public void setSizes(List<GoodsSpecAttrEntity> sizes) {
        this.sizes = sizes;
    }

    public List<GoodsSpecAttrEntity> getColors() {
        return colors;
    }

    public void setColors(List<GoodsSpecAttrEntity> colors) {
        this.colors = colors;
    }

    public List<GoodsSpecAttrEntity> getStyles() {
        return styles;
    }

    public void setStyles(List<GoodsSpecAttrEntity> styles) {
        this.styles = styles;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

//    public String getVideoImg() {
//        return videoImg;
//    }
//
//    public void setVideoImg(String videoImg) {
//        this.videoImg = videoImg;
//    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
//
//    public Integer getInventory() {
//        return inventory;
//    }
//
//    public void setInventory(Integer inventory) {
//        this.inventory = inventory;
//    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getmDetail() {
        return mDetail;
    }

    public void setmDetail(String mDetail) {
        this.mDetail = mDetail;
    }

    public String getPcDetail() {
        return pcDetail;
    }

    public void setPcDetail(String pcDetail) {
        this.pcDetail = pcDetail;
    }

    public Integer getFreightTemplate() {
        return freightTemplate;
    }

    public void setFreightTemplate(Integer freightTemplate) {
        this.freightTemplate = freightTemplate;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public List<GoodsAttrVo> getGoodsAttr() {
        return goodsAttr;
    }

    public void setGoodsAttr(List<GoodsAttrVo> goodsAttr) {
        this.goodsAttr = goodsAttr;
    }

    public List<GoodsSpecVo> getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(List<GoodsSpecVo> goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public Integer getSaleNumShow() {
        return saleNumShow;
    }

    public void setSaleNumShow(Integer saleNumShow) {
        this.saleNumShow = saleNumShow;
    }

    public String getDeputyTitle() {
        return deputyTitle;
    }

    public void setDeputyTitle(String deputyTitle) {
        this.deputyTitle = deputyTitle;
    }

    public BigDecimal getCommisionRate() {
        return commisionRate;
    }

    public void setCommisionRate(BigDecimal commisionRate) {
        this.commisionRate = commisionRate;
    }

    public Long getGoodTotalQty() {
        return goodTotalQty;
    }

    public void setGoodTotalQty(Long goodTotalQty) {
        this.goodTotalQty = goodTotalQty;
    }
}
