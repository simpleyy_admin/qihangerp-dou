package com.b2c.entity.vo;

import com.b2c.entity.ErpStockInCheckoutItemEntity;

import java.util.List;

/**
 * 描述：
 * 添加入库检验表单vo
 *
 * @author qlp
 * @date 2019-09-29 11:06
 */
public class ErpStockInCheckoutFormAddVo {
    private Long invoiceId;
    private String invoiceNo;
    private String number;
    private int hasQcReport;//是否有qc报告
    private String qcInspector;//qc报告人
//    private String[] specNumberArray;//商品规格数组
//    private Long[] quantityArray;//检验的数量数组
    private Integer checkoutUserId;
    private String checkoutUserName;
    private List<ErpStockInCheckoutItemEntity> items;

    public List<ErpStockInCheckoutItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpStockInCheckoutItemEntity> items) {
        this.items = items;
    }

    public Integer getCheckoutUserId() {
        return checkoutUserId;
    }

    public void setCheckoutUserId(Integer checkoutUserId) {
        this.checkoutUserId = checkoutUserId;
    }

    public String getCheckoutUserName() {
        return checkoutUserName;
    }

    public void setCheckoutUserName(String checkoutUserName) {
        this.checkoutUserName = checkoutUserName;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getHasQcReport() {
        return hasQcReport;
    }

    public void setHasQcReport(int hasQcReport) {
        this.hasQcReport = hasQcReport;
    }

    public String getQcInspector() {
        return qcInspector;
    }

    public void setQcInspector(String qcInspector) {
        this.qcInspector = qcInspector;
    }

}
