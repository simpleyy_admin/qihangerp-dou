package com.b2c.entity;

/**
 * 描述：
 * 仓库Entity
 *
 * @author qlp
 * @date 2019-03-21 18:13
 */
public class ErpStockLocationEntity {
    private Integer id;
    private String number;
    private String name;
    private Integer parentId1;//一级id
    private Integer parentId2;//二级id
    private String name1;//一级名称
    private String name2;//二级名称
    private Integer parentId;
    private Integer depth;
    private Integer isDelete;
    private String address;
    private Long createTime;

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getParentId1() {
        return parentId1;
    }

    public void setParentId1(Integer parentId1) {
        this.parentId1 = parentId1;
    }

    public Integer getParentId2() {
        return parentId2;
    }

    public void setParentId2(Integer parentId2) {
        this.parentId2 = parentId2;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
}
