package com.b2c.entity.erp.vo;

/**
 * @Description: pbd add 2019/6/17 9:22
 */
public class PrintImgVo {

    /**
     * 收件网点名称,由快递公司当地网点分配，若使用淘宝授权填入（taobao），使用菜鸟授权填入（cainiao）
     */
    private String net;
    private String kuaidicomname;//快递公司名
    /**
     * 快递公司编码
     */
    private String kuaidicom;
    /**
     * 收件人姓名
     */
    private String recManName;
    /**
     * 收件人手机号
     */
    private String recManMobile;
    /**
     * 收件人省份
     */
    private String recManProvince;
    /**
     * 收件人城市
     */
    private String recManCity;
    /**
     * 收件人所在区
     */
    private String recManDistrict;
    /**
     * 收件人地址
     */
    private String recManAddr;
    /**
     * 收件人所在完整地址
     */
    private String recManPrintAddr;
    /**
     * 订单编号
     */
    private String orderId;
    /**
     * 物品名称
     */
    private String cargo;
    /**
     * 物品总数量
     */
    private Integer count;
    /**
     * 物品总重量kg
     */
    private Double weight;
    /**
     * 物品总体积
     */
    private double volumn;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否需要回单：1:需要、 0:不需要(默认)，非必填；返回的回单号见返回结果的returnNum字段
     */
    private Integer needBack;
    /**
     * 是否开启订阅功能，默认是：0（不开启），如果是1说明开启订阅功能此时pollCallBackUrl必须填入，此功能只针对有快递单号的单
     */
    private Integer op;
    /**
     * 回调地址。如果op设置为1时，pollCallBackUrl必须填入
     */
    private String pollCallBackUrl;

    /**
     * 发货地址信息
     */
    private String sendManName;
    private String sendManMobile;
    private String sendManPrintAddr;

    public String getKuaidicomname() {
        return kuaidicomname;
    }

    public void setKuaidicomname(String kuaidicomname) {
        this.kuaidicomname = kuaidicomname;
    }

    public String getSendManName() {
        return sendManName;
    }

    public void setSendManName(String sendManName) {
        this.sendManName = sendManName;
    }

    public String getSendManMobile() {
        return sendManMobile;
    }

    public void setSendManMobile(String sendManMobile) {
        this.sendManMobile = sendManMobile;
    }

    public String getSendManPrintAddr() {
        return sendManPrintAddr;
    }

    public void setSendManPrintAddr(String sendManPrintAddr) {
        this.sendManPrintAddr = sendManPrintAddr;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getKuaidicom() {
        return kuaidicom;
    }

    public void setKuaidicom(String kuaidicom) {
        this.kuaidicom = kuaidicom;
    }

    public String getRecManName() {
        return recManName;
    }

    public void setRecManName(String recManName) {
        this.recManName = recManName;
    }

    public String getRecManMobile() {
        return recManMobile;
    }

    public void setRecManMobile(String recManMobile) {
        this.recManMobile = recManMobile;
    }

    public String getRecManProvince() {
        return recManProvince;
    }

    public void setRecManProvince(String recManProvince) {
        this.recManProvince = recManProvince;
    }

    public String getRecManCity() {
        return recManCity;
    }

    public void setRecManCity(String recManCity) {
        this.recManCity = recManCity;
    }

    public String getRecManDistrict() {
        return recManDistrict;
    }

    public void setRecManDistrict(String recManDistrict) {
        this.recManDistrict = recManDistrict;
    }

    public String getRecManAddr() {
        return recManAddr;
    }

    public void setRecManAddr(String recManAddr) {
        this.recManAddr = recManAddr;
    }

    public String getRecManPrintAddr() {
        return recManPrintAddr;
    }

    public void setRecManPrintAddr(String recManPrintAddr) {
        this.recManPrintAddr = recManPrintAddr;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public double getVolumn() {
        return volumn;
    }

    public void setVolumn(double volumn) {
        this.volumn = volumn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getNeedBack() {
        return needBack;
    }

    public void setNeedBack(Integer needBack) {
        this.needBack = needBack;
    }

    public Integer getOp() {
        return op;
    }

    public void setOp(Integer op) {
        this.op = op;
    }

    public String getPollCallBackUrl() {
        return pollCallBackUrl;
    }

    public void setPollCallBackUrl(String pollCallBackUrl) {
        this.pollCallBackUrl = pollCallBackUrl;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
