package com.b2c.entity.erp;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-05-31 16:00
 */
public class InvoicePickingEntity {
    private Long id;
    private String number;
    private String iiids;//invoice_info_id
    private Integer status;//拣货状态0待打印1已打印2已拣货
    private Integer count;//商品件数
    private Integer quantity;
    private Long createTime;
    private Long printTime;
    private Integer printCount;//打印次数
    private Long completeTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIiids() {
        return iiids;
    }

    public void setIiids(String iiids) {
        this.iiids = iiids;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getPrintTime() {
        return printTime;
    }

    public void setPrintTime(Long printTime) {
        this.printTime = printTime;
    }

    public Integer getPrintCount() {
        return printCount;
    }

    public void setPrintCount(Integer printCount) {
        this.printCount = printCount;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }
}
