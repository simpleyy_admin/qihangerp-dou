package com.b2c.entity.erp.enums;

/**
 * 描述：订单拣货出库状态
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum InvoiceInfoStatusEnum {
    Wait("待拣货", 0),
    Picking("拣货中", 1),//已经打印拣货单
    Picked("已拣货", 21),//已拣货等待打印快递面单
    Out("已出库", 23);

    private String name;
    private int index;

    // 构造方法
    private InvoiceInfoStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (InvoiceInfoStatusEnum c : InvoiceInfoStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
