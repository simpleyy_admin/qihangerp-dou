package com.b2c.entity.erp.vo;

import com.b2c.entity.erp.GoodsCategoryEntity;

import java.util.List;

/**
 * 描述：
 * 库存商品分类实体
 *
 * @author ly
 * @date 2019-3-21 11:57 AM
 */
public class GoodsCategoryVo {
    private GoodsCategoryEntity entity;
    private List<GoodsCategoryVo> list;

    public GoodsCategoryEntity getEntity() {
        return entity;
    }

    public void setEntity(GoodsCategoryEntity entity) {
        this.entity = entity;
    }

    public List<GoodsCategoryVo> getList() {
        return list;
    }

    public void setList(List<GoodsCategoryVo> list) {
        this.list = list;
    }
}
