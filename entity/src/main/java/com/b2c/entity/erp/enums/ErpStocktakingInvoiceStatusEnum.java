package com.b2c.entity.erp.enums;

/**
 * 盘点单状态
 */
public enum ErpStocktakingInvoiceStatusEnum {
    //'状态 0 未审核 1待盘点录入2已录入3已完成4已取消',
    WaitAudit("待审核", 0),
    WaitCounted ("待盘点录入", 1),
    WaitCountedConfirm("待盘点确认", 2),
    SUCCESS("盘点已完成", 3),
    CANCEL("已取消", 4),

    NOTFUND("未知", 99);

    private String name;
    private int index;

    // 构造方法
    private ErpStocktakingInvoiceStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (ErpStocktakingInvoiceStatusEnum c : ErpStocktakingInvoiceStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
