package com.b2c.entity.erp;

public class ErpSalesOrderRefundItemEntity {
    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
    private Long refundId;//` BIGINT(20) NOT NULL COMMENT '退货订单id',
    private Long orderId;//` BIGINT(20) NOT NULL COMMENT '相关订单id（orders）',
    private Long orderItemId;//` BIGINT(20) NOT NULL COMMENT '相关订单itemId',
    private Integer quantity;//` INT(11) NOT NULL COMMENT '退货数量',

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRefundId() {
        return refundId;
    }

    public void setRefundId(Long refundId) {
        this.refundId = refundId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
