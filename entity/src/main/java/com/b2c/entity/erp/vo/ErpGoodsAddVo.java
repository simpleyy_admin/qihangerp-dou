package com.b2c.entity.erp.vo;

import com.b2c.entity.GoodsSpecAttrEntity;

import java.util.List;

/**
 * 描述：
 * 仓库系统商品添加VO
 *
 * @author qlp
 * @date 2019-07-01 16:12
 */
public class ErpGoodsAddVo {
    private Integer id;
    private String title;
    private String number;
    private String category;
    private String brand;
    private Integer categoryId;
    private Integer unitId;
    private Integer locationId;
    private double salePrice;
    private double costPrice;
    private double freight;//运费
    private String image;
    private String sizeImg;//尺码表
    private String attr1;
    private String attr2;
    private String attr3;
    private String attr4;
    private String attr5;
    private String remark;
    private Float weight;//重量
    private Float length;//衣长/裙长
    private Float height;//袖长/高度
    private Float width1;//肩阔
    private Float width;//胸阔/宽度
    private Float width2;//腰阔
    private Float width3;//臀阔
    private Integer erpContactId;//供应商

    public double getFreight() {
        return freight;
    }

    public void setFreight(double freight) {
        this.freight = freight;
    }

    public String getSizeImg() {
        return sizeImg;
    }

    public void setSizeImg(String sizeImg) {
        this.sizeImg = sizeImg;
    }

    public Integer getErpContactId() {
        return erpContactId;
    }

    public void setErpContactId(Integer erpContactId) {
        this.erpContactId = erpContactId;
    }

    public String getAttr5() {
        return attr5;
    }

    public void setAttr5(String attr5) {
        this.attr5 = attr5;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWidth1() {
        return width1;
    }

    public void setWidth1(Float width1) {
        this.width1 = width1;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getWidth2() {
        return width2;
    }

    public void setWidth2(Float width2) {
        this.width2 = width2;
    }

    public Float getWidth3() {
        return width3;
    }

    public void setWidth3(Float width3) {
        this.width3 = width3;
    }

    public String getAttr4() {
        return attr4;
    }

    public void setAttr4(String attr4) {
        this.attr4 = attr4;
    }

    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    public String getAttr3() {
        return attr3;
    }

    public void setAttr3(String attr3) {
        this.attr3 = attr3;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(double costPrice) {
        this.costPrice = costPrice;
    }

    //    private Integer reservoirId;
//    private Integer shelfId;
    private List<ErpGoodsAddSpecVo> specList;
    private List<GoodsSpecAttrEntity> sizes;//尺码list
    private List<GoodsSpecAttrEntity> colors;//颜色list
    private List<GoodsSpecAttrEntity> styles;//款式list

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

//    public Integer getReservoirId() {
//        return reservoirId;
//    }
//
//    public void setReservoirId(Integer reservoirId) {
//        this.reservoirId = reservoirId;
//    }
//
//    public Integer getShelfId() {
//        return shelfId;
//    }
//
//    public void setShelfId(Integer shelfId) {
//        this.shelfId = shelfId;
//    }

    public List<ErpGoodsAddSpecVo> getSpecList() {
        return specList;
    }

    public void setSpecList(List<ErpGoodsAddSpecVo> specList) {
        this.specList = specList;
    }

    public List<GoodsSpecAttrEntity> getSizes() {
        return sizes;
    }

    public void setSizes(List<GoodsSpecAttrEntity> sizes) {
        this.sizes = sizes;
    }

    public List<GoodsSpecAttrEntity> getColors() {
        return colors;
    }

    public void setColors(List<GoodsSpecAttrEntity> colors) {
        this.colors = colors;
    }

    public List<GoodsSpecAttrEntity> getStyles() {
        return styles;
    }

    public void setStyles(List<GoodsSpecAttrEntity> styles) {
        this.styles = styles;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
