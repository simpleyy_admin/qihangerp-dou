package com.b2c.entity.erp;

/**
 * 描述：
 * 购货单销货单商品明细Entity
 *
 * @author qlp
 * @date 2019-03-21 15:23
 */
public class InvoiceInfoEntity {
    private Long id;
    private Long iid;//关联ID(erp_invoice)
    private String billNo;//单据编号
    private String transType;//150501采购 150502退货
    private String transTypeName;//交易类型名称
    private Double amount;//折扣后金额
    private String billDate;//单据日期
    private String serialno;//系列号
    private String description;//备注
    private Integer goodsId;//商品ID
    private String goodsNumber;//商品编码
    private Integer specId;//商品规格ID
    private String specNumber;//商品规格编码
    private Double price;//单价
    private Double disAmount;//折扣额
    private Double disRate;//折扣率
    private Long quantity;//数量
    private Long qualifiedQuantity;//合规数量
    private Long abnormalQuantity;//异常数量

    private Long inQuantity;//已入库数量
    private Integer locationId;//仓库id
    private String locationName;//仓库name
    private Integer entryId;//区分调拨单  进和出
    private Integer isDelete;//1删除  0正常
    private Integer status;//状态
    private String srcOrderNo;//订单号

    private Integer checkoutStatus;

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIid() {
        return iid;
    }

    public void setIid(Long iid) {
        this.iid = iid;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransTypeName() {
        return transTypeName;
    }

    public void setTransTypeName(String transTypeName) {
        this.transTypeName = transTypeName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Double getDisRate() {
        return disRate;
    }

    public void setDisRate(Double disRate) {
        this.disRate = disRate;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getQualifiedQuantity() {
        return qualifiedQuantity;
    }

    public void setQualifiedQuantity(Long qualifiedQuantity) {
        this.qualifiedQuantity = qualifiedQuantity;
    }

    public Long getAbnormalQuantity() {
        return abnormalQuantity;
    }

    public void setAbnormalQuantity(Long abnormalQuantity) {
        this.abnormalQuantity = abnormalQuantity;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSrcOrderNo() {
        return srcOrderNo;
    }

    public void setSrcOrderNo(String srcOrderNo) {
        this.srcOrderNo = srcOrderNo;
    }

    public Integer getCheckoutStatus() {
        return checkoutStatus;
    }

    public void setCheckoutStatus(Integer checkoutStatus) {
        this.checkoutStatus = checkoutStatus;
    }
}
