package com.b2c.entity.erp.vo;

import java.util.List;

/**
 * 描述：
 * 仓库系统商品展示list
 *
 * @author qlp
 * @date 2019-07-01 17:28
 */
public class ErpGoodsListVo {
    private Integer id;
    private Integer mallGoodsId;//商城goodsid
    private String name;
    private String number;
    private String category;
    private Integer status;
    private Integer disable;
    private Integer locationId;
    private String locationName; //仓库名
    private Integer reservoirId;
    private String reservoirName;
    private Integer shelfId;
    private String shelfName;
    private String unitName; //单位名称
    private String image;
    private String sizeImg;
    private String attr1;
    private String attr2;
    private String attr3;
    private String attr4;
    private String attr5;
    private Long qty;
    private Long qty1;
    private Long createTime;//创建时间
    private float length;//衣长/裙长/裤长
    private float weight;//重量
    private float height;//高度/袖长
    private float width;//宽度/胸阔(围)
    private float width1;//肩阔
    private float width2;//腰阔
    private float width3;//臀阔
    private String remark;
    private double salePrice;
    private double costPrice;
    private double freight;
    private Integer specCount;//规格数量

    private List<ErpGoodsSpecStockVo> specList;

    
    public String getSizeImg() {
        return sizeImg;
    }

    public void setSizeImg(String sizeImg) {
        this.sizeImg = sizeImg;
    }

    public double getFreight() {
        return freight;
    }

    public void setFreight(double freight) {
        this.freight = freight;
    }

    public double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(double costPrice) {
        this.costPrice = costPrice;
    }

    public Integer getSpecCount() {
        return specCount;
    }

    public void setSpecCount(Integer specCount) {
        this.specCount = specCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getWidth1() {
        return width1;
    }

    public void setWidth1(float width1) {
        this.width1 = width1;
    }

    public float getWidth2() {
        return width2;
    }

    public void setWidth2(float width2) {
        this.width2 = width2;
    }

    public float getWidth3() {
        return width3;
    }

    public void setWidth3(float width3) {
        this.width3 = width3;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public String getAttr5() {
        return attr5;
    }

    public void setAttr5(String attr5) {
        this.attr5 = attr5;
    }

    public String getAttr4() {
        return attr4;
    }

    public void setAttr4(String attr4) {
        this.attr4 = attr4;
    }

    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    public String getAttr3() {
        return attr3;
    }

    public void setAttr3(String attr3) {
        this.attr3 = attr3;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getMallGoodsId() {
        return mallGoodsId;
    }

    public void setMallGoodsId(Integer mallGoodsId) {
        this.mallGoodsId = mallGoodsId;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Integer getReservoirId() {
        return reservoirId;
    }

    public void setReservoirId(Integer reservoirId) {
        this.reservoirId = reservoirId;
    }

    public String getReservoirName() {
        return reservoirName;
    }

    public void setReservoirName(String reservoirName) {
        this.reservoirName = reservoirName;
    }

    public Integer getShelfId() {
        return shelfId;
    }

    public void setShelfId(Integer shelfId) {
        this.shelfId = shelfId;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public List<ErpGoodsSpecStockVo> getSpecList() {
        return specList;
    }

    public void setSpecList(List<ErpGoodsSpecStockVo> specList) {
        this.specList = specList;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Long getQty1() {
        return qty1;
    }

    public void setQty1(Long qty1) {
        this.qty1 = qty1;
    }
}
