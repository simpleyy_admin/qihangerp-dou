package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 出库商品库存信息
 *
 * @author qlp
 * @date 2019-09-20 14:18
 */
public class ErpStockOutGoodsStockInfo {
    private Integer locationId;
    private Integer reservoirId;
    private Integer shelfId;
    private String locationName;
    private String reservoirName;
    private String shelfName;
    private Long currentQty;//当前库存
    private Long lockedQty;//锁定库存

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getReservoirId() {
        return reservoirId;
    }

    public void setReservoirId(Integer reservoirId) {
        this.reservoirId = reservoirId;
    }

    public Integer getShelfId() {
        return shelfId;
    }

    public void setShelfId(Integer shelfId) {
        this.shelfId = shelfId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getReservoirName() {
        return reservoirName;
    }

    public void setReservoirName(String reservoirName) {
        this.reservoirName = reservoirName;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

}
