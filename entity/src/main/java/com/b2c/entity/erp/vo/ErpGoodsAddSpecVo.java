package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 仓库系统添加商品规格Vo
 *
 * @author qlp
 * @date 2019-07-01 16:15
 */
public class ErpGoodsAddSpecVo {
    private String color;
    private String img;
    private Integer colorId;
    private String size;
    private Integer sizeId;
    private Integer styleId;
    private String style;
    private String specNumber;
    private Long quantity;
    private Integer lowQty;
    private Integer highQty;
    private Float costPrice;
    

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getStyleId() {
        return styleId;
    }

    public void setStyleId(Integer styleId) {
        this.styleId = styleId;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Integer getLowQty() {
        return lowQty;
    }

    public void setLowQty(Integer lowQty) {
        this.lowQty = lowQty;
    }

    public Integer getHighQty() {
        return highQty;
    }

    public void setHighQty(Integer highQty) {
        this.highQty = highQty;
    }

    public Float getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Float costPrice) {
        this.costPrice = costPrice;
    }
}
