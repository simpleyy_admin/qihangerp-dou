package com.b2c.entity.erp.enums;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum InvoiceBillTypeEnum {
    //PO采购订单 OI其他入库 PUR采购入库 BAL初期余额
    PO("采购订单", "PO"),
//    Manufacture("生产订单", "Manufacture"),
    Purchase("采购订单", "Purchase"),
    OI("其他入库", "OI"),
//    PUR("采购入库", "PUR"),
    SALE("销货", "SALE"),
    ORDER("线上订单", "ORDER"),
    REFUND("线上订单退货入库", "REFUND"),
    BAL("初期余额", "BAL"),
    PUR_RETURN("采购退货","PUR_RETURN");

    private String name;
    private String index;

    // 构造方法
    private InvoiceBillTypeEnum(String name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(String index) {
        for (InvoiceBillTypeEnum c : InvoiceBillTypeEnum.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
