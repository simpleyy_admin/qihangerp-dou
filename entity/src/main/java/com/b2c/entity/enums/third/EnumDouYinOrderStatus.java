package com.b2c.entity.enums.third;

public enum  EnumDouYinOrderStatus {
    //erp 订单状态0:待提交,1:待审核,2:待发货3:已发货,4:已收货,5:已完成,6已退货
    //0	开始
    //1	待确认 (用户下单未付款 或者 货到付款订单商家未确认)
    //2	备货中 (用户已付款) 此状态商户才可执行发货操作 (货到付款的订单, 商家需要先确认订单才会进入此状态)
    //3	已发货 (商家出库、已发货)
    //4	已取消    //1.用户未支付并取消订单 //2.或超时未支付后系统自动取消订单//3.或货到付款订单用户拒收
    //5	已完成
    //在线支付订单: 商家发货后, 用户收货、拒收或者15天无物流

    //货到付款订单: 用户确认收货
    //6	(退货) 退货中-用户申请
    //7	(退货) 退货中-商家同意退货
    //8	(退货) 退货中-客服仲裁
    //9	(退货) 已关闭-退货失败
    //10	(退货) 退货中-客服同意
    //11	(退货) 退货中-用户填写完物流
    //12	(退货) 已关闭-商户同意
    //13	(退货) 退货中-再次客服仲裁
    //14	(退货) 已关闭-客服同意
    //15	(退货) 取消退货申请
    //注:	商家未发货用户才可退款, 订单会进入退款相关状态
    //
    //商家确认发货之后用户只可操作退货, 进入退货相关状态, 不会进入退款状态
    //16	(退款) 申请退款中 (商家还未发货, 用户申请退款)
    //17	(退款) 商户同意退款
    //18	(退款) 订单退款仲裁中
    //19	(退款) 退款仲裁支持用户
    //20	(退款) 退款仲裁支持商家
    //21	(退款) 订单退款成功 (即用户在商家出库前可发起退款, 最终退款成功)
    //22	(退款) 售后退款成功 (即用户在退款后, 正常走完退货流程, 退款成功)
    //23	(退货) 退货中-再次用户申请
    //24	(退货) 已关闭-退货成功
    //25	备货中-用户取消
    //26	备货中-退款商家拒绝
    //27	退货中-商家拒绝退货
    //28	退货失败
    //29	退货中-商家再次拒绝
    //30	退款中-退款申请
    //31	退款申请取消
    //32	退款成功-商家同意
    //33	退款中-商家拒绝
    //34	退款中-客服仲裁
    //35	退款中-客服同意
    //36	退款中-退款失败
    //37	已关闭-退款失败
    //38	退款中-线下退款成功
    //39	退款中-退款成功
    //订单状态1 待确认/待支付（订单创建完毕）105 已支付 2 备货中 101 部分发货 3 已发货（全部发货）4 已取消5 已完成（已收货）
    STATUS_1("待支付", -99, 1),
    STATUS_105("已支付", -99, 105),
    WAIT_SEND_GOODS("待发货", 2, 2),
    SEND_GOODS("已发货", 3, 3),
    CANCEL("已取消", -1, 4),
    TRADE_FINISHED("已完成", 5, 5),
    CLOSED("已关闭",-1,9),
    TRADE_CLOSED("退货中", 6, 6);

    // 成员变量
    private String name;
    private Integer erpIndex;
    private Integer thirdIndex;

    // 构造方法
    private EnumDouYinOrderStatus(String name, Integer erpIndex, Integer thirdIndex) {
        this.name = name;
        this.erpIndex = erpIndex;
        this.thirdIndex = thirdIndex;
    }

    // 普通方法
    public static String getErpStatusName(Integer erpIndex) {
        for (EnumDouYinOrderStatus c : EnumDouYinOrderStatus.values()) {
            if (c.getErpIndex()==erpIndex) {
                return c.name;
            }
        }
        return null;
    }
    public static String getThirdStatusName(Integer thirdIndex) {
        for (EnumDouYinOrderStatus c : EnumDouYinOrderStatus.values()) {
            if (c.getThirdIndex()==thirdIndex) {
//                System.out.println(c.name);
                return c.name;
            }
        }
        return null;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getErpIndex() {
        return erpIndex;
    }

    public void setErpIndex(Integer erpIndex) {
        this.erpIndex = erpIndex;
    }

    public Integer getThirdIndex() {
        return thirdIndex;
    }

    public void setThirdIndex(Integer thirdIndex) {
        this.thirdIndex = thirdIndex;
    }
}
