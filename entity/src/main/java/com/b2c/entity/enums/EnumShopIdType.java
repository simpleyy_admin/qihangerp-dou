package com.b2c.entity.enums;

/**
 * 描述：
 * 店铺类型Enum
 *
 * @author qlp
 * @date 2019-09-18 19:44
 */
public enum EnumShopIdType {
    ALIBABA("ALIBABA", 1),
    TMALL("TMALL",2),
    YUNGOU("YUNGOU", 3),
    OFFLINE("OFFLINE", 4),
    PDD("PDD", 5),
    TAOBAO("TAOBAO", 6),
    DaiFa("DaiFa", 11);

    private String name;
    private int index;

    // 构造方法
    private EnumShopIdType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumShopIdType c : EnumShopIdType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
