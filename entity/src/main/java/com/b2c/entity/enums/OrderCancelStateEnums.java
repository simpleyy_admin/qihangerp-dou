package com.b2c.entity.enums;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 描述：
 * 退货订单状态
 *
 * @author qlp
 * @date 2019-02-21 13:44
 */
public enum OrderCancelStateEnums {
    //退款状态0未申请售后1售后申请中(退款待审核)2同意退货(退款待收货)3买家已发货，待收货(待收货)4已收货（待退款）5退款退货成功(退款完成)6退款拒绝7已确认收货，正在退款中 8退款取消
    NoRefund("未申请售后", 0),
    RefundApply("售后审核中", 1),
    Agree("同意退货", 2),//待买家发货
    Delivered("买家已发货", 3),//买家已发货，待收货
    Received("已收货，等待退款", 4),//已收货
    Success("退货成功", 5),
    Refuse("拒绝退款", 6),
    Cancel("取消退款", 8),
    Paying("已确认收货，正在退款中", 7);

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private OrderCancelStateEnums(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (OrderCancelStateEnums c : OrderCancelStateEnums.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 将该枚举全部转化成json
     *
     * @return
     */
    public static String toJson() {
        JSONArray jsonArray = new JSONArray();
        for (OrderCancelStateEnums e : OrderCancelStateEnums.values()) {
            JSONObject object = new JSONObject();
            object.put("name", e.getName());
            object.put("value", e.getIndex());
            jsonArray.add(object);
        }
        return jsonArray.toString();
    }
}
