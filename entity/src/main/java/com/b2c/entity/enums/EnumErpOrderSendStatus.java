package com.b2c.entity.enums;

/**
 * 描述：
 * 订单发货状态
 *
 * @author qlp
 * @date 2019-03-27 16:39
 */
public enum EnumErpOrderSendStatus {
    WaitOut("待分配", 0),
    Picking("待发货", 1),
    Picked("已拣货", 2),
    HasOut("已出库", 3),
    HasSend("已发货", 4);

    private String name;
    private int index;

    // 构造方法
    private EnumErpOrderSendStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumErpOrderSendStatus c : EnumErpOrderSendStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
