package com.b2c.entity.enums;

/**
 * 描述：用户状态
 *
 * @author pbd
 * @date 2019-03-06 09:20
 */
public enum UserStateEnums {
    NORMAL("正常", 0),
    LOCK("锁定", 1);

    private String name;
    private int index;

    // 构造方法
    private UserStateEnums(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (UserStateEnums c : UserStateEnums.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
