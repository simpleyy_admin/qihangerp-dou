package com.b2c.entity.enums;

/**
 * 描述：
 * 店铺类型Enum
 *
 * @author qlp
 * @date 2019-09-18 19:44
 */
public enum EnumZhuBo {
    Ali("1", "dfgn00"),
    YouZan("2", "hyfs0000"),
    TaoBao("3", "hyfz0000"),
    Tmall("4", "hyyf000");

    private String name;
    private String index;

    // 构造方法
    private EnumZhuBo(String name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(String index) {
        for (EnumZhuBo c : EnumZhuBo.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
