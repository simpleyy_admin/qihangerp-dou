package com.b2c.entity.enums.datacenter;

public enum EnumTmallOrderSource {
    TMall("天猫订单", 0),
    TaoBao("淘宝订单", 1);

    private String name;
    private int index;

    // 构造方法
    private EnumTmallOrderSource(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumTmallOrderSource c : EnumTmallOrderSource.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // 普通方法
    public static Integer getIndex(String name) {
        for (EnumTmallOrderSource c : EnumTmallOrderSource.values()) {
            if (c.getName().equals(name)) {
                return c.index;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
