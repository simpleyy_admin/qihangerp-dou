package com.b2c.entity.enums.tao;

public enum EnumDcTmallOrderReturnStatus {
    notrefund("无售后", 0),
    refunding("售后中", 1),
    // waitsend("等待发货", 2),
    send_goods("已发货", 3),
    received("已签收",4),
    Complete("售后完成", 9);

    private String name;
    private int index;

    // 构造方法
    private EnumDcTmallOrderReturnStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumDcTmallOrderReturnStatus c : EnumDcTmallOrderReturnStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // 普通方法
    public static Integer getIndex(String name) {
        for (EnumDcTmallOrderReturnStatus c : EnumDcTmallOrderReturnStatus.values()) {
            if (c.getName().equals(name)) {
                return c.index;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
