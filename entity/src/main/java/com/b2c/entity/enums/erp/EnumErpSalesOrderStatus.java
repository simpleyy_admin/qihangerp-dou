package com.b2c.entity.enums.erp;

/**
 * 描述：
 * 订单状态枚举
 *
 * @author qlp
 * @date 2019-02-21 11:34
 */
public enum EnumErpSalesOrderStatus {
    //订单状态0:待提交,1:待审核,2:待发货3:已发货,4:已收货,5:已完成
    CANCEL("已取消", -1),
    WaitPay("待支付", 0),
    WaitAudit("待审核", 1),
    WaitSend("待发货", 2),//待发货
    Delivered("已发货", 3),//待收货
    Received("已收货", 4),//已收货
    Completed("已完成", 5),//已完成
    REFUND("已退款",6);//已退款


    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private EnumErpSalesOrderStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumErpSalesOrderStatus c : EnumErpSalesOrderStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
