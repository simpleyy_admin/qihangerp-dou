package com.b2c.entity.enums.third;

public enum EnumDouYinOrderRefundTypeStatus {
    //售后类型，枚举为0(退货退款),1(已发货仅退款),2(未发货仅退款),3(换货)
    THTK("退货退款", 0),
    FHTK("已发货仅退款",1),
    WFHTK("未发货仅退款", 2),
    HH("换货",  3);

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumDouYinOrderRefundTypeStatus(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(Integer index) {
        for (EnumDouYinOrderRefundTypeStatus c : EnumDouYinOrderRefundTypeStatus.values()) {
            if (c.getIndex()==index) {
//                System.out.println(c.name);
                return c.name;
            }
        }
        return null;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
