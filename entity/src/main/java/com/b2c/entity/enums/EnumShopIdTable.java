package com.b2c.entity.enums;

/**
 * 描述：
 * 店铺Id对应的数据库表名
 *
 * @author qlp
 * @date 2019-09-18 19:44
 */
public enum EnumShopIdTable {

    Ali("dc_ali_order", 1),
    TMall("dc_tmall_order",2),
    YunGou("orders",3),
    Pifa("orders",4),
    ZhiBo("orders",11),
    PDD("dc_pdd_order", 5),
    TaoBao("dc_tmall_order", 6),
    YouZan("dc_yz_order", 10);

    private String name;
    private int index;

    // 构造方法
    private EnumShopIdTable(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getTable(int index) {
        for (EnumShopIdTable c : EnumShopIdTable.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }



}
