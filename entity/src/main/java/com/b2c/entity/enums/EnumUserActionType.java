package com.b2c.entity.enums;

/**
 * 描述：
 * 仓库系统用户操作日志类型枚举
 *
 * @author qlp
 * @date 2019-10-24 09:05
 */
public enum EnumUserActionType {
    //操作类型1登录2查询3库存操作4新增数据5修改数据9审核
    Login("登录", 1),
    Query("查询", 2),
    StockOperation("库存操作", 3),
    Add("新增数据", 4),
    Modify("修改数据", 5),
    Audit("审核",9);

    private String name;
    private int index;

    // 构造方法
    private EnumUserActionType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumUserActionType c : EnumUserActionType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
