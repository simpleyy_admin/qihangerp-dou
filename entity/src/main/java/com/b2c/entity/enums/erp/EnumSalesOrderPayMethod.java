package com.b2c.entity.enums.erp;

public enum EnumSalesOrderPayMethod {
    //（1:微信，2:支付宝，3:线下支付，4:第三方平台，5:0元购）


    OFFLINE_WeiXin("微信线下转账", 11),
    OFFLINE_AliPay("支付宝线下转账", 12),
    OFFLINE_Bank("银行线下转账", 13),
    ONLINE_WeiXin("线上-微信支付", 1),
    ONLINE_AliPay("线上-支付宝支付", 2),
    OFFLINE_PAY("线下支付", 3),
    Free_Buy("0元购", 5),
    Third_Platform("第三方平台", 4);

    private String name;
    private int index;

    // 构造方法
    private EnumSalesOrderPayMethod(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumSalesOrderPayMethod c : EnumSalesOrderPayMethod.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
