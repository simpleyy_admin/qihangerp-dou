package com.b2c.entity.enums;

public enum EnumXhsOrderAfterStatus {
    //小红书售后状态，1无售后 2售后处理中 3售后完成(含取消)
    STATUS_1("无售后",  1),
    STATUS_2("售后处理中",  2),
    STATUS_3("售后完成(含取消)", 3);

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumXhsOrderAfterStatus(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getStatusName(Integer index) {
        for (EnumXhsOrderAfterStatus c : EnumXhsOrderAfterStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
