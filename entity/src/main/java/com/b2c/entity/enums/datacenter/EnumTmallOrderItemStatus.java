package com.b2c.entity.enums.datacenter;


/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-23 09:33
 */
public enum EnumTmallOrderItemStatus {

    NotRefund("无退款", 1);

    private String name;
    private int index;

    // 构造方法
    private EnumTmallOrderItemStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumTmallOrderItemStatus c : EnumTmallOrderItemStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // 普通方法
    public static Integer getIndex(String name) {
        for (EnumTmallOrderItemStatus c : EnumTmallOrderItemStatus.values()) {
            if (c.getName().equals(name)) {
                return c.index;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
