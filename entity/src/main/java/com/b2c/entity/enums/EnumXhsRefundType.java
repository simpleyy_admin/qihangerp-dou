package com.b2c.entity.enums;

public enum EnumXhsRefundType {
    //退货类型 1-退货退款, 2-换货, 3:仅退款(old) 4:仅退款(new) 理论上不会有3出现 5:未发货仅退款
    STATUS_1("退货退款",  1),
    STATUS_2("换货",  2),
    STATUS_3("仅退款(old)",  3),
    STATUS_4("仅退款(new)",  4),
    STATUS_5("未发货仅退款",  5);

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumXhsRefundType(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getStatusName(Integer index) {
        for (EnumXhsRefundType c : EnumXhsRefundType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
