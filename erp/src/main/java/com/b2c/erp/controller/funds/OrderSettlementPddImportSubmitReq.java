package com.b2c.erp.controller.funds;

import java.util.List;

import com.b2c.entity.funds.OrderSettlementPddEntity;

public class OrderSettlementPddImportSubmitReq {
    private Integer shopId;

    private List<OrderSettlementPddEntity> orderList;//导入的订单

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<OrderSettlementPddEntity> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderSettlementPddEntity> orderList) {
        this.orderList = orderList;
    }

    
}
