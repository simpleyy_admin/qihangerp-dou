package com.b2c.erp.controller.api.req;

import lombok.Data;

@Data
public class GoodsListReq {
    private int pageIndex;
    private int pageSize;
    private String number;
}
