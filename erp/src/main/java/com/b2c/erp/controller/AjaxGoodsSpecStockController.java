package com.b2c.erp.controller;

import com.b2c.entity.erp.vo.ErpGoodsSpecStockListVo;
import com.b2c.entity.erp.vo.GoodsSearchShowVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.interfaces.wms.StockService;
import com.b2c.erp.req.GoodsSearchReq;
import com.b2c.erp.response.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品规格库存AJAX
 */
@RequestMapping(value = "/ajax_goods_stock")
@RestController
public class AjaxGoodsSpecStockController {
    @Autowired
    private StockService stockService;
    /**
     * 关键词搜索商品
     *
     * @return
     */
    @RequestMapping(value = "/goods_spec_stock_search", method = RequestMethod.POST)
    public ApiResult<List<ErpGoodsSpecStockListVo>> searchGoodsDetail(@RequestBody GoodsSearchReq req) {
        ResultVo<List<GoodsSearchShowVo>> resultVo = new ResultVo<>();
        List<ErpGoodsSpecStockListVo> goods = stockService.searchGoodSpecStockByNumber(req.getKey());

        return new ApiResult<>(0, "SUCCESS", goods);
    }
}
