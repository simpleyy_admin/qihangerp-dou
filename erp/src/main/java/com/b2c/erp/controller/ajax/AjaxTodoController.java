package com.b2c.erp.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.interfaces.TodoService;

@RestController
@RequestMapping(value = "/ajax_todo")
public class AjaxTodoController {
    @Autowired
    private TodoService todoService;
    
    @RequestMapping(value = "/add_todo", method = RequestMethod.POST)
    public ApiResult<Integer> add(@RequestBody DataRow req) {
        String sourceId = req.getString("sourceId");
        Integer sourceType = req.getInt("sourceType");//类型1拼多多退货
        String content = req.getString("content");
        Integer shopId = req.getInt("shopId");

        todoService.addTodo(shopId,sourceType, sourceId, content);
        return new ApiResult<>(0, "SUCCESS");
    }
    @RequestMapping(value = "/todo_handle", method = RequestMethod.POST)
    public ApiResult<Integer> handle(@RequestBody DataRow req) {
   
        Integer id = req.getInt("id");//
        String result = req.getString("result");

        todoService.handle(id,result);
        return new ApiResult<>(0, "SUCCESS");
    }

    
    @RequestMapping(value = "/todo_completed", method = RequestMethod.POST)
    public ApiResult<Integer> completed(@RequestBody DataRow req) {
   
        Integer id = req.getInt("dataId");
        

        todoService.completed(id);
        return new ApiResult<>(0, "SUCCESS");
    }
    
}
