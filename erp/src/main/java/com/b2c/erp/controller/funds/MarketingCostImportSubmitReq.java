package com.b2c.erp.controller.funds;

import com.b2c.entity.pdd.PddMarketingFeeEntity;

import java.util.List;

public class MarketingCostImportSubmitReq<T> {
    private Integer shopId;
    private String source;

    private List<T> list;//导入execl list

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
