package com.b2c.erp.controller.api.vo;

import lombok.Data;

@Data
public class GoodsDetailSkuListVo {
  private Integer  id;//: 2259, // skuId
  private Integer  color;//color_id: '1', // 规格类目 k_s 为 s1 的对应规格值 id
  private Integer size;//size_id: '11', // 规格类目 k_s 为 s2 的对应规格值 id
  private Integer style;//style_id
  private Integer  price;//: 100, // 价格（单位分）
  private Integer  stock_num;//: 110 // 当前 sku 组合对应的库存
}
