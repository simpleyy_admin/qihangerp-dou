package com.b2c.erp.controller.pdd;

public class ShopDataReportPddVo {
    private String date;
    private Integer date1;
    private Integer date2;
    private Integer date3;
    private Integer date4;
    private Integer date5;


    public ShopDataReportPddVo(String date, Integer date1, Integer date2, Integer date3, Integer date4, Integer date5) {
        this.date = date;
        this.date1 = date1;
        this.date2 = date2;
        this.date3 = date3;
        this.date4 = date4;
        this.date5 = date5;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDate1() {
        return date1;
    }

    public void setDate1(Integer date1) {
        this.date1 = date1;
    }

    public Integer getDate2() {
        return date2;
    }

    public void setDate2(Integer date2) {
        this.date2 = date2;
    }

    public Integer getDate3() {
        return date3;
    }

    public void setDate3(Integer date3) {
        this.date3 = date3;
    }

    public Integer getDate4() {
        return date4;
    }

    public void setDate4(Integer date4) {
        this.date4 = date4;
    }

    public Integer getDate5() {
        return date5;
    }

    public void setDate5(Integer date5) {
        this.date5 = date5;
    }
}
