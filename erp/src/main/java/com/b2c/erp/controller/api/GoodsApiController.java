package com.b2c.erp.controller.api;

import com.alibaba.fastjson2.JSONObject;
import com.b2c.entity.GoodsSpecAttrEntity;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.erp.ErpGoodsEntity;
import com.b2c.entity.erp.ErpGoodsSpecEntity;
import com.b2c.entity.result.PagingResponse;
import com.b2c.erp.controller.api.req.GoodsListReq;
import com.b2c.erp.controller.api.vo.GoodsDetailSkuListVo;
import com.b2c.erp.controller.api.vo.GoodsDetailVo;
import com.b2c.erp.controller.api.vo.GoodsSkuAttrValueVo;
import com.b2c.erp.controller.api.vo.GoodsSkuAttrVo;
import com.b2c.interfaces.erp.ErpGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class GoodsApiController {
    @Autowired
    private ErpGoodsService goodsService;

    @RequestMapping(value = "/goods_sku_detail/{goodsId}", method = RequestMethod.GET)
    public ApiResult<GoodsDetailVo> getGoodsSkuDetail(@PathVariable Integer goodsId) {
        GoodsDetailVo goodsDetailVo = new GoodsDetailVo();
        ErpGoodsEntity goods = goodsService.getById(goodsId);
        goodsDetailVo.setGoodsId(goodsId);
        goodsDetailVo.setPrice(goods.getPrice().toString());
        goodsDetailVo.setStockNum(100);
        goodsDetailVo.setPicture(goods.getImage());

        List<GoodsSkuAttrVo> skuAttr = new ArrayList<>();
        //商品sku属性值
        List<GoodsSpecAttrEntity> colors = goodsService.getSkuValueList("color",goodsId);
        GoodsSkuAttrVo colorAttr = new GoodsSkuAttrVo();
        colorAttr.setK("颜色");
        colorAttr.setK_s("color");
        colorAttr.setLargeImageMode(true);
        List<GoodsSkuAttrValueVo> cs = new ArrayList<>();
        for (var c:colors) {
            GoodsSkuAttrValueVo savv = new GoodsSkuAttrValueVo();
            savv.setId(c.getVid()+"");
            savv.setName(c.getV());
            savv.setImgUrl(c.getImg());
            cs.add(savv);
        }
        colorAttr.setV(cs);

        skuAttr.add(colorAttr);

        List<GoodsSpecAttrEntity> sizes = goodsService.getSkuValueList("size",goodsId);
        GoodsSkuAttrVo sizeAttr = new GoodsSkuAttrVo();
        sizeAttr.setK("尺码");
        sizeAttr.setK_s("size");
        List<GoodsSkuAttrValueVo> ss = new ArrayList<>();
        for (var c:sizes) {
            GoodsSkuAttrValueVo savv = new GoodsSkuAttrValueVo();
            savv.setId(c.getVid()+"");
            savv.setName(c.getV());
            ss.add(savv);
        }
        sizeAttr.setV(ss);

        skuAttr.add(sizeAttr);

        List<GoodsSpecAttrEntity> styles = goodsService.getSkuValueList("style",goodsId);


        goodsDetailVo.setSkuAttr(skuAttr);
        //商品SKU
        List<ErpGoodsSpecEntity> goodsSpec = goodsService.getSpecListByGoodsId(goodsId);
        List<GoodsDetailSkuListVo> skuList = new ArrayList<>();

        for (var spec:goodsSpec) {
            GoodsDetailSkuListVo skuVo = new GoodsDetailSkuListVo();
            skuVo.setId(spec.getId());
            skuVo.setStock_num(100);
            skuVo.setColor(spec.getColorId());
            skuVo.setSize(spec.getSizeId());
            skuVo.setStyle(spec.getStyleId());
            Double price = spec.getPurPrice() == 0 ? spec.getSalePrice():spec.getPurPrice();
            skuVo.setPrice(price.intValue()*100);
            skuList.add(skuVo);
        }

        goodsDetailVo.setSkuList(skuList);


        return new ApiResult<>(ApiResultEnum.SUCCESS,goodsDetailVo);
    }

    @RequestMapping(value = "/goods_list", method = RequestMethod.GET)
    public ApiResult<PagingResponse<ErpGoodsEntity>> getGoodsList(GoodsListReq req) {
        log.info("请求商品列表参数："+ JSONObject.toJSONString(req));
        var list = goodsService.getList(req.getPageIndex(), req.getPageSize(), req.getNumber(),0);

        return new ApiResult<>(ApiResultEnum.SUCCESS,list);
    }

}
