package com.b2c.erp.controller.shop;

public class ShopGoodsLinkErpSkuVo {
    private Integer id;
    private String spec;
    private String num;
    
    public String getSpec() {
        return spec;
    }
    public void setSpec(String spec) {
        this.spec = spec;
    }
    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
}
