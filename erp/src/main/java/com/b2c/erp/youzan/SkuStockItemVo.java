package com.b2c.erp.youzan;

/**
 * @Description: pbd add 2019/6/25 21:07
 */
public class SkuStockItemVo {
    private String k;
    private Integer kid;
    private String v;
    private Integer vid;

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public Integer getKid() {
        return kid;
    }

    public void setKid(Integer kid) {
        this.kid = kid;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public Integer getVid() {
        return vid;
    }

    public void setVid(Integer vid) {
        this.vid = vid;
    }
}
