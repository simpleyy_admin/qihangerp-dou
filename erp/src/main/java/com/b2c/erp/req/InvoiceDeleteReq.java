package com.b2c.erp.req;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-22 14:27
 */
public class InvoiceDeleteReq {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
