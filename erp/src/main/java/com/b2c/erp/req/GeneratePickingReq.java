package com.b2c.erp.req;

import java.util.List;

/**
 * 描述：
 * 生成拣货单req
 *
 * @author qlp
 * @date 2019-05-31 15:30
 */
public class GeneratePickingReq {
    private List<Long> id;

    public List<Long> getId() {
        return id;
    }

    public void setId(List<Long> id) {
        this.id = id;
    }
    //    private Long[] id;
//
//    public Long[] getId() {
//        return id;
//    }
//
//    public void setId(Long[] id) {
//        this.id = id;
//    }
}
