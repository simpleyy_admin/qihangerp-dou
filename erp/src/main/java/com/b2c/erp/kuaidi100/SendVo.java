package com.b2c.erp.kuaidi100;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-18 09:39
 */
public class SendVo {
    private String sendManName;
    private String sendManMobile;
    private String sendManPrintAddr;

    public String getSendManName() {
        return sendManName;
    }

    public void setSendManName(String sendManName) {
        this.sendManName = sendManName;
    }

    public String getSendManMobile() {
        return sendManMobile;
    }

    public void setSendManMobile(String sendManMobile) {
        this.sendManMobile = sendManMobile;
    }

    public String getSendManPrintAddr() {
        return sendManPrintAddr;
    }

    public void setSendManPrintAddr(String sendManPrintAddr) {
        this.sendManPrintAddr = sendManPrintAddr;
    }
}
