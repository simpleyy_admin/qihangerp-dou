# 更新日志
### 分析模块
+ 新增一级菜单《分析》
```
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('分析','','fx',1,2,0)
```
+ 新增二级菜单《商品动销-分析》
```
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('商品动销分析','/report/goods_sales','spdx',1,9,278)
```
+ 新增二级菜单《商品类目动销-分析》
```
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('商品类目动销分析','/report/goods_category_analyse','splmdx',1,9,278)
```

### wms库存数据每日统计
`
CREATE TABLE `daily_report` (
  `tjdate` varchar(20) NOT NULL COMMENT '日期',
  `zkc` bigint(20) NOT NULL DEFAULT '0' COMMENT '报损总数',
  `cgrk` bigint(20) NOT NULL DEFAULT '0' COMMENT '采购入库',
  `xsck` bigint(20) NOT NULL COMMENT '销售出库',
  `thrk` bigint(20) DEFAULT NULL COMMENT '退货入库',
  `cgth` bigint(20) DEFAULT NULL COMMENT '采购退货',
  `cgzj` decimal(10,2) DEFAULT '0.00' COMMENT '采购总价',
  `bstj` bigint(20) DEFAULT NULL COMMENT '报损数',
  UNIQUE KEY `tjdate_index` (`tjdate`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品每日统计';
`
##### 2021-08-12
wms系统ERP化改造，菜单改造
+ 新增一级菜单-商品管理wms_manage_permission
`INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('商品','','goods',1,9,0)
`
+ 商品管理、商品分类管理相关菜单移动到“商品”一级菜单下
+ 完善商品添加功能，新增品牌表
```
CREATE TABLE erp_goods_brand (
id INT NOT NULL PRIMARY key AUTO_INCREMENT,
`name` VARCHAR(50) NOT NULL 
)
```
+ erp_goods表新增字段brand（int，default：0）
+ 新增”设置“模块供应商管理、客户管理
```
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('供应商管理','/contact/supplier_list','gysgl',1,0,5)
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('客户管理','/contact/client_list','khgl',1,0,5)
```

+ 新增每日结余汇总
```
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('每日库存统计','/data/daily_report','rbb',1,0,4)
```
+ 新增采购统计，采购审核
```
 INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('采购审核','/ajax_purchase/purchase_check','pur_check',1,0,1)
 INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('采购统计','/stock_in/purchase_in_list_count','cgrktj',0,0,1)

```
+ 新增每日采购入库汇总
```
INSERT INTO  wms_manage_permission (`name`,url,permission_key,is_menu,sort,parent_id) VALUE ('每日采购入库统计','/data/daily_purchase_report','rcgbb',1,0,4)
```

##### 2020-03-19
引入Kafka Producer，用于：
+ 1、订单发货，通知FMS
##### 2020-02-19
+ 1、优化WMS入库日志存储时间；
    1) 新增erp_goods_stock_logs表字段createOn
        ```
            ALTER TABLE `erp_goods_stock_logs` ADD COLUMN createOn BIGINT(20) NOT NULL DEFAULT 0 ;
        ```
+ 2、优化WMS验货存储时间；
    1) 新增erp_stock_in_checkout表字段createOn、modifyOn；
        ```
        ALTER TABLE `erp_stock_in_checkout` ADD COLUMN createOn BIGINT(20) NOT NULL DEFAULT 0 ;
        ALTER TABLE `erp_stock_in_checkout` ADD COLUMN modifyOn BIGINT(20) NOT NULL DEFAULT 0 ;
        ```
+ 3、新增WMS订单快递单状态；
    1) 新增erp_order表字段logisticsPrintStatus、logisticsPrintCount、logisticsPrintTime、logisticsPrintType
        ```
        ALTER TABLE `erp_order` ADD COLUMN logisticsPrintStatus int(11) NOT NULL DEFAULT 0;
        ALTER TABLE `erp_order` ADD COLUMN logisticsPrintCount int(11) NOT NULL DEFAULT 0;
        ALTER TABLE `erp_order` ADD COLUMN logisticsPrintType int(11) NOT NULL DEFAULT 0 COMMENT '快递单打印类型1手写2打印';
        ALTER TABLE `erp_order` ADD COLUMN logisticsPrintTime BIGINT(20) NULL ;
        ```
+ 4、完善WMS订单打印页面（新版订单打印功能）；

##### 2020-02-18
+ 1、移除商品分类管理增删改功能；
+ 2、移除商品新增、修改、更新规格功能；
+ 3、优化采购管理列表显示；
+ 4、新增采购入库功能，移除采购验货子流程，简化入库操作；
    1) 新增erp_stock_in_form表字段
        1) 新增字段invoiceId
           ```
           # 用于入库单关联采购单ID，之前是通过验货单ID关联的
           ALTER TABLE `erp_stock_in_form` ADD COLUMN invoiceId BIGINT(20) NULL ;
           ``` 
        2) 新增字段stockInTime1（默认值0，保存时间戳）
            ```
            ALTER TABLE `erp_stock_in_form` ADD COLUMN stockInTime1 BIGINT(20) NULL ;
            ```
        3) 新增字段hasQcReport、qcInspector
            ```
            ALTER TABLE `erp_stock_in_form` ADD COLUMN qcInspector varchar(20) NULL COMMENT '是否有qc报告0没有1有';
            ALTER TABLE `erp_stock_in_form` ADD COLUMN hasQcReport INT(2) NULL  COMMENT 'qc报告人';
            ```
            
