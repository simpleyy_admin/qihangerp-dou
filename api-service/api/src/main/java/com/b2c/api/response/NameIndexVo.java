package com.b2c.api.response;

import lombok.Data;

@Data
public class NameIndexVo {
    private String name;
    private Integer index;

    public NameIndexVo(String name, Integer index) {
        this.name = name;
        this.index = index;
    }
}
