package com.b2c.api.query;

import lombok.Data;

@Data
public class TodoQuery extends PageQuery {
    private String content;
    private Integer status;
    private String startDate;
    private String endDate;
}
