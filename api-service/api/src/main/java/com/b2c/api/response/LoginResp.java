package com.b2c.api.response;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-11-19 15:36
 */
public class LoginResp implements Serializable {
    private String token;
    private int expires;//过期时间，毫秒
    private Integer userId;
    private String userName;
    public LoginResp(){}

    public LoginResp(String token, int expires, Integer userId, String userName) {
        this.token = token;
        this.expires = expires;
        this.userId = userId;
        this.userName=userName;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getExpires() {
        return expires;
    }

    public void setExpires(int expires) {
        this.expires = expires;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
