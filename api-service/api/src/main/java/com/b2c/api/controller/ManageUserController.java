package com.b2c.api.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.b2c.api.DataConfigObject;
import com.b2c.api.response.LoginResp;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.DataRow;
import com.b2c.entity.ManageUserEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统管理员
 */
@RestController
@RequestMapping("/api/v1")
public class ManageUserController extends CommonController{
    @Autowired
    private WmsUserService manageUserService;
    /**
     * @api {post} /api/v1/manage_user_login 管理员登录
     * @apiVersion 1.0.0
     * @apiName ManageUserUserLogin
     * @apiGroup ManageUser
     * @apiParam {String}  userName 用户名/手机号
     * @apiParam {String}  userPwd 密码
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK{
        "code": "0成功其他失败",
        "msg": "成功或失败信息",
        "data": {
            "token": "token",
            "expires": "过期时间",
            "userId": "用户id",
            "userName": "用户名",
        }
        }
     */
    @RequestMapping(value = "/manage_user_login", method = RequestMethod.POST)
    public ApiResult<LoginResp> userLogin(@RequestBody DataRow reqData, HttpServletRequest request) {
        String userName=reqData.getString("userName");
        String userPwd=reqData.getString("userPwd");
        ResultVo<ManageUserEntity> result = manageUserService.userLogin(userName, userPwd);


        if(result.getCode()==0){
            var user=result.getData();
            String name=StringUtils.isEmpty(user.getUserName()) ? user.getTrueName() : user.getUserName();

            int expires = 7*24*60*60 * 1000;//60分钟
            Date expDate = new Date(System.currentTimeMillis() + expires);
            String secret = DataConfigObject.getInstance().getJwtSecret();
            Algorithm algorithm = Algorithm.HMAC256(secret);
            Map<String,Object> mapObj= new HashMap<>();
            mapObj.put("userId",user.getId());
            String token = JWT.create()
                    .withIssuer("auth0").withHeader(mapObj)
                    .withClaim("userId", user.getId())
                    .withClaim("userName",name)
                    .withExpiresAt(expDate).withNotBefore(new Date())
                    .sign(algorithm);
            return new ApiResult<>(ApiResultEnum.SUCCESS.getIndex(),"成功",new LoginResp(token,expires,user.getId().intValue(),name));
        }
        return new ApiResult<>(ApiResultEnum.Fail.getIndex(), result.getMsg());

    }
    /**
     * @api {post} /api/v1/manage_check_user_login 检查管理员是否登录
     * @apiVersion 1.0.0
     * @apiName ManageUserCheckUserLogin
     * @apiGroup ManageUser
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK{
    "code": "0成功其他失败",
    "msg": "成功或失败信息",
    "data": {
        "token": "token",
        "expires": "过期时间",
        "userId": "用户id",
        "userName": "用户名",
    }
    }
     */
    @RequestMapping(value = "/manage_check_user_login", method = RequestMethod.POST)
    public ApiResult<LoginResp> checkUserLogin(@RequestHeader String token) {

        try {
            String secret = DataConfigObject.getInstance().getJwtSecret();
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            return new ApiResult<>(ApiResultEnum.SUCCESS.getIndex(),"成功",new LoginResp(token,0,jwt.getClaim("userId").asInt(),jwt.getClaim("userName").asString()));
        }catch (TokenExpiredException e){
            return new ApiResult<>(EnumResultVo.Fail.getIndex(),"账号信息过期了，请重新登录");
        }
    }
    /**
     * @api {post} /api/v1/manage_user_exit 管理员退出登录
     * @apiVersion 1.0.0
     * @apiName ManageUserUserExit
     * @apiGroup ManageUser
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     {
        "code": "0成功其他失败",
        "msg": "成功或失败信息",
        "data":null
    }
     */
    @RequestMapping(value = "/manage_user_exit", method = RequestMethod.POST)
    public ApiResult<Integer> userExit(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"成功");
    }
}
