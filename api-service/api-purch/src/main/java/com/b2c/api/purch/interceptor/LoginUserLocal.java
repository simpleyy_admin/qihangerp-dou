package com.b2c.api.purch.interceptor;

public class LoginUserLocal {
    private Integer userId;
    private String userName;

    public LoginUserLocal(Integer userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

}
