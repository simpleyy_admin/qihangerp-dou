package com.b2c.api.purch;

import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.util.Properties;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-11-08 13:58
 */
public class DataConfigObject {
    //创建 SingleObject 的一个对象
    private static DataConfigObject instance = new DataConfigObject();

    private Integer pageSize = 10;
    private String jwtSecret = "01234567890ecom000";
    private String pddClientId = "";
    private String pddClientSecret = "";

    private String loginUserName = "";

    //让构造函数为 private，这样该类就不会被实例化
    private DataConfigObject() {
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("config.properties");
            pageSize = Integer.parseInt(properties.getProperty("pageSize"));
            jwtSecret = properties.getProperty("jwt_secret");
            pddClientId = properties.getProperty("pdd_client_id");
            pddClientSecret = properties.getProperty("pdd_client_secret");
        } catch (Exception e) {
            pageSize = 10;
        }
    }

    //获取唯一可用的对象
    public static DataConfigObject getInstance() {
        return instance;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public String getJwtSecret() {
        return jwtSecret;
    }

    public String getPddClientId() {
        return pddClientId;
    }

    public void setPddClientId(String pddClientId) {
        this.pddClientId = pddClientId;
    }

    public String getPddClientSecret() {
        return pddClientSecret;
    }

    public void setPddClientSecret(String pddClientSecret) {
        this.pddClientSecret = pddClientSecret;
    }
}
