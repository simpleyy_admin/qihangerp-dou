package com.b2c.api.purch;


import com.b2c.api.purch.interceptor.AuthInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * 拦截器按照顺序执行
         *addPathPatterns 用于添加拦截规则
         *excludePathPatterns 用于排除拦截
         */
        String v1="/api/v1/";
        List<String> patterns= new ArrayList<>();
        patterns.add("/");
        patterns.add(v1+"manage_user_login");
        patterns.add(v1+"manage_user_exit");
        patterns.add(v1+"manage_check_user_login");
        patterns.add(v1+"manage_user_menu");
        patterns.add(v1+"manage_user_all_menu");
        patterns.add(v1+"/common/**");
        registry.addInterceptor(new AuthInterceptor()).excludePathPatterns(patterns);
    }
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("*")
                .allowedOrigins("*")
                .allowCredentials(true)
                .maxAge(3600);
    }
}
