package com.b2c.api.purch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.b2c.api", "com.b2c.repository", "com.b2c.service","com.b2c.interfaces"})
@SpringBootApplication
public class ApiPurchApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiPurchApplication.class, args);
    }
}
