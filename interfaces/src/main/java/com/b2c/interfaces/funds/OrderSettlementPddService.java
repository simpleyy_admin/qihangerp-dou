package com.b2c.interfaces.funds;

import java.util.List;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.funds.OrderSettlementDetailPddVo;
import com.b2c.entity.funds.OrderSettlementPddEntity;
import com.b2c.entity.result.ResultVo;

public interface OrderSettlementPddService {
    PagingResponse<OrderSettlementPddEntity> getList(Integer pageIndex, Integer pageSize, String orderSn, String startTime, String endTime);
    ResultVo<String> importExcelSettlementList(List<OrderSettlementPddEntity> list, Integer shopId);
    PagingResponse<OrderSettlementDetailPddVo> getOrderSettlementList(Integer pageIndex, Integer pageSize, String orderSn, String startTime, String endTime);
    List<OrderSettlementDetailPddVo> getOrderSettlementList(Integer shopId, String startTime, String endTime);
}
