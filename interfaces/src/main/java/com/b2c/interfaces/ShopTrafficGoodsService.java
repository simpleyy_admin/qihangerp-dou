package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.shop.ShopTrafficEntity;
import com.b2c.entity.shop.ShopTrafficGoodsEntity;
import com.b2c.entity.tao.TrafficTaoModel;

import java.util.List;

public interface ShopTrafficGoodsService {
    PagingResponse<ShopTrafficGoodsEntity> getGoodsTrafficList(Integer pageIndex,Integer pageSize ,Integer shopType,Integer shopId,String goodsNum,String date);
    PagingResponse<ShopTrafficEntity> getShopTrafficList(Integer pageIndex,Integer pageSize ,Integer shopType,Integer shopId,String date);
    ResultVo<Integer> addGoodsTraffic(ShopTrafficGoodsEntity entity);
    ResultVo<String> addGoodsTrafficTao(List<TrafficTaoModel> list, Integer shopId);


    ShopTrafficEntity getById(Long id);

    /**
     * 最后一条记录的时间
     * @return
     */
    String getLastDay(int shopType,Integer shopId);
    /**
     * 添加拼多多店铺流量
     * @param entity
     * @return
     */
    ResultVo<Integer> addShopTrafficForPdd(ShopTrafficEntity entity);

    ResultVo<Integer> addShopGoodsTrafficForPdd(ShopTrafficGoodsEntity entity);

    /**
     * 修改拼多多店铺流量数据
     * @param entity
     * @return
     */
    ResultVo<Integer> editShopTrafficForPdd(ShopTrafficEntity entity);
    /**
     * 添加抖店店铺流量
     * @param entity
     * @return
     */
    ResultVo<Integer> addShopTrafficForDy(ShopTrafficEntity entity);
}
