package com.b2c.interfaces;

import com.b2c.entity.datacenter.DcSysThirdSettingEntity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-12 11:53
 */
public interface SysThirdSettingService {
    DcSysThirdSettingEntity getEntity(Integer id);
    void updateEntity(Integer id,String access_token,String refresh_token,Integer expires_in,String thirdId);
}
