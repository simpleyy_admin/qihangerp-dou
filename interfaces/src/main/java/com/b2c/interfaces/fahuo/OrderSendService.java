package com.b2c.interfaces.fahuo;

import java.util.List;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.vo.GoodsSearchShowVo;
import com.b2c.entity.fahuo.OrderSendDailyVo;
import com.b2c.entity.fahuo.OrderSendEntity;
import com.b2c.entity.fahuo.OrderSendItemEntity;
import com.b2c.entity.result.ResultVo;

public interface OrderSendService {
    PagingResponse<OrderSendEntity> getList(Integer pageIndex, Integer pageSize, Integer sendStatus, String orderSn, String mobile, String logisticsCode, String orderTimeStartDate, String orderTimeEndDate, Integer shopId,Integer isSettle,Integer supplierId);
    List<OrderSendEntity> getList(Integer sendStatus,Integer supplierId,Integer isSettle);
    ResultVo<Integer> orderSendTypeEdit(Long orderId, Integer supplierId);
    /**
     * 手动发货（用于非仓库发货情况）
     * @param orderSendId
     * @param company
     * @param companyCode
     * @param code
     * @return
     */
    ResultVo<Integer> orderHandExpress(Long orderSendId, String company, String companyCode, String code);

    /**
     * 根据订单多选id获取明细list
     * @param orderIds
     * @return
     */
    List<GoodsSearchShowVo> getItemListByOrderIds(String orderIds);

    /**
     * 拉取拼多多订单物流信息
     * @return
     */
    Integer pullOrderLogisticsPdd();

    OrderSendEntity getOrderAndItemsById(Long id);


    /**
     * 取消订单
     * @param id
     * @param remark
     * @return
     */
    ResultVo<Integer> orederCancel(Long id ,String remark);

    List<OrderSendDailyVo> getSendReport(Integer shopId,String startDate,String endDate);

    ResultVo<Integer> updOrderRemark(Long orderId,String remark);
}
