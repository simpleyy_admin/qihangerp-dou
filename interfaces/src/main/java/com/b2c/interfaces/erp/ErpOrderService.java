package com.b2c.interfaces.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpOrderItemEntity;
import com.b2c.entity.apitao.OrderSendQuery;
import com.b2c.entity.ErpOrderEntity;
import com.b2c.entity.erp.vo.FaHuoDaiJianHuoGoodsVo;
import com.b2c.entity.erp.vo.ErpOrderItemListVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.order.DouYinOrderCountVo;
import com.b2c.entity.enums.EnumErpOrderSendStatus;
import com.b2c.entity.vo.OrderScanCodeVo;
import com.b2c.entity.vo.order.OrderWaitSendListVo;

import java.util.List;

/**
 * 描述：
 * 仓库系统订单service
 *
 * @author qlp
 * @date 2019-09-17 15:00
 */
public interface ErpOrderService {

    /***
     * 获取待发货订单
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param mobile
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<OrderWaitSendListVo> getWaitSendOrderList(Integer pageIndex, Integer pageSize, EnumErpOrderSendStatus sendStatus, String orderNum, String mobile, String logisticsCode, Integer startTime, Integer endTime,Integer saleType,Integer shopId);
    public List<ErpOrderItemListVo> orderWaitSendListExport(Integer startDate, Integer endDate);

    public PagingResponse<OrderWaitSendListVo> getNewWaitSendOrderList(OrderSendQuery query);


    /**
     * 已发货订单list item
     * @param startTime
     * @param endTime
     * @return
     */
    List<ErpOrderItemListVo> getErporderItemSendList(Integer startTime, Integer endTime);

    ErpOrderEntity getOrderEntityById(Long id);
    List<ErpOrderEntity> getOrderEntityByIds(String ids);

    /**
     * 获取订单
     *
     * @param num 订单编号
     * @return
     */
    ErpOrderEntity getOrderEntityByNum(String num);

    /**
     * 根据商城订单id获取订单商品list
     *
     * @param orderId
     * @return
     */
    List<ErpOrderItemListVo> getErpOrderItemsByOrderId(Long orderId);




    /**
     * 按订单items批量生成拣货单
     * @param orderItemIdList
     * @return
     */
    ResultVo<Long> generatingPickingListByOrderItem(List<Long> orderItemIdList);

    /**
     * 按订单批量生成拣货单
     *
     * @param orderIdList 订单id list
     * @return
     */
    ResultVo<Long> generatingPickingListByOrder(List<Long> orderIdList);

    /**
     * 订单发货
     *
     * @param orderId
     * @return
     */
    ResultVo<Integer> sendOrder(Long orderId);

    /**
     * 手动快递单发货
     *
     * @param erpOrderId
     * @param company
     * @param companyCode
     * @param code
     * @return
     */
    ResultVo<Integer> orderHandExpress(Integer erpOrderId, String company, String companyCode, String code);


    /**
     * 取消订单确认
     *
     * @param erpOrderIdArray
     * @return
     */
    ResultVo<Integer> cancelOrderConfirm(List<Long> erpOrderIdArray);

    /**
     * 根据订单Id list获取商品列表并且去重
     *
     * @param
     * @return
     */
    List<FaHuoDaiJianHuoGoodsVo> getDaiJianHuoOrderItemGoodsListAndDistinctSku(List<Long> orderIdList);

    /**
     * 获取已发货订单明细
     * @param orderNum
     * @param skuNumber
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<ErpOrderItemListVo> getOrderItemList(Integer pageIndex, Integer pageSize, String orderNum, String skuNumber, Integer startTime, Integer endTime,Integer orderStatus);

    List<ErpOrderItemListVo> getOrderItemListForExcel(String orderNum, String skuNumber, Integer startTime, Integer endTime);
    /**
     * 修改退货单物流信息
     * @param id
     * @param company
     * @param expressCode
     */
    public void updErpOrderReturnCode(Long id,String company,String expressCode);

    /**
     * 获取订单信息（包括items）
     * @param num 物流单号或者订单号
     * @param
     * @return
     */
    List<OrderScanCodeVo> getOrderAndItemsByLogisticsCodeOrorderNum(String num);
    List<OrderScanCodeVo> getOrderAndItemsByLogisticsCodeOrordeId(List<Object> orderIdArr);
    /**
     * 获取订单信息（包括items）
     * @param orderId 订单ID
     * @param
     * @return
     */
    OrderWaitSendListVo getOrderAndItemsByOrderId(Long orderId);

    public ResultVo<Integer> addErpOrderReturnItem(Long id, String specNumber, Integer num);

    public List<DouYinOrderCountVo> getDouYinOrderCount(String strDate, String endDate);

    public List<ErpOrderItemEntity> douyinOrderSettleExport (String startDate, String endDate);

    public void douyinOrderSettle(List<Long> ids,Integer isSettle);

}
