package com.b2c.interfaces;

/**
 * @Description:后台管理员相关 pbd add 2019/3/19 10:25
 */
public interface SystemRoleService {

    /**
     * 添加管理员登录日志
     *
     * @param userId
     * @param loginIp
     */
    void addManageUserLogin(Integer userId, String loginIp);

}
