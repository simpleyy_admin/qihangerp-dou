package com.b2c.interfaces.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsOrderSettleEntity;

public interface XhsOrderSettleService {
    ResultVo<Long> addOrderSettle(XhsOrderSettleEntity entity);
    PagingResponse<XhsOrderSettleEntity> getList(Integer pageIndex, Integer pageSize, String orderNum, String startDate, String endDate, Integer type);
}
