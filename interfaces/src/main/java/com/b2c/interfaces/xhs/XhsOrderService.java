package com.b2c.interfaces.xhs;


import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsOrderEntity;


public interface XhsOrderService {
    ResultVo<Long> pullOrder(XhsOrderEntity order);
    PagingResponse<XhsOrderEntity> getOrderList(Integer shopId, Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, Integer state, String logisticsCode);
    XhsOrderEntity getOrderDetailById(Long id);

    /**
     * 拉取收货地址
     * @param id 订单id
     * @param
     * @param
     * @return
     */
    ResultVo<Long> pullOrderAddress(Long id, String province,String city,String district,String address,String receiverName,String receiverPhone);

    /**
     * 订单备注
     * @param id
     * @param remark
     * @return
     */
    ResultVo<Integer> updOrderRemark(Long id,String remark);
}
