package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.ArticleEntity;

public interface ArticleService {
    PagingResponse<ArticleEntity> getList(Integer pageIndex, Integer pageSize, String title,String platform);
    void add(String title,String content,String platform,String tag);
}
