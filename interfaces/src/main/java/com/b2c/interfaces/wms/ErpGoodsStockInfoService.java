package com.b2c.interfaces.wms;

import java.util.List;

import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.entity.ErpGoodsStockInfoItemEntity;


/**
 * 描述：
 * 商品库存信息Service
 *
 * @author qlp
 * @date 2019-09-26 16:33
 */
public interface ErpGoodsStockInfoService {

    ErpGoodsStockInfoEntity getById(Long stockInfoId);

    /**
     * 更新删除状态
     * @param stockInfoId
     */
    void updateDeleteById(Long stockInfoId);

    /**
     * 分配仓位
     * @param specId
     * @param stockLocationId
     */
    void apportionStockLocationForSpec(Integer specId,Integer stockLocationId);

    /**
     * 获取库存批次信息
     * @param stockInfoId
     * @return
     */
    List<ErpGoodsStockInfoItemEntity> getStockInfoItemListBySpecId(Integer specId);
}
