package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStockInCheckoutEntity;
import com.b2c.entity.vo.ErpStockInCheckoutFormAddVo;
import com.b2c.entity.vo.ErpStockInCheckoutFormDetailVo;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-29 11:44
 */
public interface ErpStockInCheckoutService {
    /**
     * 添加验货表单
     * @param addVo
     * @return
     */
    Long addCheckoutForm(ErpStockInCheckoutFormAddVo addVo);

    /**
     * 根据表单id获取验货记录
     * @param invoiceId
     * @return
     */
    List<ErpStockInCheckoutEntity> getCheckoutForm(Long invoiceId);

    /**
     * 获取验货list
     *
     * @param pageIndex 页码
     * @param pageSize
     * @param number    单据编号
     * @param status    状态
     * @return
     */
    PagingResponse<ErpStockInCheckoutEntity> getCheckoutList(Integer pageIndex, Integer pageSize, String number, Integer status);

    ErpStockInCheckoutFormDetailVo getDetailById(Long id);
}
