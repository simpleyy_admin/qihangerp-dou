package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.InvoiceEntity;
import com.b2c.entity.erp.InvoiceInfoEntity;
import com.b2c.entity.erp.enums.*;
import com.b2c.entity.erp.vo.ErpInvoiceInfoVo;
import com.b2c.entity.erp.vo.ErpInvoiceVo;
import com.b2c.entity.erp.vo.InvoiceInfoListVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.wms.ErpPurchaseGoodVo;
import com.b2c.entity.vo.wms.InvoiceDetailVo;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;

import java.util.List;

/**
 * 描述：
 * 购货销货表单
 *
 * @author qlp
 * @date 2019-03-22 10:19
 */
public interface InvoiceService {
    /**
     * 获取表单数据
     *
     * @param id
     * @return
     */
    InvoiceEntity getInvoiceById(Long id);

    /**
     * 获取表单数据
     * @param billNo
     * @return
     */
    InvoiceEntity getInvoiceByBillNo(String billNo);

    /**
     * 获取采购表单详情
     *
     * @param invoiceId 采购单id
     * @return
     */
    InvoiceDetailVo getInvoiceDetail(Long invoiceId);

    /**
     * 获取采购表单详情
     *
     * @param billNo 采购单号
     * @return
     */
    InvoiceDetailVo getInvoiceDetail(String billNo);


    /***
     * 新增采购单
     * @param invoiceForm
     * @param invoiceInfo
     * @return
     */
    Integer purchaseFormSubmit(InvoiceEntity invoiceForm, List<InvoiceInfoEntity> invoiceInfo);

    // /**
    //  * 新增销货单
    //  *
    //  * @param invoiceForm
    //  * @param invoiceInfo
    //  * @return
    //  */
    // Integer saleFormSubmit(InvoiceEntity invoiceForm, List<InvoiceInfoEntity> invoiceInfo);

    /***
     * 获取采购单
     * @param pageIndex
     * @param pageSize
     * @param transType 交易类型
     * @param billTypeEnum 单据类型
     * @param billStatusEnum 单据状态
     * @param billNo
     * @param startDate
     * @param endDate
     * @return
     */
    PagingResponse<InvoiceEntity> getList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum,
                                          InvoiceCheckoutStatusEnum checkoutStatusEnum, InvoiceQualifiedStatusEnum qualifiedStatusEnum, String billNo, String startDate, String endDate, Integer contactId);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    Integer deleteForm(Long id);



    /**
     * 根据规格id和采购单id查询商品信息
     *
     * @param invoiceId  采购单据id
     * @param specNumber 商品规格编码
     * @return
     */
    InvoiceInfoListVo getGoodsSpecByInvoiceAndSpec(Long invoiceId, String specNumber);





    /**
     * 检验异常数量
     *
     * @param id
     * @param quantity
     * @param abnormalQuantity
     */
//    void updataCheckout(ArrayList<Integer> id, ArrayList<Integer> quantity, ArrayList<String> abnormalQuantity, String userName, InvoiceCheckoutStatusEnum checkoutStatusEnum);

    /**
     * 跳过检验
     *
     * @param id
     */
//    void updataCheckoutIt(Integer id,  String userName, Integer stutas);

    /**
     * 获取检验分页列表
     *
     * @param pageIndex
     * @param pageSize
     * @param transType 交易类型
     * @param billNo
     * @return
     */
//    PagingResponse<InvoiceEntity> getCheckoutList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, String billNo);

    /**
     * 获取已检验分页列表
     */
//    PagingResponse<InvoiceEntity> getCheckoutOk(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, InvoiceCheckoutStatusEnum checkoutStatusEnum, String billNo);

    /**
     * 获取异常分页列表
     *
     * @return
     */
    PagingResponse<InvoiceEntity> getAbnormal(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, InvoiceCheckoutStatusEnum checkoutStatusEnum, String billNo, String startDate, String endDate, String transTypes);

    /**
     * 获取退货单list
     *
     * @param pageIndex 页码
     * @param pageSize
     * @param number    单据编号
     * @param status    状态
     * @return
     */
    PagingResponse<InvoiceEntity> getPurchaseCancelList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, String number, Integer status,Integer contactId);

    /**
     * 获取采购明细list（已审核的）
     * @param pageIndex
     * @param pageSize
     * @param transType
     * @param number
     * @param billDate
     * @return
     */
    PagingResponse<InvoiceInfoListVo> getInvoiceInfoList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, String number, String billDate);

    /**
     * 根据iid查询退货单详情
     * @param iid
     * @return
     */
    List<ErpInvoiceInfoVo> getErpInvoiceInfoByIid(Long iid);
    /**
     * 采购退货商品出库
     * @param id 采购单id
     * @param ids 采购商品信息id组合
     * @param stockId 采购商品出库仓库id
     * @param number 采购商品出库数量
     * @return
     */
    ResultVo<Integer> purchaseGoodOut(Integer id, String[] ids, String[] stockId, String[] number,Integer stockInUserId, String stockInUserName);

    /**
     * 查询退货打印商品信息
     * @param id
     * @return
     */
     ErpInvoiceVo getDetailById(Long id);
    /**
     * 采购单审核
     * @param erpInvoiceId
     * @param status
     */
     public void checkErpInvoice(Integer erpInvoiceId,Integer status);

    Integer checkInvoice(Integer InvoiceId, Integer checked, String checkName);

    /**
     * 修改采购单商品规格信息
     * @param InvoiceInfoId
     * @param price
     * @param quantity
     */
    public void updInvoiceInfo(Integer erpInvoiceId,Integer InvoiceInfoId, Double price, Integer quantity);
    /**
     * 删除采购商品列表
     * @param erpInvoiceInfoId
     * @return
     */
    public ResultVo<Integer> delErpInvoiceInfo(Long erpInvoiceInfoId,Long erpInvoiceId);
    /**
     * 采购单商品添加
     * @param id
     * @param invoiceInfo
     * @return
     */
    public ResultVo<Integer> addInvoiceItem(Long id, ErpPurchaseGoodVo invoiceInfo);
    /**
     * 采购入库明细
     * @param startDate
     * @param endDate
     * @return
     */
    public List<ErpStockInFormItemVo> invoiceItemPOList (String startDate, String endDate);

    /**
     * 采购单付款
     * @param id
     * @param amount
     * @return
     */
    ResultVo<Integer> purchasePayAmount(Long id,Double amount);

    ResultVo<Integer> purchaseRefundAmount(Long id,Double amount);
}