package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.entity.ErpGoodsStockLossFormEntity;
import com.b2c.entity.erp.vo.ErpGoodsBadStockLogVo;
import com.b2c.entity.erp.vo.ErpGoodsBadStockVo;
import com.b2c.entity.erp.vo.ErpGoodsStockLossFormVo;

import java.util.List;

public interface StockBadService {

    /**
     * 查询不良品列表
     * @param pageIndex
     * @param pageSize
     * @param number
     * @return
     */
    public PagingResponse<ErpGoodsBadStockVo> getStockBadList(Integer pageIndex, Integer pageSize , String number,Integer type);

    /**
     * 根据Id查询
     * @param stockId
     * @return
     */
    public ErpGoodsBadStockVo getStockBad(Integer stockId);

    /**
     * 根据Id查询不良品详情
     * @param stockId
     * @return
     */
    public List<ErpGoodsBadStockLogVo> getBadLog(Integer stockId);

    /**
     * 关键词搜索不良商品
     *
     * @return
     */
    public List<ErpGoodsBadStockLogVo> getLIKEBad(String number);

    /**
     * 新增报损单
     * @param userId
     * @param userName
     * @param lossText
     * @param billNo
     * @param billDate
     * @param specNumbers
     * @param quantities
     * @param locationNames
     * @param remark 报损理由
     */
    void lossAdd(Integer userId ,String userName,String lossText ,String billNo , String billDate ,String[] specNumbers ,String[] quantities ,String[] locationNames,String[] remark);

    /**
     * 根据仓位和规格编号查询仓位和数量是否存在
     * @return
     */
    public ErpGoodsStockInfoEntity getLocationAndQuanty(String LocationName , String specNumber);

    /**
     * 查询报损单详情
     * @param pageIndex
     * @param pageSize
     * @param number
     * @param startTime
     * @param endTime
     * @return
     */
    public PagingResponse<ErpGoodsStockLossFormEntity> getLossFromList(Integer pageIndex, Integer pageSize , String number, String startTime , String endTime);

    /**
     * 根据Id获取报损单详情
     * @param formId
     * @return
     */
    public ErpGoodsStockLossFormVo getLossItem(Integer formId);

    /**
     * 报损单审核
     * @param id
     * @param userId
     * @param userName
     * @return
     */
    public Integer checkLoss(Integer id , Integer userId ,String userName);
}
