package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpContactAddressEntity;
import com.b2c.entity.ErpContactEntity;
import com.b2c.entity.erp.vo.ErpContactAddressVo;

public interface ClientManageService {

    /**
     * 查询客户列表
     * @param pageIndex
     * @param pageSize
     * @param name
     * @param mobile
     * @param type -10客户10供应商
     * @param category 类型99直播间
     * @return
     */
    public PagingResponse<ErpContactEntity> getClientList(Integer pageIndex, Integer pageSize, String name, String mobile,Integer type, Integer category);

    /**
     * 根据Id查询客户
     * @return
     */
    public ErpContactEntity getClientById(Integer id);

    /**
     * 新增客户
     * @param entity
     * @return
     */
    public Integer addManage(ErpContactEntity entity);

    void addContact(Integer categoryId,String name,String place,String contact,String address,String remark);

    /**
     * 修改客户
     * @param entity
     * @return
     */
    public Integer updManage(ErpContactEntity entity);

    /**
     * 删除客户
     * @param id
     * @return
     */
    public Integer delManage(Integer id);

    /**
     * 查询详细地址
     * @param contactId
     * @return
     */
    public ErpContactAddressVo getErpContactAddress(Integer contactId);

    /**
     * 修改主表地址
     * @param userId
     * @param consignee
     * @param mobile
     * @param areaNameArray
     * @param address
     * @return
     */
    public Integer updAddress(int userId, String consignee, String mobile, String[] areaNameArray,String address);

    /**
     * 根据Id查询子地址详情
     * @param contactId
     * @return
     */
    public ErpContactAddressEntity getAddressIn(Integer contactId);

    /**
     * 新增子地址
     * @param userId
     * @param consignee
     * @param mobile
     * @param areaNameArray
     * @param areaCodeArray
     * @param address
     * @return
     */
    public Integer addAddressIn(Integer userId, String consignee, String mobile, String[] areaNameArray, String[] areaCodeArray, String address);

    /**
     * 修改子地址
     * @param addressId
     * @param consignee
     * @param mobile
     * @param areaNameArray
     * @param areaCodeArray
     * @param address
     * @return
     */
    public Integer updAddressIn(Integer addressId, String consignee, String mobile, String[] areaNameArray, String[] areaCodeArray, String address);

    /**
     * 删除子地址
     * @param addressId
     * @return
     */
    public Integer delAddressIn(Integer addressId);
}
