package com.b2c.interfaces.kwai;


import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.kwai.DcKwaiOrderEntity;
import com.b2c.entity.result.ResultVo;

public interface DcKwaiOrderService {
     ResultVo<Integer> editKwaiOrder(DcKwaiOrderEntity order);

     PagingResponse<DcKwaiOrderEntity> getKwaiOrders(Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String state);

     DcKwaiOrderEntity getOderDetailByOrderId(Long id);

     ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId,Integer quantity);

     ResultVo<Integer> kwaiOrderAffirm(Long dcKwaiOrdersId,Integer clientId, String receiver, String mobile, String address, String sellerMemo);

     void updKwaiOrderStatus(Long KwaiOid);

     ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity);


}
