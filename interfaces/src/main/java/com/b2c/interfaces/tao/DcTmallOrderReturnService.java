package com.b2c.interfaces.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.tao.TmallOrderRefundVo;

public interface DcTmallOrderReturnService {
    
    PagingResponse<TmallOrderRefundVo> getList(Integer shopId,Integer pageIndex, Integer pageSize, String orderId, String refundId,String logisticsCode);
}
