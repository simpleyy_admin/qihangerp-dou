package com.b2c.interfaces;

import com.b2c.entity.ExpressCompanyEntity;
import com.b2c.entity.OrderCancelEntity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-08 14:47
 */
public interface ExpressCompanyService {

    /**
     * 获取快递公司列表
     *
     * @return
     */
    List<ExpressCompanyEntity> getExpressCompany();

    ExpressCompanyEntity getEntityByCode(String code);

    /**
     * 根据快递公司名查询
     *
     * @param name
     * @return
     */
    String getCodeByName(String name);

    /**
     * 根据快递公司编码查询
     *
     * @param code
     * @return
     */
    String getNameByCode(String code);

    /**
     * 更新
     * @param code
     * @param name
     */
    void update(String code,String name);
}
