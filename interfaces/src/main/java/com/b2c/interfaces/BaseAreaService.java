package com.b2c.interfaces;

import com.b2c.entity.result.ResultVo;
import com.b2c.entity.BaseAreaEntity;

import java.util.List;

/**
 * 描述：
 * 基础地区Service
 *
 * @author qlp
 * @date 2019-01-30 14:00
 */
public interface BaseAreaService {

    /**
     * 根据地区code查询子类
     *
     * @param parentCode
     * @return
     */
    ResultVo<List<BaseAreaEntity>> getListByParent(String parentCode);
}
