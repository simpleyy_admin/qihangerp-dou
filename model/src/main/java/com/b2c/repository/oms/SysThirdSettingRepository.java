package com.b2c.repository.oms;

import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.datacenter.DcSysThirdSettingEntity;
import com.b2c.repository.Tables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 描述：
 * 第三方开放平台设置Repository
 *
 * @author qlp
 * @date 2019-09-12 11:45
 */
@Repository
public class SysThirdSettingRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 查询店铺列表
     * @return
     */
    public DcSysThirdSettingEntity getEntity(Integer id){
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT * FROM  "+ Tables.DcSysThirdSetting+ " WHERE id=?");
        try {
            var entity = jdbcTemplate.queryForObject(sb.toString(), new BeanPropertyRowMapper<>(DcSysThirdSettingEntity.class), id);
            return entity;
        }catch (Exception e){
            return null;
        }

    }

    public void updateEntity(Integer id,String access_token, String refresh_token, Integer expires_in, String thirdId) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE "+Tables.DcSysThirdSetting);
        sb.append(" SET ");
        sb.append(" access_token=?,expires_in=?,access_token_begin=?,refresh_token=?,refresh_token_timeout=?,modify_on=?");
        sb.append(" WHERE third_id= ?");
        jdbcTemplate.update(sb.toString(),access_token,expires_in,System.currentTimeMillis()/1000,refresh_token,0,System.currentTimeMillis()/1000,thirdId);
    }
}
