package com.b2c.repository;

/**
 * 描述：
 * 数据表
 *
 * @author qlp
 * @date 2018-12-19 12:17 PM
 */
public class Tables {
    /**
     * 商品分类表
     */
    public static String GoodsCategory = " goods_category ";
    /**
     * 商品分类属性表
     */
    public static String GoodsCategoryAttribute = " goods_category_attribute ";
    public static String GoodsCategoryAttributeValue = " goods_category_attribute_value ";
    /**
     * 运费模板表
     */
    public static String FreightTemplate = " freight_template ";
    /**
     * 运费模板详情表
     */
    public static String FreightTemplateDetail = "freight_template_detail";

    /**
     * 商品表
     */
    public static String Goods = " goods ";
    /**
     * 商品信息表
     */
    public static String O_Goods = "o_goods";

    /**
     * 商品规格
     */
    public static String GoodsSpec = " goods_spec ";
    public static String GoodsSpecAttr = " goods_spec_attr ";

    /**
     * 商品规格
     */
    public static String O_GoodsSpec = " o_goods_spec ";
    /**
     * 商品详情
     */
    public static String GoodsDetail = " goods_detail ";
    /**
     * 商品附件
     */
    public static String GoodsAttachment = " goods_attachment ";

    /**
     * 商品属性
     */
    public static String GoodsAttribute = " goods_attribute ";
    public static String GoodsComment = " goods_commend ";
    public static String GoodsCommentItem = " goods_commend_item ";

    /**
     * 商品评论表
     */
    public static String OrderGoodsComment = " order_goods_comment ";

    /**
     * 管理员用户表
     */
    public static String ManageUser = " manage_user ";
    public static String DcManageUser="dc_manage_user";


    /**
     * 仓库管理员用户表
     */
    public static String SysManageUser = " sys_manage_user ";
    public static String SysManageGroup = " sys_manage_group ";
    public static String SysManagePermission = " sys_manage_permission ";
    public static String SysManageUserPermission = " sys_manage_user_permission ";
    public static String SysManageLoginLog = " sys_manage_login_log ";

    /**
     * banner表
     */
    public static String Banners = "banners";

    /**
     * 用户表
     */
    public static String User = " user ";

    /**
     * 用户购物车表
     */
    public static String UserShoppingCart = " user_shopping_cart ";

    /**
     * 用户结算
     */
    public static String UserShoppingSettle = " user_shopping_settle ";

    /**
     * 用户分组
     */
    public static String UserSalesMan = "user_sales_man";

    /**
     * 用户地址
     */
    public static String UserAddress = " user_address ";
    /**
     * 用户站内信
     */
    public static String UserMessage = " user_message ";
    public static String Order = " orders ";
    public static String OrderItem = " order_item ";
    public static String OrderLogistics = " order_logistics ";
    public static String OrderLogs = " order_logs ";
    public static String OrderSalesmanPayLog="order_salesman_pay_log";
    public static String UserGroup = " user_group ";
    public static String OrderReward = "order_reward";
//    public static String GoodsInventoryDetail = " goods_inventory_detail ";//商品库存记录表
    public static String OrderCancel = " order_cancel ";//退货订单
    public static String OrderCancelItem = " order_cancel_item ";//退货订单明细
    public static String BaseArea = " base_areas ";
    public static String UserFavorite = " user_favorite ";
    public static String UserUpgradeLog = " user_upgrade_log ";
    public static String UserBalanceLog = " user_balance_log";
    public static String UserWithdraw = " user_withdraw ";
    public static String SysConfig = " sys_config ";//系统配置表
    public static String ExpressCompany = " erp_express_company ";//快递公司表

    public static String ErpGoodsCategory = " erp_goods_category"; //仓库商品分类
    public static String ErpGoodsCategoryAttribute = " erp_goods_category_attribute "; //仓库商品分类属性
    public static String ErpGoodsCategoryAttributeValue = " erp_goods_category_attribute_value "; //仓库商品分类属性值
    public static String ErpGoods = " erp_goods"; //仓库商品分类
    //    public static String ErpSupplier = " erp_supplier ";//供应商表
    public static String ErpGoodsSpec = " erp_goods_spec ";//商品规格表
    public static String ErpStockInForm = " erp_stock_in_form ";//入库表单
    public static String ErpStockInFormItem = " erp_stock_in_form_item ";//入库表单明细

    public static String ErpGoodsSpecAttr = "erp_goods_spec_attr";//商品规格属性表
    public static String ErpGoodsStockInfo = " erp_goods_stock_info ";//商品仓库库存表
    public static String ErpGoodsStockInfoItem = " erp_goods_stock_info_item ";//商品仓库库存表
    public static String ErpGoodsStockLogs = " erp_goods_stock_logs ";//商品仓库库存日志表
    public static String ErpGoodsStockLossForm = " erp_goods_stock_loss_form ";//报损单
    public static String ErpGoodsStockLossItem = " erp_goods_stock_loss_item ";//报损单
//    public static String ErpStoreHouse = " erp_stock_location ";//仓库表
    public static String ErpInvoice = " erp_invoice ";//购货销货表单表
    public static String ErpInvoiceInfo = " erp_invoice_info ";//购货销货表单明细表
    public static String ErpInvoiceImg = " erp_invoice_img ";//购货销货表单图片表
    public static String ErpUnit = " erp_unit ";//计量表
    public static String ErpContact = " erp_contact ";//联系人表（供应商、客户）
    public static String ErpStaff = " erp_staff ";//工作人员
    public static String ErpGoodsSpecMoveFrom = "erp_goods_spec_move_form";//移库表

    public static String SalesPromotion = " sales_promotion ";
    public static String SalesPromotionGoods = " sales_promotion_goods ";
    public static String ErpInvoicePicking = " erp_invoice_picking ";//拣货单
    public static String ErpInvoicePickingInfo = " erp_invoice_picking_info ";//拣货单字表

//    public static String ErpReservoir = " erp_stock_location ";//库区管理
    public static String ErpStockLocation = " erp_stock_location ";//货架管理

    //    public static String ErpStockOutGoods = " erp_stock_out_goods ";//出库商品表
    public static String ErpStockOutForm = " erp_stock_out_form ";//出库单据表
    public static String ErpStockOutFormItem = " erp_stock_out_form_item ";//出库单据表
    public static String ErpStockOutLockedLogs = " erp_stock_out_locked_logs ";//出库单锁定库存日志
    public static String ErpOrder = " erp_order "; //Erp订单表
    public static String ErpOrderItem = " erp_order_item "; //Erp订单表item
    public static String ErpOrderReturn = " erp_order_return "; //Erp退货订单表
    public static String ErpOrderReturnItem = " erp_order_return_item "; //Erp退货订单表item

    public static String ErpSalesOrder = " erp_sales_order "; //Erp订单表
    public static String ErpSalesOrderItem = " erp_sales_order_item "; //Erp订单表item
    public static String ErpSalesOrderRefund = " erp_sales_order_refund "; //Erp退货订单表
    public static String ErpSalesOrderRefundItem = " erp_sales_order_refund_item "; //Erp退货订单表item

    public static String ErpStockInCheckout = " erp_stock_in_checkout "; //
    public static String ErpStockInCheckoutItem = " erp_stock_in_checkout_item "; //

    /**
     * 不良品
     */
    public static String ErpGoodsBadStock = "erp_goods_bad_stock";
    public static String ErpGoodsBadStockLog = "erp_goods_bad_stock_log";
    /**盘点**/
    public static String ErpStocktakingInvoice = " erp_stocktaking_invoice ";
    public static String ErpStocktakingInvoiceItem = " erp_stocktaking_invoice_item ";
    public static String ErpLiveData = " erp_live_data ";
    public static String ErpLiveExpenses = " erp_live_expenses ";
    /**
     * 有赞商品列表
     */
    public static String DcYzGoods="dc_yz_goods";
    public static String DcYzGoodsSku="dc_yz_goods_sku";
    public static String dcYzOrder = "dc_yz_order";
    public static String dcYzOrderAddress = "dc_yz_order_address";
    public static String dcYzOrderItems = "dc_yz_order_items";
    public static String dcYzOrderUser = "dc_yz_order_user";

    /**
     * 有赞商品规格列表
     */
    public static String GoodsSpecYz = " goods_spec_yz";

    public static String WxTemplate = "wx_template"; //微信模板
    public static String WxTemplateBanners = "wx_template_banners";
    public static String WxTemplateGoods = "wx_template_goods";

    public static String DcSysThirdSetting = "dc_sys_third_setting"; //第三方平台设置
    public static String DcShop = "dc_shop"; //第三方平台设置
    public static String DcAliOrder = " dc_ali_order "; //阿里订单
    public static String DcAliOrderRefund = " dc_ali_order_refund "; //阿里退货订单
    public static String DcAliOrderUser = " dc_ali_order_user "; //阿里订单user
    public static String DcAliOrderItem = " dc_ali_order_items "; //阿里订单items
    public static String DcAliOrderAddress = " dc_ali_order_address "; //阿里订单address
    public static String dcAliOrderRefund = " dc_ali_order_refund "; //阿里退货
    public static String DcAliGoods = " dc_ali_goods "; //阿里退货
    public static String DcAliGoodsSku = " dc_ali_goods_sku "; //阿里退货
    public static String DcTmallOrder = " dc_tmall_order "; //阿里订单
    public static String DcTmallOrderItem = " dc_tmall_order_items "; //阿里订单items
    public static String DcTmallOrderAddress = " dc_tmall_order_address "; //阿里订单address
    public static String DcTmallOrderRefund = " dc_tmall_order_refund "; //阿里订单
//    public static String DcSysThirdSetting="dc_sys_third_setting"; //第三方平台设置
//    public static String DcPiPiOrder = " dc_pipi_order "; //批批网订单
//    public static String DcPiPiOrderItem = " dc_pipi_order_items "; //批批网订单items

    public static String PlatformAccountItems=" platform_account_items "; //平台账目明细

    public static String DcPddGoods = " dc_pdd_goods "; //拼多多退货
    public static String DcPddGoodsSku = " dc_pdd_goods_sku "; //拼多多退货
    public static String DcPddOrder = " dc_pdd_orders "; //拼多多
    public static String DcPddOrderItem = " dc_pdd_orders_item "; //拼多多
    public static String DcPddOrderRefund = " dc_pdd_refund "; //拼多多退货


    public static String DcMogujieOrder = " dc_mogujie_order "; //蘑菇街
    public static String DcMogujieOrderItem = " dc_mogujie_order_item "; //蘑菇街

    public static String DcDouyinOrder = " dc_douyin_orders "; //抖音
    public static String DcDouyinOrderItem = " dc_douyin_orders_items "; //抖音


    /*****财务系统*****/
//    public static String FmsSalesOrder = " fms_sales_order "; //销售结算单
//    public static String FmsSalesOrderItem = " fms_sales_order_item "; //销售结算单sku明细
}