package com.b2c.repository.erp;

import com.b2c.entity.ContactEntity;
import com.b2c.entity.erp.enums.ContactTypeEnum;
import com.b2c.repository.Tables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 描述：
 * 供应商Repository
 *
 * @author qlp
 * @date 2019-03-21 13:48
 */
@Repository
public class ContactRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 获取联系人列表
     *
     * @param type
     * @return
     */
    public List<ContactEntity> getList(ContactTypeEnum type) {
        String sql = "SELECT * FROM " + Tables.ErpContact + " WHERE type=? and disable=0 and isDelete=0 ";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(ContactEntity.class), type.getIndex());
    }
}
