package com.b2c.service.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpOrderReturnEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.ErpOrderReturnRepository;
import com.b2c.interfaces.erp.ErpOrderReturnService;
import com.b2c.entity.vo.ErpOrderReturnItemVo;
import com.b2c.entity.vo.ErpOrderReturnVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-15 15:15
 */
@Service
public class ErpOrderReturnServiceImpl implements ErpOrderReturnService {
    @Autowired
    private ErpOrderReturnRepository repository;

    @Override
    public PagingResponse<ErpOrderReturnVo> getList(int pageIndex, int pageSize, String refNum, String orderNum, String logisticsCode,String refMobile,Integer status) {
        return repository.getList(pageIndex, pageSize, refNum, orderNum, logisticsCode,refMobile,status);
    }

    @Override
    public List<ErpOrderReturnItemVo> getItemListById(Long returnOrderId) {
        return repository.getItemListById(returnOrderId);
    }

    @Override
    public ErpOrderReturnEntity getById(Long returnOrderId) {
        ErpOrderReturnEntity order = repository.getById(returnOrderId);
        if (StringUtils.isEmpty(order.getLogisticsCompany()) || StringUtils.isEmpty(order.getLogisticsCode())) {
            order.setLogisticsCode("");
            order.setLogisticsCompany("");
        }
        return order;
    }

    @Override
    public ResultVo<Integer> confirmReceiveAndStockIn(Long returnOrderId,String[] returnItemIds,String[] locationIds,Integer userId, String trueName) {
        return repository.confirmReceiveAndStockIn(returnOrderId,returnItemIds,locationIds,userId,trueName);
    }

    @Override
    public ErpOrderReturnVo getStockInfo(Long id) {
        return repository.getStockInfo(id);
    }

    @Override
    public Integer checkStockInfo(String specNumber, String locationNmae) {
        return repository.checkStockInfo(specNumber, locationNmae);
    }

    @Override
    public void erp_order_return_cancel(Long erpOrderReturnId) {
        repository.erp_order_return_cancel(erpOrderReturnId);
    }
    //    @Override
//    public Integer returnOrderStockIn(String[] locationIds, String[] goodsIds, String[] specIds, String[] quantities, String[] itemIds, Integer userId, String userName, String billNo, Long returnId) {
//        int count = 0;
//        if (specIds.length > 0) {
//            for (int i = 0; i < goodsIds.length; i++) {
//                Long quantity = Long.parseLong(quantities[i]);
//                if (quantity.longValue() > 0) {
//                    repository.returnOrderStockIn(Integer.parseInt(locationIds[i]), Integer.parseInt(goodsIds[i]), Integer.parseInt(specIds[i]), Long.parseLong(quantities[i]), Integer.parseInt(itemIds[i]), userId, userName, billNo, returnId);
//                    count++;
//                }
//            }
//        }
//        return count;
//    }

  /*  @Override
    public Integer checkBadStock(String specNumber, String locationNmae) {
        return repository.checkBadStock(specNumber,locationNmae);
    }*/

    @Override
    public Integer returnOrderStockBadIn(String[] goodsIds, String[] specNumbers, String[] quantities, String[] itemIds, String billNo, Integer returnId) {
        for (int i = 0; i < goodsIds.length; i++)
            repository.returnOrderStockBadIn(Integer.parseInt(goodsIds[i]), specNumbers[i], Long.parseLong(quantities[i]), Integer.parseInt(itemIds[i]), billNo, returnId);
        return 0;
    }

    @Override
    public PagingResponse<ErpOrderReturnItemVo> getReturnCompleteItemList(Integer pageIndex, Integer pageSize, String orderNum, String skuNumber, Integer startTime, Integer endTime) {
        return repository.getReturnCompleteItemList( pageIndex, pageSize, orderNum, skuNumber, startTime, endTime);
    }

    @Override
    public List<ErpOrderReturnItemVo> getReturnCompleteItemListForExcel(String orderNum, String skuNumber, Integer startTime, Integer endTime) {
        return repository.getReturnCompleteItemListForExcel(orderNum,skuNumber,startTime,endTime);
    }

}
