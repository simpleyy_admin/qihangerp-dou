package com.b2c.service.erp;

import com.b2c.entity.UserEntity;
import com.b2c.repository.erp.ErpSalesUserRepository;
import com.b2c.interfaces.erp.ErpSalesUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErpSalesUserServiceImpl implements ErpSalesUserService {
    @Autowired
    private ErpSalesUserRepository repository;
    @Override
    public List<UserEntity> getDeveloperUserList() {
        return repository.getDeveloperUserList();
    }
    @Override
    public List<UserEntity> getUserListByDeveloperId(Integer developerId) {
        return repository.getUserListByDeveloperId(developerId);
    }

    @Override
    public List<UserEntity> getUserList() {
        return repository.getUserList();
    }
}
