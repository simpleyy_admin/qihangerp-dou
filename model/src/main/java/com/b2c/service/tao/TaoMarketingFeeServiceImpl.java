package com.b2c.service.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoMarketingFeeEntity;
import com.b2c.interfaces.tao.TaoMarketingFeeService;
import com.b2c.repository.tao.TaoMarketingFeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaoMarketingFeeServiceImpl implements TaoMarketingFeeService {
    @Autowired
    private TaoMarketingFeeRepository repository;
    @Override
    public PagingResponse<TaoMarketingFeeEntity> getList(Integer pageIndex, Integer pageSize, Integer shopId, Integer type,String source, String startTime, String endTime) {
        return repository.getList(pageIndex, pageSize, shopId, type,source, startTime, endTime);
    }

    @Override
    public ResultVo<String> importExcelFeeList(List<TaoMarketingFeeEntity> list, Integer shopId, String source) {
        return repository.importExcelFeeList(list, shopId,source);
    }
}
