package com.b2c.service.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.apitao.DcTmallOrderCostEntity;
import com.b2c.entity.apitao.TaoOrderSalesDataEntity;
import com.b2c.entity.apitao.UpdOrderTagReq;
import com.b2c.entity.query.OrderItemSwapReq;
import com.b2c.entity.query.TaoOrderQuery;
import com.b2c.entity.query.TmallOrderCostQuery;
import com.b2c.entity.datacenter.DcTmallOrderEntity;
import com.b2c.entity.datacenter.DcTmallOrderItemEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.oms.DcTmallOrderRepository;
import com.b2c.repository.oms.OrderConfirmRepository;
import com.b2c.entity.vo.erp.ErpSalesOrderDetailVo;
import com.b2c.entity.vo.finance.FinanceOrderItemListVo;
import com.b2c.entity.vo.finance.FinanceOrderListVo;
import com.b2c.entity.vo.OrderImportPiPiEntity;

import com.b2c.interfaces.tao.DcTmallOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-23 15:30
 */
@Service
public class DcTmallOrderServiceImpl implements DcTmallOrderService {
    @Autowired
    private DcTmallOrderRepository repository;
    @Autowired
    private OrderConfirmRepository orderConfirmRepository;

//    @Override
//    public ResultVo<Integer> importExcelOrder(EnumTmallOrderSource orderSource, Integer shopId, Integer orderType, Integer developerId, Integer clientId, List<DcTmallOrderEntity> orderList) {
//        return repository.importExcelOrder(orderSource, shopId, orderType, developerId, clientId, orderList);
//    }

    @Override
    public ResultVo<Integer> updateTmallOrderForOpenTaobao(Integer shopId, DcTmallOrderEntity order) {
        return repository.updateOrder(shopId, order);
    }

//    @Override
//    public PagingResponse<DcTmallOrderEntity> getList(EnumTmallOrderSource orderSource, Integer pageIndex, Integer pageSize, String orderId, Integer status, Integer shopId, String receiverMobile, String startTime, String endTime) {
//        return repository.getList(orderSource, pageIndex, pageSize, orderId, status, shopId, receiverMobile, startTime, endTime);
//    }

    @Override
    public PagingResponse<DcTmallOrderEntity> getList(Integer pageIndex, Integer pageSize, String orderId, Integer status, Integer shopId, String receiverMobile,Integer developerId, String startTime, String endTime) {
        return repository.getList(pageIndex, pageSize, orderId, status, shopId, receiverMobile,developerId, startTime, endTime);
    }

    @Override
    public PagingResponse<DcTmallOrderEntity> getNewList(TaoOrderQuery reqData) {
        return repository.getNewList(reqData);
    }

    @Override
    public PagingResponse<FinanceOrderItemListVo> getOrderItemList(Integer pageIndex, Integer pageSize, String orderId, Integer sendStatus, String receiverMobile,String specNumber, String startTime, String endTime) {
        return repository.getOrderItemList(pageIndex, pageSize, orderId, sendStatus, receiverMobile,specNumber, startTime, endTime);
    }

    @Override
    public List<FinanceOrderItemListVo> getOrderItemListForExcel(String orderId, Integer sendStatus, String receiverMobile,String specNumber, String startTime, String endTime) {
        return repository.getOrderItemListForExcel(orderId, sendStatus, receiverMobile,specNumber, startTime, endTime);
    }

    @Override
    public PagingResponse<FinanceOrderListVo> getOrderList(Integer pageIndex, Integer pageSize, String orderId, Integer sendStatus, String receiverMobile, Integer developerId, String startTime, String endTime) {
        return repository.getOrderList(pageIndex, pageSize, orderId, sendStatus, receiverMobile, developerId, startTime, endTime);
    }

    @Override
    public List<FinanceOrderListVo> getOrderListForExcel(String orderId, Integer sendStatus, String receiverMobile, Integer developerId, String startTime, String endTime) {
        return repository.getOrderListForExcel(orderId, sendStatus, receiverMobile, developerId, startTime, endTime);
    }

//    @Override
//    public List<DcTmallOrderEntity> getListExport(String startDate, String endDate) {
//        return repository.getListExport(startDate, endDate);
//    }

    @Override
    public DcTmallOrderEntity getOrderDetailAndItemsById(Long orderId) {
        return repository.getOrderDetailAndItemsById(orderId);
    }

    @Override
    public DcTmallOrderEntity getOrderEntityById(Long orderId) {
        return repository.getOrderEntityById(orderId);
    }

    @Override
    public List<DcTmallOrderItemEntity> getOrderItemsByOrderId(Long orderId) {
        return repository.getOrderItemsByOrderId(orderId);
    }

    /**
     * 订单确认并加入到仓库发货队列
     *
     * @param orderId
     * @return
     */
    @Override
    public ResultVo<Integer> orderConfirmAndJoinDeliveryQueueForTmall(Long orderId, Integer clientId, String receiver, String mobile, String address, String sellerMemo) {
        return orderConfirmRepository.orderConfirmAndJoinDeliveryQueueForTao(orderId, clientId, receiver, mobile, address, sellerMemo);
    }


    @Override
    public Integer updGoodTmallSpec(Integer itemId, Integer specId) {
        return repository.updGoodTmallSpec(itemId, specId);
    }

//    @Override
//    public ResultVo<Integer> addTaoBaoOrderCancel(Long orderId, String[] itemIds, String[] nums) {
//        return repository.addTaoBaoOrderCancel(orderId, itemIds, nums);
//    }

   @Override
   public ResultVo<Long> addTaoOrderRefund(Long orderItemId, String refundId) {
       return repository.addTaoOrderRefund(orderItemId, refundId);
   }

//    @Override
//    public PagingResponse<RefundOrderListVo> getTmallAfterOrders(Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String returnOrderNum, String buyerName, Integer state, Integer orderSource) {
//        return repository.getTmallAfterOrders(pageIndex, pageSize, orderNum, startTime, endTime, returnOrderNum, buyerName, state, orderSource);
//    }

//    @Override
//    public ResultVo<Integer> reviewRefundTmall(Long id, Integer state, ExpressInfoVo exress) {
//        return repository.reviewRefundTmall(id, state, exress);
//    }

//    @Override
//    public ResultVo<Integer> addTmallOrderByDeveLoper(DcTmallOrderEntity order, Integer buyUserId, Integer orderType, ErpOrderSourceEnum sourceEnum) {
//        return repository.addTmallOrderByDeveLoper(order, buyUserId, orderType, sourceEnum);
//    }

    @Override
    public void batchUpdateTmallOrderReceiver(List<DcTmallOrderEntity> orderList) {
        repository.batchUpdateTmallOrderReceiver(orderList);
    }

    @Override
    public ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId, Integer quantity) {
        return repository.updOrderSpec(orderItemId,erpGoodSpecId,quantity);
    }

    @Override
    public void updOderTag(UpdOrderTagReq req) {
        repository.updOderTag(req);
    }

    @Override
    public void updOrderTagCost(List<DcTmallOrderCostEntity> costs) {
        repository.updOrderTagCost(costs);
    }

    @Override
    public PagingResponse<DcTmallOrderCostEntity> orderCosts(TmallOrderCostQuery query) {
        return repository.orderCosts(query);
    }

    @Override
    public void batchUpdateTmallOrderCost(List<DcTmallOrderCostEntity> costList) {
        repository.batchUpdateTmallOrderCost(costList);
    }

    @Override
    public ResultVo<Integer> addHySalesOrder(ErpSalesOrderDetailVo salesOrder) {
        return repository.addHySalesOrder(salesOrder);
    }

    @Override
    public ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity) {
        return repository.addOrderGift(orderId,erpGoodsId,erpGoodSpecId,quantity);
    }

    @Override
    public PagingResponse<TaoOrderSalesDataEntity> salesOrderDataList(Integer shopId, Integer pageIndex, Integer pageSize, String startTime, String endTime) {
        return repository.salesOrderDataList(shopId,pageIndex,pageSize,startTime,endTime);
    }

    @Override
    public ResultVo<Integer> orderItemSwap(OrderItemSwapReq req) {
        return repository.orderItemSwap(req);
    }

    @Override
    public void updTaoOrderStatus(Long orderId, Integer status) {
        repository.updTaoOrderStatus(orderId,status);
    }

    @Override
    public void orderSend(Long orderId,String company,String code){
        repository.orderSend(orderId,company,code);
    }

    @Override
    public void editRefundLogisticsCode(Long orderId, String company, String code,String address) {
        repository.editRefundLogisticsCode(orderId,company,code,address);
        
    }

    @Override
    public ResultVo<String> signRefund(Long refundId, Integer auditStatus,String remark) {
       return repository.signRefund(refundId, auditStatus,remark);
    }

    @Override
    public ResultVo<String> importExcelOrder(List<OrderImportPiPiEntity> orderList,Integer shopId) {
       
        return repository.importExcelOrder(orderList,shopId);
    }
}
