package com.b2c.service.tao;

import com.b2c.interfaces.tao.TaoGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoGoodsEntity;
import com.b2c.repository.tao.TaoGoodsRepository;

@Service
public class TaoGoodsServiceImpl implements TaoGoodsService {

    @Autowired
    private TaoGoodsRepository repository;

    @Override
    public ResultVo<Long> addGoods(TaoGoodsEntity goodsEntity) {
     
        return repository.addGoods(goodsEntity);
    }

    @Override
    public PagingResponse<TaoGoodsEntity> getGoodsList(Integer shopId, Integer pageIndex, Integer pageSize,
            String num) {
        return repository.getGoodsList(shopId, pageIndex, pageSize, num);
    }
    
}
