package com.b2c.service.order;

import com.b2c.entity.ShopRefundOrderEntity;

import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.ShopRefundOrderRepository;
import com.b2c.interfaces.ShopRefundOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ShopRefundOrderServiceImpl implements ShopRefundOrderService {

    @Autowired
    private ShopRefundOrderRepository repository;


    @Override
    public ResultVo<List<ShopRefundOrderEntity>> getShopRefundOrder(String logisticsCode, Integer shopType) {
        if(StringUtils.isEmpty(logisticsCode)) return new ResultVo<>(EnumResultVo.ParamsError,"缺少参数logisticsCode");
        // if(StringUtils.isEmpty(shopType)) return new ResultVo<>(EnumResultVo.ParamsError,"缺少参数shopType");

        // if(shopType.intValue() != 8 && shopType.intValue() != 99) return new ResultVo<>(EnumResultVo.ParamsError,"暂时不支持<抖店>及<ERP系统>以外的平台");

        List<ShopRefundOrderEntity> list = repository.getShopRefundOrder(logisticsCode,shopType);

        return new ResultVo<>(EnumResultVo.SUCCESS,list);
    }


    @Override
    public ResultVo<Integer> refundReceiveConfirm(Long refundId, Integer shopType) {
        return repository.refundReceiveConfirm(refundId, shopType);
    }


    @Override
    public ResultVo<Integer> signRefund(Long refundId, Integer shopType, Integer status, String remark) {
        return repository.signRefund(refundId, shopType, status, remark);
    }


    @Override
    public ResultVo<Integer> signRemark(Long refundId, Integer shopType, String remark) {
        return repository.signRemark(refundId, shopType, remark);
    }
}
