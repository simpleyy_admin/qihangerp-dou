package com.b2c.service;

import com.b2c.entity.datacenter.DcSysThirdSettingEntity;
import com.b2c.repository.oms.SysThirdSettingRepository;
import com.b2c.interfaces.SysThirdSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-12 11:53
 */
@Service
public class SysThirdSettingServiceImpl implements SysThirdSettingService {
    @Autowired
    private SysThirdSettingRepository repository;


    @Override
    public DcSysThirdSettingEntity getEntity(Integer id) {
        return repository.getEntity(id);
    }

    @Override
    public void updateEntity(Integer id, String access_token, String refresh_token, Integer expires_in, String thirdId) {
        repository.updateEntity(id, access_token, refresh_token, expires_in, thirdId);
    }
}
