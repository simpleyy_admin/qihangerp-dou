package com.b2c.service.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsOrderEntity;
import com.b2c.repository.XhsOrderRepository;
import com.b2c.interfaces.xhs.XhsOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XhsOrderServiceImpl implements XhsOrderService {
    @Autowired
    private XhsOrderRepository orderRepository;
    @Override
    public ResultVo<Long> pullOrder(XhsOrderEntity order) {
        return orderRepository.pullOrder(order);
    }

    @Override
    public PagingResponse<XhsOrderEntity> getOrderList(Integer shopId, Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, Integer state, String logisticsCode) {
        return orderRepository.getOrderList(shopId,pageIndex,pageSize,orderNum,startTime,endTime,state,logisticsCode);
    }

    @Override
    public XhsOrderEntity getOrderDetailById(Long id) {
        return orderRepository.getOrderDetailById(id);
    }

    @Override
    public ResultVo<Long> pullOrderAddress(Long id, String province, String city, String district, String address, String receiverName, String receiverPhone) {
        return orderRepository.pullOrderAddress(id, province, city, district, address, receiverName, receiverPhone);
    }

    @Override
    public ResultVo<Integer> updOrderRemark(Long id, String remark) {
        return orderRepository.updOrderRemark(id, remark);
    }


}
