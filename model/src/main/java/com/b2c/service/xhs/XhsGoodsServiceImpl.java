package com.b2c.service.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsGoodsEntity;
import com.b2c.repository.XhsGoodsRepository;
import com.b2c.interfaces.xhs.XhsGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XhsGoodsServiceImpl  implements XhsGoodsService {
    @Autowired
    private XhsGoodsRepository repository;
    @Override
    public ResultVo<Long> pullGoods(XhsGoodsEntity goodsEntity) {
        return repository.pullGoods(goodsEntity);
    }

    @Override
    public PagingResponse<XhsGoodsEntity> getGoodsList(Integer shopId, Integer pageIndex, Integer pageSize, String goodsNum) {
        return repository.getGoodsList(shopId, pageIndex, pageSize, goodsNum);
    }

    @Override
    public ResultVo<Integer> linkErpGoodsSpec(Long xhsGoodsSpecId, String erpCode) {
        return repository.linkErpGoodsSpec(xhsGoodsSpecId, erpCode);
    }

    @Override
    public XhsGoodsEntity getGoodsSpecByXhsSkuId(String xhsSkuId) {
        return repository.getGoodsSpecByXhsSkuId(xhsSkuId);
    }
}
