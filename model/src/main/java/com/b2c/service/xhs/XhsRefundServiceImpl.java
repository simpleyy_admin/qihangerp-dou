package com.b2c.service.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsRefundEntity;
import com.b2c.repository.XhsRefundRepository;
import com.b2c.interfaces.xhs.XhsRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XhsRefundServiceImpl implements XhsRefundService {
    @Autowired
    private XhsRefundRepository repository;

    @Override
    public ResultVo<Long> pullRefund(XhsRefundEntity refund) {
        return repository.pullRefund(refund);
    }

    @Override
    public PagingResponse<XhsRefundEntity> getRefundList(Integer shopId, Integer pageIndex, Integer pageSize) {
        return repository.getRefundList(shopId, pageIndex, pageSize);
    }

    @Override
    public XhsRefundEntity getRefundDetailById(Long id) {
        return repository.getRefundDetailById(id);
    }

    @Override
    public void agreeRefundOnly(Long id) {
        repository.agreeRefundOnly(id);
    }
}
