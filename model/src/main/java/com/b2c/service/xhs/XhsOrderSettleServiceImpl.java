package com.b2c.service.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsOrderSettleEntity;
import com.b2c.repository.XhsOrderSettleRepository;
import com.b2c.interfaces.xhs.XhsOrderSettleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XhsOrderSettleServiceImpl implements XhsOrderSettleService {
    @Autowired
    private XhsOrderSettleRepository repository;

    @Override
    public ResultVo<Long> addOrderSettle(XhsOrderSettleEntity entity) {
        return repository.addOrderSettle(entity);
    }

    @Override
    public PagingResponse<XhsOrderSettleEntity> getList(Integer pageIndex, Integer pageSize, String orderNum, String startDate, String endDate, Integer type) {
        return repository.getList(pageIndex, pageSize, orderNum, startDate, endDate, type);
    }
}
