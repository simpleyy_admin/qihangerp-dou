package com.b2c.service;

import com.b2c.repository.ScheduledRepository;
import com.b2c.interfaces.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduledServiceImpl  implements ScheduledService {
    @Autowired
    private ScheduledRepository repository;

    @Override
    public void addWmsSalesData(String date) {
        repository.addWmsSalesData(date);
    }
}
