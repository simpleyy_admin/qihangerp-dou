package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStockInFormEntity;
import com.b2c.repository.erp.ErpStockInFormRepository;
import com.b2c.interfaces.wms.ErpStockInFormService;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;
import com.b2c.entity.query.ErpStockInItemQuery;
import com.b2c.entity.vo.ErpStockInFormDetailVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-09 15:12
 */
@Service
public class ErpStockInFormServiceImpl implements ErpStockInFormService {
    @Autowired
    private ErpStockInFormRepository repository;

    @Override
    public ErpStockInFormDetailVo getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public List<ErpStockInFormEntity> getListByCheckoutId(Long checkoutId) {
        return repository.getListByCheckoutId(checkoutId);
    }

    @Override
    public PagingResponse<ErpStockInFormEntity> getErpStockInList(Integer pageIndex, Integer pageSize,Integer inType, String number, Integer startDate, Integer endDate,Long invoiceId) {
        return repository.getErpStockInList(pageIndex, pageSize,inType, number, startDate, endDate, invoiceId);
    }

    @Override
    public PagingResponse<ErpStockInFormItemVo> getErpStockInItemListByPurchase(Integer pageIndex, Integer pageSize, String billNo, String contractNo, String checkoutNumber,String specNumber, Integer startTime, Integer endTime) {
        return repository.getErpStockInItemListByPurchase(pageIndex, pageSize, billNo, contractNo,checkoutNumber,specNumber, startTime, endTime);
    }

    @Override
    public List<ErpStockInFormItemVo> getErpStockInItemListByPurchaseForExcel(String billNo, String contractNo, String stockInNumber, Integer startTime, Integer endTime) {
        return repository.getErpStockInItemListByPurchaseForExcel(billNo, contractNo,stockInNumber, startTime, endTime);
    }

    @Override
    public PagingResponse<ErpStockInFormItemVo> getErpStockInItemListByRefund(Integer pageIndex, Integer pageSize, String refundNo, Integer startTime, Integer endTime) {
        return repository.getErpStockInItemListByRefund(pageIndex,pageSize,refundNo,startTime,endTime);
    }

    @Override
    public List<ErpStockInFormItemVo> getErpStockInItemListByRefundForExcel(String refundNo, Integer startTime, Integer endTime) {
        return repository.getErpStockInItemListByRefundForExcel( refundNo,  startTime,  endTime) ;
    }

    @Override
    public PagingResponse<ErpStockInFormItemVo> purchase_in_list_count(ErpStockInItemQuery query) {
        return repository.purchase_in_list_count(query);
    }

    @Override
    public List<ErpStockInFormItemVo> purchase_in_list_count_export(ErpStockInItemQuery query) {
        return repository.purchase_in_list_count_export(query);
    }

    @Override
    public List<ErpStockInFormItemVo> purchase_in_ht_export(Long iid) {
        return repository.purchase_in_ht_export(iid);
    }
}
