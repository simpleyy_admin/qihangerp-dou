package com.b2c.service.wms;

import java.util.List;

import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.repository.erp.ErpGoodsStockInfoRepository;
import com.b2c.interfaces.wms.ErpGoodsStockInfoService;
import com.b2c.entity.ErpGoodsStockInfoItemEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-26 16:34
 */
@Service
public class ErpGoodsStockInfoServiceImpl implements ErpGoodsStockInfoService {
    @Autowired
    private ErpGoodsStockInfoRepository repository;


    @Override
    public ErpGoodsStockInfoEntity getById(Long stockInfoId) {
        return repository.getById(stockInfoId);
    }

    @Override
    public void updateDeleteById(Long stockInfoId) {
        repository.updateDeleteById(stockInfoId);
    }

    @Override
    public void apportionStockLocationForSpec(Integer specId, Integer stockLocationId) {
        repository.apportionStockLocationForSpec(specId,stockLocationId);
    }

    @Override
    public List<ErpGoodsStockInfoItemEntity> getStockInfoItemListBySpecId(Integer specId) {
        return repository.getStockInfoItemListBySpecId(specId);
    }
}
