package com.b2c.service.wms;

import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.StockInRepository;
import com.b2c.interfaces.wms.StockInService;
import com.b2c.entity.vo.ErpCheckoutStockInItemVo;
import com.b2c.entity.vo.ErpPurchaseStockInItemVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-20 09:48
 */
@Service
public class StockInServiceImpl implements StockInService {
    @Autowired
    private StockInRepository stockInRepository;

//    @Override
//    public ResultVo<Long> checkoutStockIn(Long checkoutId, List<ErpCheckoutStockInItemVo> items, Integer stockInUserId, String stockInUserName) {
//        return stockInRepository.checkoutStockIn(checkoutId, items, stockInUserId, stockInUserName);
//    }

    @Override
    public ResultVo<Long> purchaseStockIn(Long invoiceId,Integer qcReport,String qcInspector, List<ErpPurchaseStockInItemVo> items, Integer stockInUserId, String stockInUserName) {
        return stockInRepository.purchaseStockIn(invoiceId, qcReport, qcInspector,items,stockInUserId,stockInUserName);
    }

    @Override
    public ResultVo<Long> checkoutStockInDataChecked(Long checkoutId, List<ErpCheckoutStockInItemVo> items) {
        return stockInRepository.checkoutStockInDataChecked(checkoutId, items);
    }

    @Override
    public ResultVo<String> purchaseStockInDataChecked(Long invoiceId, List<ErpPurchaseStockInItemVo> items) {
        return stockInRepository.purchaseStockInDataChecked(invoiceId,items);
    }
}
