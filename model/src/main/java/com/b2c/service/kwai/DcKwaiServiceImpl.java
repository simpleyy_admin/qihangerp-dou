package com.b2c.service.kwai;


import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.kwai.DcKwaiOrderEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.DcKwaiOrderRepositroy;
import com.b2c.repository.oms.OrderConfirmRepository;

import com.b2c.interfaces.kwai.DcKwaiOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DcKwaiServiceImpl implements DcKwaiOrderService {
    @Autowired
    private DcKwaiOrderRepositroy repository;
    @Autowired
    private OrderConfirmRepository confirmRepository;
    @Override
    public ResultVo<Integer> editKwaiOrder(DcKwaiOrderEntity order) {
        return repository.editKwaiOrder(order);
    }

    @Override
    public PagingResponse<DcKwaiOrderEntity> getKwaiOrders(Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String state) {
        return repository.getKwaiOrders(pageIndex,pageSize,orderNum,startTime,endTime,state);
    }

    @Override
    public DcKwaiOrderEntity getOderDetailByOrderId(Long id) {
        return repository.getOderDetailByOrderId(id);
    }
    @Override
    public ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId,Integer quantity) {
        return repository.updOrderSpec(orderItemId,erpGoodSpecId,quantity);
    }
    @Override
    public ResultVo<Integer> kwaiOrderAffirm(Long dcKwaiOrdersId, Integer clientId, String receiver, String mobile, String address, String sellerMemo) {
        return confirmRepository.kwaiOrderAffirm(dcKwaiOrdersId,clientId,receiver,mobile,address,sellerMemo);
    }

    @Override
    public void updKwaiOrderStatus(Long KwaiOid) {
        repository.updKwaiOrderStatus(KwaiOid);
    }
    @Override
    public ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity) {
        return repository.addOrderGift(orderId,erpGoodsId,erpGoodSpecId,quantity);
    }
}
