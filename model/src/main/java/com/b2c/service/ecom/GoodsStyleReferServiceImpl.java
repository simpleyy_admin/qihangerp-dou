package com.b2c.service.ecom;

import javax.annotation.Resource;

import com.b2c.interfaces.ecom.GoodsStyleReferService;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsStyleReferEntity;
import com.b2c.repository.ecom.GoodsStyleReferRepository;

@Service
public class GoodsStyleReferServiceImpl implements GoodsStyleReferService {
    @Resource
    private GoodsStyleReferRepository repository;
    @Override
    public PagingResponse<GoodsStyleReferEntity> getList(Integer pageIndex, Integer pageSize, String platform,String type) {
        
        return repository.getList(pageIndex, pageSize, platform, type);
    }

    @Override
    public void add(GoodsStyleReferEntity entity) {
        repository.add(entity);
        
    }
    
}
