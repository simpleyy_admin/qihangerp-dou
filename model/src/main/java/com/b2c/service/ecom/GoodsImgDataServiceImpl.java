package com.b2c.service.ecom;

import com.b2c.interfaces.ecom.GoodsImgDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsImgDataEntity;
import com.b2c.repository.ecom.GoodsImgDataRepository;

@Service
public class GoodsImgDataServiceImpl implements GoodsImgDataService {
    @Autowired
    private GoodsImgDataRepository repository;

    @Override
    public PagingResponse<GoodsImgDataEntity> getList(Integer pageIndex, Integer pageSize, String num,
            String platform) {
        
        return repository.getList(pageIndex, pageSize, num, platform);
    }

    @Override
    public void add(GoodsImgDataEntity entity) {
        repository.add(entity);
    }
    
}
