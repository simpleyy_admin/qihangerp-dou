package com.b2c.service.ecom;

import com.b2c.interfaces.ecom.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.ArticleEntity;
import com.b2c.repository.ecom.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository repository;

    @Override
    public PagingResponse<ArticleEntity> getList(Integer pageIndex, Integer pageSize, String title, String platform) {
 
        return repository.getList(pageIndex, pageSize, title, platform);
    }

    @Override
    public void add(String title, String content, String platform, String tag) {
        repository.add(title, content, platform, tag);
    }
    
}
