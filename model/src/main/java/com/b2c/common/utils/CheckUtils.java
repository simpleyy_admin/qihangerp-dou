package com.b2c.common.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description:公用数据检查类 pbd add 2019/5/6 14:55
 */
public class CheckUtils {
    /**
     * 检查手机号(非法返回false)
     *
     * @param str
     * @return
     */
    public static boolean isMobile(String str) {
        Pattern p = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$"); // 验证手机号
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 检查对象是否为空(为空返回true)
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof String && obj.toString().trim().length() == 0) {
            return true;
        }
        if (obj.getClass().isArray() && Array.getLength(obj) == 0) {
            return true;
        }
        if (obj instanceof Collection && ((Collection) obj).isEmpty()) {
            return true;
        }
        if (obj instanceof Map && ((Map) obj).isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * 检查字符是否包含空格(有空格返回false)
     *
     * @param str
     * @return
     */
    public static boolean isSpace(String str) {
        Pattern p = Pattern.compile("^\\S+$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 查询字符是否包含http前辍(有返回true)
     *
     * @param str
     * @return
     */
    public static String getHttpUrl(String str) {
        if (str.trim().startsWith("http")) {
            return str.trim();
        }
        return "http://" + str.trim();
    }

    // public static void main(String[] args) {
    // }
}
