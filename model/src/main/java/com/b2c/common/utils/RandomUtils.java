package com.b2c.common.utils;

import java.util.Random;

/**
 * 描述：
 * 随机Utils
 *
 * @author qlp
 * @date 2019-01-09 3:34 PM
 */
public class RandomUtils {

    /**
     * 生成随机字符串
     *
     * @param hasChar
     * @param lenght
     * @return
     */
    public static String builder(boolean hasChar, int lenght) {
        String str = "0,1,2,3,4,5,6,7,8,9";
        if (hasChar) {
            str = "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        }

        String[] str2 = str.split(",");//将字符串以,分割
        //        int sum = 0;//计数器
        //        for (int i=0; i<str2.length; ++i)
        //        {
        //            ++sum;
        //            if (0 == sum % 10)
        //            {
        //                System.out.println("");//没十个数据换行
        //            }
        //            System.out.print(str2[i] + " ");
        //        }
        //        System.out.println("");
        Random rand = new Random();//创建Random类的对象rand
        int index = 0;
        String randStr = "";//创建内容为空字符串对象randStr

        for (int i = 0; i < lenght; i++) {
            index = rand.nextInt(str2.length - 1);//在0到str2.length-1生成一个伪随机数赋值给index
            randStr += str2[index];
        }

//        for (String in 0..len) {
//            index = rand.nextInt(str2.size - 1)//在0到str2.length-1生成一个伪随机数赋值给index
//            randStr += str2[index]//将对应索引的数组与randStr的变量值相连接
//        }
        //        System.out.println("验证码：" + randStr);//输出所求的验证码的值
        return randStr;
    }


    /**
     * 获取一定长度的随机字符串
     *
     * @param length 指定字符串长度
     * @return 一定长度的字符串
     */
    public static String randomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}
