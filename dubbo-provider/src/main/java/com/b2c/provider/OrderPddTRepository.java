package com.b2c.provider;


import com.b2c.entity.result.PagingResp;
import com.b2c.entity.test.OrderPddTEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderPddTRepository {
    @Resource
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public PagingResp<OrderPddTEntity> getOrderList(Integer pageIndex, Integer pageSize, Integer shopId, Integer orderStatus, Integer refundStatus, String orderSn, String goodsId, String trackNum) {
        List<Object> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS  ");
        sb.append("id,order_sn FROM dc_pdd_orders ");
        sb.append(" WHERE shopId= ? ");
        sb.append(" AND  confirm_time > DATE_SUB(CURDATE(), INTERVAL 3 MONTH)  ");

        params.add(shopId);
        if (orderStatus != null) {
            sb.append(" AND order_status=? ");
            params.add(orderStatus);
        }


        if (refundStatus != null && refundStatus > 0) {
            sb.append(" AND refund_status=? ");
            params.add(refundStatus);
        }
        // else{
        //     sb.append(" AND refund_status=1 ");
        // }

        sb.append(" ORDER BY id DESC ");
        sb.append("  LIMIT ?,?");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);


        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(OrderPddTEntity.class), params.toArray(new Object[params.size()]));
        int totalSize = getTotalSize();
        return new PagingResp<>(pageIndex, pageSize, totalSize, list);
    }

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }
}
