package com.b2c.provider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//@MapperScan("com.b2c.provider")
@SpringBootApplication
@EnableDubbo//启动基于dubbo注解功能
public class DubboProviderApplication {

    public static void main(String[] args) {
        //new EmbeddedZooKeeper(2181, false).start();//启动zookeeper
        SpringApplication.run(DubboProviderApplication.class, args);
        System.out.println("dubbo service started");
    }
}
